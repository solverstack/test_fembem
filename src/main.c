#define ___TEST_FEMBEM___ 1

#include "main.h"

// instantiation of globales defined in main.h
enum numModel testedModel = _bem;
int nbRHS = 1;
int sparseRHS = -1;
int newRHS = 1;
int nbPts = 16000;
int tileSize = 1024;
int nbPtsMain;
int nbPtsBEM;
void *rhs = NULL;
void *solFMMfar = NULL;
void *solCLAfar = NULL;
void *solFMMnear = NULL;
void *solCLAnear = NULL;
int nbPtsPerLeaf = 50;
int nbAlgoRuns = 1;
char *sTYPE_ELEMENT_ARRAY_test_fembem[] = {NULL};
int coupled = 0;
int partial_fembem = 0;
int verboseTest = 0;
int use_ntiles_rec = 0;
int check_result = 0;
int generate_traces = 0;
int useDetail;
int computeNorm;
int symMatContent = 1;
int symMatSolver = 1;
int simplePrec = 0;
int complexALGO = 1;
ScalarType stype = DOUBLE_COMPLEX;
double lambda = -1.;
double ptsPerLambda = 10.;
double ptsPerLambdaDetail = -1.;
double radiusLeaf = 0.;
int writeMesh = 0;
int writeMeshUNV = 0;
int conformalMesh=1;
enum algo testedAlgo = _undefined;
int use_simple_assembly = 0;
int divider = 2;
int hmat_residual = 0;
int hmat_refine = 0;
int use_hmat_shuffle = 0;
int divider_min = 2;
int divider_max = 4;
int use_hodlr = 0;
size_t nnz_FEM = 0;
ZnonZero *z_matComp_FEM = NULL;
size_t *ptr_ordered_FEM = NULL;

/*! \brief Main routine
  \return 0 for success
*/
int main(int argc, char **argv) {
  int ierr ;
  double step, max_error = DBL_MAX;

  MpfArgGetDouble(&argc, argv, 1, "--max-error", &max_error);
  if (MpfArgHasName(&argc, argv, 1, "-h") || MpfArgHasName(&argc, argv, 1, "--help") ) {
    ierr=printHelp() ;
    return ierr;
  }
  /* --- Choix de l'algo a tester (_undefined par defaut) --- */
  if (MpfArgHasName(&argc, argv, 1, "-gemvhmat") > 0) {
    testedAlgo = _gemvHMAT ;
    printf("Testing : gemv HMAT.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-solvehmat") > 0) {
    testedAlgo = _solveHMAT ;
    printf("Testing : solve HMAT.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-inversehmat") > 0) {
    testedAlgo = _inverseHMAT ;
    printf("Testing : inverse HMAT.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-gemvchameleon") > 0) {
    testedAlgo = _gemvCHAMELEON ;
    printf("Testing : gemv CHAMELEON.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-solvechameleon") > 0) {
    testedAlgo = _solveCHAMELEON ;
    printf("Testing : solve CHAMELEON.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-gemvhlibpro") > 0) {
    testedAlgo = _gemvHLIBPRO ;
    printf("Testing : gemv HLIBPRO.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-solvehlibpro") > 0) {
    testedAlgo = _solveHLIBPRO ;
    printf("Testing : solve HLIBPRO.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-gemmhchameleon") > 0) {
    testedAlgo = _gemmHCHAMELEON ;
    printf("Testing : gemm H-CHAMELEON.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-gemvhchameleon") > 0) {
    testedAlgo = _gemvHCHAMELEON ;
    printf("Testing : gemv H-CHAMELEON.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-gemvcomposyx") > 0) {
    testedAlgo = _gemvCOMPOSYX ;
    printf("Testing : gemv COMPOSYX.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-solvecomposyx") > 0) {
    testedAlgo = _solveCOMPOSYX ;
    printf("Testing : solve COMPOSYX.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-solvehchameleon") > 0) {
    testedAlgo = _solveHCHAMELEON ;
    printf("Testing : solve H-CHAMELEON.\n") ;
  }
  /* Check the test */
  switch(testedAlgo) {
    case _gemvHMAT:
    case _solveHMAT:
    case _inverseHMAT:
      // nothing
      break;
    case _gemvCHAMELEON:
    case _solveCHAMELEON: // Add "--chameleon" in the command line if it is not yet there
      if (!MpfArgHasName(&argc, argv, 0, "--chameleon")) argv[argc++]="--chameleon";
      break;
    case _gemvHLIBPRO:
    case _solveHLIBPRO:
      break;
    case _gemvHCHAMELEON:
    case _gemmHCHAMELEON:
    case _solveHCHAMELEON: // Add "--chameleon" in the command line if it is not yet there
      if (!MpfArgHasName(&argc, argv, 0, "--chameleon")) argv[argc++]="--chameleon";
      break;
    case _gemvCOMPOSYX:
    case _solveCOMPOSYX:
      // nothing
    break;
    case _undefined:
    default:
      SETERRQ(1, "No algorithm selected. Please choose one (use -h/--help for usage).");
      break;
  }

  /* ------------------------------------------------------------------------- */
  /* ----------------------  INITIATION DE MPF (PARALLELE)  -------------------*/
  /* ------------------------------------------------------------------------- */

  ierr=SCAB_Init(&argc, &argv) ; CHKERRQ(ierr) ;

  switch(testedAlgo) {
    case _gemvHMAT:
    case _solveHMAT:
    case _inverseHMAT:
      ierr=readFlagsTestHMAT(&argc, &argv) ; CHKERRQ(ierr) ;
      ierr = init_hmat_interface(); CHKERRQ(ierr);
      break;
    case _gemvCHAMELEON:
    case _solveCHAMELEON:
      break;
    case _gemvHLIBPRO:
    case _solveHLIBPRO:
      ierr=initHLIBPRO(&argc, &argv) ; CHKERRQ(ierr) ;
      break;
    case _gemvHCHAMELEON:
    case _gemmHCHAMELEON:
    case _solveHCHAMELEON:
#ifdef HAVE_HMAT
      mpf_hmat_settings.engine = mpf_hmat_seq;
#endif
      ierr = init_hmat_interface(); CHKERRQ(ierr);
      break;
    case _gemvCOMPOSYX:
    case _solveCOMPOSYX:
    break;
  case _undefined:
    default:
      SETERRQ(1, "No algorithm selected. Please choose one (use -h/--help for usage).");
      break;
  }

  /* ------------------------------------------------------------------------- */
  /* ---------------------  DECODAGE DE LA LIGNE D'OPTIONS  -------------------*/
  /* ------------------------------------------------------------------------- */

  /* --- Numerical method and matrix content we want to test (_bem is default) --- */
  if (MpfArgHasName(&argc, argv, 1, "--bem") > 0) {
    testedModel = _bem ;
    Mpf_printf(MPI_COMM_WORLD, "Testing: BEM (dense matrix).\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "--fem") > 0) {
    testedModel = _fem ;
    Mpf_printf(MPI_COMM_WORLD, "Testing : FEM (sparse matrix).\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "--fembem") > 0) {
    testedModel = _fembem ;
    Mpf_printf(MPI_COMM_WORLD, "Testing : FEM-BEM (coupled dense/sparse matrices).\n") ;
  }

  if (MpfArgHasName(&argc, argv, 1, "-v")) {
    verboseTest=1 ;
    Mpf_printf(MPI_COMM_WORLD, "Mode verbose\n") ;
  }
  double nbf;
  if (MpfArgGetDouble(&argc, argv, 1, "-nbpts", &nbf)) {
    if (nbf<0 || nbf>INT_MAX)
      SETERRQ(1, "Incorrect value '-nbpts %.0f' (must be between 0 and %d)", nbf, INT_MAX);
    nbPts = (int)nbf;
    Mpf_printf(MPI_COMM_WORLD, "Reading nbPts = %d\n", nbPts) ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-ntilesrec") > 0) {
    use_ntiles_rec = 1;
    Mpf_printf(MPI_COMM_WORLD, "Clustering: NTiles recursive\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-check_result") > 0) {
    check_result = 1;
    Mpf_printf(MPI_COMM_WORLD, "Check result activated\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "-traces") > 0) {
    generate_traces = 1;
    Mpf_printf(MPI_COMM_WORLD, "Generate traces activated\n") ;
  }

  /* Nullify part of the FEM-EM interactions (used in initPipe) */
  if (MpfArgHasName(&argc, argv, 1, "--partial-fembem")) {
    partial_fembem = 1;
    Mpf_printf(MPI_COMM_WORLD, "Using partial interactions between BEM and FEM unknowns\n");
  }

  /* --- Write a VTK file 'mesh.vtu' of the mesh --- */
  if (MpfArgHasName(&argc, argv, 1, "--write-mesh")) {
    writeMesh=1;
    Mpf_printf(MPI_COMM_WORLD, "Activate writing of VTK file 'mesh.vtu'\n") ;
  }
  /* --- Write a UNV file 'mesh.unv' of the mesh --- */
  if (MpfArgHasName(&argc, argv, 1, "--write-mesh-unv")) {
    writeMeshUNV=1;
    Mpf_printf(MPI_COMM_WORLD, "Activate writing of UNV file 'mesh.unv'\n") ;
  }
  /* --- Create (or not) a conformal volume mesh in pipe.c --- */
  if (MpfArgHasName(&argc, argv, 1, "--conformal-mesh")) {
      conformalMesh=1;
      Mpf_printf(MPI_COMM_WORLD, "Create a conformal volume mesh.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "--no-conformal-mesh")) {
      conformalMesh=0;
      Mpf_printf(MPI_COMM_WORLD, "Do not create a conformal volume mesh (old way)\n") ;
  }

  switch(testedModel){
    case _bem: // 2D mesh
      nbPtsBEM = nbPts;
      nbPtsMain = nbPts;
      ierr=initCylinder(&argc, &argv) ; CHKERRQ(ierr) ; break;
    case _fem:
    case _fembem: // 3D mesh
      ierr=initPipe(&argc, &argv) ; CHKERRQ(ierr) ; break;
    default:
      SETERRQ(1, "Incorrect value testedModel=%d");
      break;
  }

  if (MpfArgGetInt(&argc, argv, 1, "-nbrhs", &nbRHS)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading nbRHS = %d\n", nbRHS) ;
  }
  if (MpfArgGetInt(&argc, argv, 1, "-sparserhs", &sparseRHS)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading sparseRHS = %d\n", sparseRHS) ;
  }
  if (MpfArgHasName(&argc, argv, 1, "--new-rhs")) {
    newRHS = 1;
    Mpf_printf(MPI_COMM_WORLD, "Using NEW right-hand sides.\n");
  }
  if (MpfArgHasName(&argc, argv, 1, "--no-new-rhs")) {
    newRHS = 0;
    Mpf_printf(MPI_COMM_WORLD, "Using OLD right-hand sides.\n");
  }

  /* Added detail on the main cylinder */
  if (MpfArgHasName(&argc, argv, 1, "-with_detail")) {
    useDetail = 1;
    ASSERTQ(testedModel==_bem); // detail is not (yet) compatible with FEM
    Mpf_printf(MPI_COMM_WORLD, "Add an overmeshed detail\n");
  }

  /* Wavelength */
  if (MpfArgGetDouble(&argc, argv, 1, "-lambda", &lambda)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading lambda = %f\n", lambda) ;
    ptsPerLambda=-1; // Just to remember that this value wasn't used
  }
  /* Points per wavelength */
  if (MpfArgGetDouble(&argc, argv, 1, "-ptsperlambda", &ptsPerLambda)) {
    if (lambda!=-1.)
      SETERRQ(1, "Can not use both -lambda and -ptsPerLambda.") ;
    Mpf_printf(MPI_COMM_WORLD, "Reading ptsPerLambda = %f\n", ptsPerLambda) ;
  }
  /* Setting lambda it it is not given by the user */
  if (lambda==-1.) {
    ierr = getMeshStep(&step) ;
    lambda = ptsPerLambda*step ;
    Mpf_printf(MPI_COMM_WORLD, "   Setting lambda = %f (with %f points per wavelength)\n", lambda, ptsPerLambda) ;
  }

  if (useDetail == 1) {
    //ici on est sur que lambda est initialise, ce dont on a besoin
    // pour le setting de
    ierr = initCylinderDetail(&argc, &argv) ; CHKERRQ(ierr) ;
  }

  /* --- Choice of the matrix symmetry (true by default) --- */
  if (MpfArgHasName(&argc, argv, 1, "--sym") > 0) {
    symMatSolver = 1 ;
    symMatContent = 1 ;
    Mpf_printf(MPI_COMM_WORLD, "Testing: Symmetric matrices and solvers.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "--nosym") > 0) {
    symMatSolver = 0 ;
    symMatContent = 0 ;
    Mpf_printf(MPI_COMM_WORLD, "Testing: Non-Symmetric matrices and solvers.\n") ;
  }
  if (MpfArgHasName(&argc, argv, 1, "--symcont") > 0) {
    symMatSolver = 0 ;
    symMatContent = 1 ;
    Mpf_printf(MPI_COMM_WORLD, "Testing: Symmetric matrices and non symmetric solvers.\n") ;
  }
  if (symMatContent==1 && MpfArgHasName(&argc, argv, 1, "--posdef") > 0) {
    symMatContent = 2 ;
    Mpf_printf(MPI_COMM_WORLD, "Testing: Positive definite matrices.\n") ;
  }

  /* --- Choix de l'arithmetique de calcul (default is '-z') --- */
  if (MpfArgHasName(&argc, argv, 1, "-s") > 0) {
    simplePrec = 1 ;
    stype=SIMPLE_PRECISION ; /* For the fmm, it will be changed below depending on the FMM algo selected */
    complexALGO=0;
    Mpf_printf(MPI_COMM_WORLD, "Simple Precision\n");
  }
  if (MpfArgHasName(&argc, argv, 1, "-d") > 0) {
    simplePrec = 0 ;
    stype=DOUBLE_PRECISION ; /* For the fmm, it will be changed below depending on the FMM algo selected */
    complexALGO=0;
    Mpf_printf(MPI_COMM_WORLD, "Double Precision\n");
  }
  if (MpfArgHasName(&argc, argv, 1, "-c") > 0) {
    simplePrec = 1 ;
    stype=SIMPLE_COMPLEX ; /* For the fmm, it will be changed below depending on the FMM algo selected */
    complexALGO=1;
    Mpf_printf(MPI_COMM_WORLD, "Simple Complex\n");
  }
  if (MpfArgHasName(&argc, argv, 1, "-z") > 0) {
    simplePrec = 0 ;
    stype=DOUBLE_COMPLEX ; /* For the fmm, it will be changed below depending on the FMM algo selected */
    complexALGO=1;
    Mpf_printf(MPI_COMM_WORLD, "Double Complex\n");
  }

  /* --- Compute and display matrix norms during tests --- */
  if (MpfArgHasName(&argc, argv, 1, "--compute_norm") > 0) {
    computeNorm = 1 ;
    Mpf_printf(MPI_COMM_WORLD, "Compute and display matrix norms during tests \n") ;
  }

  /* Setting remaining variables */
  if (sparseRHS == -1) {
    sparseRHS=(double)nbPts/80./log10((double)nbPts) ;
    if (sparseRHS<1) sparseRHS = 1;
    Mpf_printf(MPI_COMM_WORLD, "Setting sparseRHS = %d\n", sparseRHS) ;
  }

  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> TEST_FEMBEM_Version = %s\n" , PACKAGE_VERSION);
#ifdef HAVE_HMAT
  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> HMAT_Version = %s\n" , hmat_get_version() );
#endif
  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> ScalarType = %s\n" , MPF_scalName[stype]);

  /* Prepare the mesh and RHS used for the FMM/HMAT tests */
  ierr = prepareTEST() ; CHKERRQ(ierr) ;
  double relative_error;
  /* Run the test */
  switch(testedAlgo) {
    case _gemvHMAT:
    case _solveHMAT:
    case _inverseHMAT:
      ierr = testHMAT(&relative_error) ; CHKERRQ(ierr) ;
      break;
    case _gemvCHAMELEON:
    case _solveCHAMELEON:
      ierr = testCHAMELEON(&relative_error); CHKERRQ(ierr);
      break;
    case _gemvHLIBPRO:
    case _solveHLIBPRO:
      ierr = testHLIBPRO(&relative_error); CHKERRQ(ierr);
      break;
    case _gemvHCHAMELEON:
    case _gemmHCHAMELEON:
    case _solveHCHAMELEON:
      ierr = testHCHAMELEON(&relative_error); CHKERRQ(ierr);
      break;
    case _gemvCOMPOSYX:
    case _solveCOMPOSYX:
      ierr = testCOMPOSYX(&relative_error);  CHKERRQ(ierr);
    break;
    default:
      SETERRQ(1, "Unknown algorithm=%d", testedAlgo);
      break;
  }
  int error_exit = 0;
  if(relative_error > max_error) {
    error_exit = 1;
    Mpf_printf(MPI_COMM_WORLD, "Error is too high (%g > %g), exiting with error.", relative_error, max_error);
  }
  Mpf_printf(MPI_COMM_WORLD, "\ntest_FEMBEM : end of computation\n");

  if (rhs) {
    MpfFree(rhs);
    rhs = NULL;
  }

  ierr=SCAB_Exit(&argc, argv); CHKERRQ(ierr);

  return error_exit;
}
