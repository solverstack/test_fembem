#include "main.h"
#ifdef HAVE_CHAMELEON
#include "chameleon.h"
cham_flttype_t chameleonType;
int (*CHAMELEON_sytrf_Tile)( cham_uplo_t, CHAM_desc_t * )                = NULL; // LLt facto
int (*CHAMELEON_sytrs_Tile)( cham_uplo_t, CHAM_desc_t *, CHAM_desc_t * ) = NULL; // LLt solve
int (*CHAMELEON_potrf_Tile)( cham_uplo_t, CHAM_desc_t * )                = NULL; // LLh facto
int (*CHAMELEON_potrs_Tile)( cham_uplo_t, CHAM_desc_t * )                = NULL; // LLh solve
int (*CHAMELEON_getrf_nopiv_Tile)( CHAM_desc_t * )                       = NULL; // LU facto
int (*CHAMELEON_getrs_nopiv_Tile)( CHAM_desc_t *, CHAM_desc_t * )        = NULL; // LU Solve

int
testFEMBEM_initChameleon( ScalarType stype ) {
  int ierr = 0;

  /* Set the scalar type and the functions used by CHAMELEON */
  switch(stype) {
#if defined(CHAMELEON_PREC_S)
    case (SIMPLE_PRECISION) :
      chameleonType = ChamRealFloat;
      CHAMELEON_sytrf_Tile = CHAMELEON_spotrf_Tile;
      CHAMELEON_sytrs_Tile = CHAMELEON_spotrs_Tile;
      CHAMELEON_potrf_Tile = CHAMELEON_spotrf_Tile;
      CHAMELEON_getrf_nopiv_Tile = CHAMELEON_sgetrf_nopiv_Tile;
      CHAMELEON_getrs_nopiv_Tile = CHAMELEON_sgetrs_nopiv_Tile;
      break ;
#endif
#if defined(CHAMELEON_PREC_D)
    case (DOUBLE_PRECISION) :
      chameleonType = ChamRealDouble;
      CHAMELEON_sytrf_Tile = CHAMELEON_dpotrf_Tile;
      CHAMELEON_sytrs_Tile = CHAMELEON_dpotrs_Tile;
      CHAMELEON_potrf_Tile = CHAMELEON_dpotrf_Tile;
      CHAMELEON_getrf_nopiv_Tile = CHAMELEON_dgetrf_nopiv_Tile;
      CHAMELEON_getrs_nopiv_Tile = CHAMELEON_dgetrs_nopiv_Tile;
      break ;
#endif
#if defined(CHAMELEON_PREC_C)
    case (SIMPLE_COMPLEX) :
      chameleonType = ChamComplexFloat;
      CHAMELEON_sytrf_Tile = CHAMELEON_csytrf_Tile;
      CHAMELEON_sytrs_Tile = CHAMELEON_csytrs_Tile;
      CHAMELEON_potrf_Tile = CHAMELEON_cpotrf_Tile;
      CHAMELEON_getrf_nopiv_Tile = CHAMELEON_cgetrf_nopiv_Tile;
      CHAMELEON_getrs_nopiv_Tile = CHAMELEON_cgetrs_nopiv_Tile;
      break ;
#endif
#if defined(CHAMELEON_PREC_Z)
    case (DOUBLE_COMPLEX) :
      chameleonType = ChamComplexDouble;
      CHAMELEON_sytrf_Tile = CHAMELEON_zsytrf_Tile;
      CHAMELEON_sytrs_Tile = CHAMELEON_zsytrs_Tile;
      CHAMELEON_potrf_Tile = CHAMELEON_zpotrf_Tile;
      CHAMELEON_getrf_nopiv_Tile = CHAMELEON_zgetrf_nopiv_Tile;
      CHAMELEON_getrs_nopiv_Tile = CHAMELEON_zgetrs_nopiv_Tile;
      break ;
#endif
    default :
      SETERRQ(1, "testHCHAMELEON : unknown scalar type\n") ;
      break ;
  }
  return ierr;
}

int CHAMELEON_gemm_Tile( CHAM_desc_t *descA,
                         CHAM_desc_t *descX,
                         CHAM_desc_t *descY )
{
  int ierr = 0;

  switch(stype) {
#if defined(CHAMELEON_PREC_S)
    case (SIMPLE_PRECISION) :
      ierr = CHAMELEON_sgemm_Tile( ChamNoTrans, ChamNoTrans,
                                   (float)1., descA, descX,
                                   (float)0., descY );
      CHKERRQ(ierr);
      break ;
#endif
#if defined(CHAMELEON_PREC_D)
    case (DOUBLE_PRECISION) :
      ierr = CHAMELEON_dgemm_Tile( ChamNoTrans, ChamNoTrans,
                                   (double)1., descA, descX,
                                   (double)0., descY);
      CHKERRQ(ierr);
      break ;
#endif
#if defined(CHAMELEON_PREC_C)
    case (SIMPLE_COMPLEX) :
      ierr = CHAMELEON_cgemm_Tile( ChamNoTrans, ChamNoTrans,
                                   (CHAMELEON_Complex32_t)1., descA, descX,
                                   (CHAMELEON_Complex32_t)0., descY);
      CHKERRQ(ierr);
      break ;
#endif
#if defined(CHAMELEON_PREC_Z)
    case (DOUBLE_COMPLEX) :
      ierr = CHAMELEON_zgemm_Tile( ChamNoTrans, ChamNoTrans,
                                   (CHAMELEON_Complex64_t)1., descA, descX,
                                   (CHAMELEON_Complex64_t)0., descY);
      CHKERRQ(ierr);
      break ;
#endif
    default :
      SETERRQ(1, "CHAMELEON_gemm_Tile: unknown scalar type\n") ;
      break ;
  }
  return ierr;
}

// indices are 0 based, bounds included
static int
CHAMELEON_build_callback_FEMBEM( void *op_args,
                                 cham_uplo_t uplo, int m, int n, int ndata,
                                 const CHAM_desc_t *desc, CHAM_tile_t *tile, ... )
{
  ASSERTA(tile);

  //    CHAM_tile_t *tile = desc->get_blktile( desc, m, n );
  int row_min = m * desc->mb + 1;
  int row_max = row_min + tile->m - 1;
  int col_min = n * desc->nb + 1;
  int col_max = col_min + tile->n - 1;
  int size_of_buffer = tile->m * tile->n;

  (simplePrec ? computeDenseBlockFEMBEM_Cprec : computeDenseBlockFEMBEM)( &row_min, &row_max, &col_min, &col_max,
                                                                          &Mpf_i_zero, &Mpf_i_one, &Mpf_i_one, &Mpf_i_one,
                                                                          &size_of_buffer, &(tile->ld), tile->mat, op_args );

  (void)ndata;
  return 0;
}

int CHAMELEON_generate_matrix( cham_flttype_t flttype, int NB, int PQ[2],
CHAM_desc_t     **descA )
{
  cham_map_data_t data;
  cham_map_operator_t op;
  int ierr;
  int N = nbPts;
  cham_uplo_t uplo = symMatSolver ? ChamLower : ChamUpperLower;
  contextTestFEMBEM *myCtx = MpfCalloc(1, sizeof(contextTestFEMBEM)) ; CHKPTRQ(myCtx);
  myCtx->colDim = N;

  /**
     * Let's first create the Chameleon descriptor
     */
  ierr = CHAMELEON_Desc_Create( descA, CHAMELEON_MAT_ALLOC_GLOBAL, flttype,
                                NB, NB, NB*NB,  // Tiles' dimensions
                                N, N,           // Matrix's dimension
                                0, 0, N, N,     // Work with the whole matrix
                                PQ[0], PQ[1] ); // number of rows and columns of the 2D distribution grid
  CHKERRQ(ierr);

  // Assembly driven by chameleon and using the callback CHAMELEON_build_callback_FEMBEM() defined above
  data.access = ChamW;
  data.desc   = *descA;

  op.name     = "BuildFEMBEM";
  op.cpufunc  = CHAMELEON_build_callback_FEMBEM;
  op.cudafunc = NULL;
  op.hipfunc  = NULL;

  ierr = CHAMELEON_mapv_Tile( uplo, 1, &data, &op, myCtx );
  CHKERRQ(ierr);

  MpfFree( myCtx );

  return 0;
}
#endif
