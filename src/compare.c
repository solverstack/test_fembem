#include "main.h"

static inline void
frobenius_update( double *scale, double *sumsq, const double value )
{
  double absval = fabs(value);
  double ratio;
  if ( absval != 0. ){
    if ( (*scale) < absval ) {
      ratio = (*scale) / absval;
      *sumsq = (double)1. + (*sumsq) * ratio * ratio;
      *scale = absval;
    } else {
      ratio = absval / (*scale);
      *sumsq = (*sumsq) + ratio * ratio;
    }
  }
}

int computeRelativeError(void *sol, void *ref, double *eps) {
  int ierr, rank ;
  double normRef[2], normDelta[2];
  Z_type *z_sol=sol, *z_ref=ref ;
  D_type *d_sol=sol, *d_ref=ref ;

  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank); CHKERRQ(ierr);
  *eps=0.;
  if (rank==0) {
    normDelta[0] = 1.;
    normDelta[1] = 0.;
    normRef[0]   = 1.;
    normRef[1]   = 0.;
    for (size_t i=0 ; i<(size_t)nbPts*nbRHS ; i++) {
      if (complexALGO) {
        frobenius_update( normDelta, normDelta+1, (z_sol[i].r - z_ref[i].r) );
        frobenius_update( normDelta, normDelta+1, (z_sol[i].i - z_ref[i].i) );

        frobenius_update( normRef, normRef+1, z_ref[i].r );
        frobenius_update( normRef, normRef+1, z_ref[i].i );
      } else {
        frobenius_update( normDelta, normDelta+1, (d_sol[i] - d_ref[i]) );
        frobenius_update( normRef, normRef+1, d_ref[i] );
      }
    }
    if ( normDelta[1] == 0. ) {/* To handle correctly the case where both vectors are null */
      *eps = 0.;
    }
    else {
      *eps = (normDelta[0] * sqrt( normDelta[1] ) )
          /  (normRef[0]   * sqrt( normRef[1]   ) );
    }
  }

  return 0 ;
}

int computeVecNorm(void *ref, double *norm) {
  int ierr, rank ;
  double normRef ;
  Z_type *z_ref=ref ;
  D_type *d_ref=ref ;

  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank); CHKERRQ(ierr);
  *norm=0.;
  if (rank==0) {
    normRef=0. ;
    for (size_t i=0 ; i<(size_t)nbPts*nbRHS ; i++) {
      if (complexALGO) {
        normRef += z_ref[i].r*z_ref[i].r+z_ref[i].i*z_ref[i].i ;
      } else {
        normRef += d_ref[i]*d_ref[i] ;
      }
    }
    *norm = sqrt(normRef) ;
  }

  return 0 ;
}

int sommePara(double *buf) {
  size_t n = (size_t)(complexALGO?2:1)*nbPts*nbRHS ;
  /* En parallele, somme les resultats des differents processeurs vers tous les proc */
  /* MPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm) */
  int ierr=MPI_AllreduceL(MPI_IN_PLACE, buf, n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD) ; CHKERRQ(ierr) ;

  return 0 ;
}
