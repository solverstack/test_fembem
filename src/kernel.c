#include "main.h"
#include <mpf_simd.h>

extern double meshStep;
MPF_SIMD_DISP(int computeKernelBEM(double *coord1, double *coord2, int self, double complex *kernel)) {
  double r, v[3], k=0. ;

  v[0]=coord1[0]-coord2[0] ;
  v[1]=coord1[1]-coord2[1] ;
  v[2]=coord1[2]-coord2[2] ;
  r=sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]) ;
  if (r<meshStep/2.) r=meshStep/2.;

  if (complexALGO) {
    k=2*M_PI/lambda;
    *kernel = mpf_simd_cexp(I * k * r) / (4 * M_PI * r);
  } else
    *kernel = 1 / (4 * M_PI * r);

  if (symMatContent==0) { // non symmetric case
    // s = prod scal nu.r avec nu=(1,1,1)/sqrt(3)
    double s = (v[0]+v[1]+v[2])/1.7320508075688772;
    double fs=1+atan(s)/M_PI;
    *kernel *= fs;
  }
  if (symMatContent==2 && self) { // symmetric positive definite case : add 'n.max(A)' times Identity
    *kernel *= nbPts;
  }
  return 0;
}
