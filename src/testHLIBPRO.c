#include "main.h"

#ifdef HAVE_HLIBPRO
#include "hlib-c.h"

#define CHECK_INFO  { if ( info != HLIB_NO_ERROR ) \
{ char buf[1024]; hlib_error_desc( buf, 1024 ); \
  printf( "\n%s\n\n", buf ); exit(1); } }

static double assemblyEpsilon=1e-4;
static double recompressionEpsilon=1e-4;
static hlib_lrapx_t compressionMethod=HLIB_LRAPX_ACAPLUS;
static int maxLeafSize=100;
static  hmat_factorization_t factorization_type=hmat_factorization_ldlt;
static int nbCPUs=12;

static void ckernelHLIBpro ( const size_t n, const int *rowidx, const size_t m, const int *colidx,
                             hlib_complex_t *matrix, void *user_context ) {
  size_t  rowi, colj;
  for ( colj = 0; colj < m; colj++ ) {
    const int idx1 = colidx[colj];
    for ( rowi = 0; rowi < n; rowi++ ) {
      const int idx0 = rowidx[rowi];
      interactionKernel(user_context, idx0, idx1, (void*)&matrix[(colj*n) + rowi]);
    }
  }
}

static void kernelHLIBpro ( const size_t n, const int *rowidx, const size_t m, const int *colidx,
                            hlib_real_t *matrix, void *user_context ) {
  size_t  rowi, colj;
  for ( colj = 0; colj < m; colj++ ) {
    const int idx1 = colidx[colj];
    for ( rowi = 0; rowi < n; rowi++ ) {
      const int idx0 = rowidx[rowi];
      interactionKernel(user_context, idx0, idx1, (void*)&matrix[(colj*n) + rowi]);
    }
  }
}

/*! \brief Init parameters for HLIBpro solver
   */
int initHLIBPRO(int* argc, char*** argv) {

  unsigned int M=hlib_major_version();
  unsigned int m=hlib_minor_version();
  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> HLIBpro_Version = %d.%d\n", M, m);

  if (MpfArgGetDouble( argc, *argv, 1, "--hlibpro-eps-assemb" , &assemblyEpsilon  )) {
    Mpf_printf(MPI_COMM_WORLD,"[HLIBpro] Compression epsilon: %e\n", assemblyEpsilon );
  }
  if (MpfArgGetDouble( argc, *argv, 1, "--hlibpro-eps-recompr" , &recompressionEpsilon  )) {
    Mpf_printf(MPI_COMM_WORLD,"[HLIBpro] Recompression epsilon: %e\n", recompressionEpsilon );
  }

  if (MpfArgHasName( argc, *argv, 1, "--hlibpro-svd" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HLIBpro] Using SVD compression\n");
    compressionMethod = HLIB_LRAPX_SVD;
  }
  if (MpfArgHasName( argc, *argv, 1, "--hlibpro-aca-partial" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HLIBpro] Using ACA with partial pivoting\n");
    compressionMethod = HLIB_LRAPX_ACA;
  }
  if (MpfArgHasName( argc, *argv, 1, "--hlibpro-aca-full" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HLIBpro] Using ACA with full pivoting\n");
    compressionMethod = HLIB_LRAPX_ACAFULL;
  }

  if (MpfArgHasName( argc, *argv, 1, "--hlibpro-lu" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HLIBpro] Force LU decomposition\n");
    factorization_type = hmat_factorization_lu;
  }
  if (MpfArgHasName( argc, *argv, 1, "--hlibpro-llt" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HLIBpro] Force LLt decomposition (for symmetric matrices)\n");
    factorization_type = hmat_factorization_llt;
  }
  if (MpfArgHasName( argc, *argv, 1, "--hlibpro-ldlt" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HLIBpro] Force LDLt decomposition (for symmetric matrices)\n");
    factorization_type = hmat_factorization_ldlt;
  }

  if (MpfArgGetInt ( argc, *argv, 1, "--hlibpro-leaf-size", &maxLeafSize))  {
    Mpf_printf(MPI_COMM_WORLD,"[HLIBpro] Maximum leaf size: %d\n", maxLeafSize);
  }

  /* Number of CPUs for the HLIBpro solver: OMP_NUM_THREADS (or 12 if it is not set)
   */
  char *s_num;
  if ( ((s_num=getenv("OMP_NUM_THREADS")) != NULL) && (atoi(s_num) != 0) )
    nbCPUs=atoi(s_num) ;
  MpfArgGetInt( argc, *argv, 1, "--hlibpro-ncpus", &nbCPUs);
  Mpf_printf(MPI_COMM_WORLD, "[HLIBpro] Using lib HLIBpro with %d cores.\n", nbCPUs);

  return 0;
}

/*! \brief Runs the test using HLIBPRO solver
   */
int testHLIBPRO(double * relative_error) {
  int ierr, rank, sSize ;
  double temps_initial, temps_final, temps_cpu;

  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> HAssemblyAccuracy = %.4e \n", assemblyEpsilon) ;
  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> HAssemblyRecompression = %.4e \n", recompressionEpsilon) ;

  switch(stype){
    case SIMPLE_PRECISION:
    case SIMPLE_COMPLEX:
      SETERRQ(1, "Simple precision accuracy unavailable in hlibpro");
      break;
    default:
      sSize = Mpf_stype_size[stype];
      break;
  }

  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank); CHKERRQ(ierr);

  /* Realise le produit matrice vecteur classique : A.rhs -> solCLA */

  Mpf_printf(MPI_COMM_WORLD, "\n**** Computing Classical product...\n") ;
  void *solCLA=NULL;
  ierr = produitClassique(&solCLA, MPI_COMM_WORLD) ; CHKERRQ(ierr) ;

  /* Cree la H-Matrice */

  temps_initial = getTime ();
  Mpf_printf(MPI_COMM_WORLD, "\n**** Creating HLIBpro Matrix...\n") ;

  /* Check the coherency between the factorization chosen (with --hlibpro-lu/llt/ldlt)
     and the symmetry of the matrix (chosen with --sym/--nosym/--symcont) */
  if (testedAlgo==_solveHLIBPRO)
    switch(factorization_type){
      case hmat_factorization_ldlt:
      case hmat_factorization_llt:ASSERTQ(symMatSolver); break;
      case hmat_factorization_lu:ASSERTQ(!symMatSolver); break;
      default:break;
    }

  int info, i;

  hlib_init( & info ); CHECK_INFO;
  hlib_set_verbosity( 2 );
  hlib_set_nthreads( nbCPUs );

  Mpf_printf(MPI_COMM_WORLD, "HLIBpro: building cluster tree\n" );

  double *points = testedModel==_bem ? createCylinder() : createPipe();
  // HLIBpro demands an array of pointers for the vertices
  double**vertices = MpfCalloc( (size_t)nbPts, sizeof(double*) ); CHKPTRQ(vertices);
  for ( i = 0; i < nbPts; i++ )
    vertices[i] = &points[(size_t)3*i];
  hlib_coord_t coord = hlib_coord_import( (size_t)nbPts, 3, vertices, NULL, & info ); CHECK_INFO;
  hlib_clustertree_t ct = hlib_clt_build_bsp( coord, HLIB_BSP_CARD_MAX, maxLeafSize, & info ); CHECK_INFO;
  hlib_clt_print_ps( ct, "testHLIBpro_ct", & info ); CHECK_INFO;

  Mpf_printf(MPI_COMM_WORLD, "HLIBpro: building block cluster tree\n" );
  hlib_admcond_t adm = hlib_admcond_geom( HLIB_ADM_STD_MIN, 3.0, & info ); CHECK_INFO;
  hlib_blockclustertree_t bct = hlib_bct_build( ct, ct, adm, & info ); CHECK_INFO;
  hlib_bct_print_ps( bct, "testHLIBpro_bct", & info ); CHECK_INFO;

  Mpf_printf(MPI_COMM_WORLD, "HLIBpro: building BEM matrix using ACA+\n" );
  hlib_acc_t acc = hlib_acc_fixed_eps( assemblyEpsilon );
  contextTestFEMBEM *myCtx = MpfCalloc(1, sizeof(contextTestFEMBEM)) ; CHKPTRQ(myCtx);
  hlib_matrix_t hmatrix = stype==DOUBLE_COMPLEX ?
        hlib_matrix_build_ccoeff( bct, ckernelHLIBpro, (void*)myCtx, compressionMethod, acc, symMatSolver, & info ) :
        hlib_matrix_build_coeff( bct, kernelHLIBpro, (void*)myCtx, compressionMethod, acc, symMatSolver, & info ); CHECK_INFO;
  hlib_matrix_print_ps( hmatrix, "testHLIBpro_mat", HLIB_MATIO_SVD, & info );        CHECK_INFO;

  temps_final = getTime ();
  temps_cpu = temps_final - temps_initial ;
  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuCreation = %f \n", temps_cpu) ;

  /* Compute & display some informations */
  if (computeNorm) {
    temps_initial = getTime ();
    Mpf_printf(MPI_COMM_WORLD, "Norm: %.16g\n", hlib_matrix_norm_frobenius( hmatrix, & info ));CHECK_INFO;
    temps_final = getTime ();
    temps_cpu = temps_final - temps_initial ;
    Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuNorm = %f \n", temps_cpu) ;
  }
  size_t bytesize = hlib_matrix_bytesize( hmatrix, & info ); CHECK_INFO;
  size_t fullsize = (size_t) nbPts*nbPts*sSize/(symMatSolver?2.:1.);
  Mpf_printf(MPI_COMM_WORLD, "Compressed size: %ld\n", bytesize/sSize);
  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> AssembledSizeMb = %f \n", (double)bytesize/1024./1024.) ;
  Mpf_printf(MPI_COMM_WORLD, "Uncompressed size: %ld\n", fullsize/sSize);
  Mpf_printf(MPI_COMM_WORLD, "Ratio: %g\n", (double)bytesize/fullsize);

  /* Create two vectors for HLIBpro */
  hlib_vector_t vec_in = stype==DOUBLE_COMPLEX ? hlib_vector_cbuild( nbPts, & info ) : hlib_vector_build( nbPts, & info ); CHECK_INFO;
  hlib_vector_t vec_out= stype==DOUBLE_COMPLEX ? hlib_vector_cbuild( nbPts, & info ) : hlib_vector_build( nbPts, & info ); CHECK_INFO;

  /* Switch on the various HLIBpro tests */

  switch(testedAlgo) {
    case _gemvHLIBPRO:

      /* Appelle le produit mat.vec : A.rhs -> solHLIBpro */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Computing HLIBpro product...\n") ;
      void *solHLIBpro = MpfCalloc((size_t)nbPts*nbRHS, complexALGO ? sizeof(Z_type) : sizeof(D_type)) ; CHKPTRQ(solHLIBpro) ;
      temps_initial = getTime ();

      for (i=0 ; i<nbRHS ; i++) {
        /* import the data into vec_in */
        stype==DOUBLE_COMPLEX ?
              hlib_vector_cimport( vec_in, (void*)((char*)rhs+(size_t)i*nbPts*sSize), & info ) :
              hlib_vector_import( vec_in, (void*)((char*)rhs+(size_t)i*nbPts*sSize), & info ); CHECK_INFO;
        /* bring into H-ordering */
        hlib_vector_permute( vec_in, hlib_clt_perm_e2i( ct, & info ), & info ); CHECK_INFO;
        /* Do the GEMV */
        hlib_matrix_mulvec( 1.0, hmatrix, vec_in, 0.0, vec_out, HLIB_MATOP_NORM, & info ); CHECK_INFO;
        /* bring back from H-ordering */
        hlib_vector_permute( vec_out, hlib_clt_perm_i2e( ct, & info ), & info ); CHECK_INFO;
        /* export the data out of vec_out */
        stype==DOUBLE_COMPLEX ?
              hlib_vector_cexport( vec_out, (void*)((char*)solHLIBpro+(size_t)i*nbPts*sSize), & info ):
              hlib_vector_export( vec_out, (void*)((char*)solHLIBpro+(size_t)i*nbPts*sSize), & info ); CHECK_INFO;
      }

      if (rank) {
        if (solHLIBpro) MpfFree(solHLIBpro) ;
        solHLIBpro=NULL ;
      }
      temps_final = getTime ();
      temps_cpu = (temps_final - temps_initial) ;
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuGEMV = %f \n", temps_cpu) ;
      if (verboseTest) {
        ierr = displayArray("solHLIBpro", solHLIBpro, nbPts, nbRHS) ; CHKERRQ(ierr) ;
      }

      /* Compare les 2 produits matrice-vecteur */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
      ierr=computeRelativeError(solHLIBpro, solCLA, relative_error); CHKERRQ(ierr);

      Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);
      if (solHLIBpro) MpfFree(solHLIBpro) ;
      solHLIBpro=NULL ;
      break;
    case _solveHLIBPRO:

      /* Factorize the H-matrix */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Factorizing HLIBpro...\n") ;
      temps_initial = getTime ();

      acc = hlib_acc_fixed_eps( recompressionEpsilon );
      hlib_linearoperator_t hfacto=NULL;
      switch(factorization_type){
        case hmat_factorization_ldlt:
          hfacto = hlib_matrix_ldl_inv( hmatrix, acc, & info ); CHECK_INFO;
          break;
        case hmat_factorization_llt:
          hfacto = hlib_matrix_ll_inv( hmatrix, acc, & info ); CHECK_INFO;
          break;
        case hmat_factorization_lu:
          hfacto = hlib_matrix_lu_inv( hmatrix, acc, & info ); CHECK_INFO;
          break;
        default:break;
      }

      temps_final = getTime ();
      temps_cpu = (temps_final - temps_initial) ;
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuFacto = %f \n", temps_cpu) ;

      /* Compute & display some informations */
      if (computeNorm) {
        temps_initial = getTime ();
        Mpf_printf(MPI_COMM_WORLD, "Norm: %.16g\n", hlib_matrix_norm_frobenius( hmatrix, & info ));CHECK_INFO;
        temps_final = getTime ();
        temps_cpu = temps_final - temps_initial ;
        Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuNormFacto = %f \n", temps_cpu) ;
      }
      size_t bytesize = hlib_matrix_bytesize( hmatrix, & info ); CHECK_INFO;
      size_t fullsize = (size_t) nbPts*nbPts*sSize/(symMatSolver?2.:1.);
      Mpf_printf(MPI_COMM_WORLD, "Compressed size: %ld\n", bytesize/sSize);
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> FactorizedSizeMb = %f \n", (double)bytesize/1024./1024.) ;
      Mpf_printf(MPI_COMM_WORLD, "Uncompressed size: %ld\n", fullsize/sSize);
      Mpf_printf(MPI_COMM_WORLD, "Ratio: %g\n", (double)bytesize/fullsize);

      /* Solve the system : A-1.solCLA -> solCLA */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Solve HLIBpro...\n") ;
      temps_initial = getTime ();

      for (i=0 ; i<nbRHS ; i++) {
        /* import the data into vec_in */
        stype==DOUBLE_COMPLEX ?
              hlib_vector_cimport( vec_in, (void*)((char*)solCLA+(size_t)i*nbPts*sSize), & info ) :
              hlib_vector_import( vec_in, (void*)((char*)solCLA+(size_t)i*nbPts*sSize), & info ); CHECK_INFO;
        /* bring into H-ordering */
        hlib_vector_permute( vec_in, hlib_clt_perm_e2i( ct, & info ), & info ); CHECK_INFO;
        /* Do the Solve */
        hlib_vector_fill(vec_out, 0., &info); CHECK_INFO;
        hlib_linearoperator_apply_add( 1.0, hfacto, vec_in, vec_out, HLIB_MATOP_NORM, & info ); CHECK_INFO;
        /* bring back from H-ordering */
        hlib_vector_permute( vec_out, hlib_clt_perm_i2e( ct, & info ), & info ); CHECK_INFO;
        /* export the data out of vec_out */
        stype==DOUBLE_COMPLEX ?
              hlib_vector_cexport( vec_out, (void*)((char*)solCLA+(size_t)i*nbPts*sSize), & info ) :
              hlib_vector_export( vec_out, (void*)((char*)solCLA+(size_t)i*nbPts*sSize), & info ); CHECK_INFO;
      }

      temps_final = getTime ();
      temps_cpu = (temps_final - temps_initial) ;
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuSolve = %f \n", temps_cpu) ;

      /* Compare the two vectors solCLA and rhs */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
      ierr=computeRelativeError(solCLA, rhs, relative_error) ; CHKERRQ(ierr) ;

      Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);
      break;
    default:
      break;
  }

  hlib_vector_free( vec_in, & info ); CHECK_INFO;
  hlib_vector_free( vec_out, & info ); CHECK_INFO;
  hlib_matrix_free( hmatrix, & info ); CHECK_INFO;
  hlib_bct_free( bct, & info ); CHECK_INFO;
  hlib_admcond_free( adm, & info ); CHECK_INFO;
  hlib_clt_free( ct, & info ); CHECK_INFO;
  hlib_coord_free( coord, & info ); CHECK_INFO;

  MpfFree(points); points=NULL;
  MpfFree(vertices); vertices=NULL;
  if (myCtx) MpfFree(myCtx) ;
  myCtx=NULL ;

  hlib_done( & info ); CHECK_INFO;

  return 0;
}

#else
int initHLIBPRO(int* argc, char*** argv) {
  return 0;
}
int testHLIBPRO(double* relative_error) {
  Mpf_printf(MPI_COMM_WORLD, "No HLIBpro support.\n") ;
  *relative_error = 0;
  return 0;
}
#endif
