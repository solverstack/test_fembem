#include "main.h"
#include <fenv.h>

#if defined(HAVE_CHAMELEON) && defined(CHAMELEON_USE_HMAT)
#include <chameleon/runtime.h>

int get_worker_id( void )
{
    return RUNTIME_thread_rank( NULL );
}

#endif

/*! \brief Runs the test of H-chameleon matrices : gemv, solve
 */
int testHCHAMELEON(double * relative_error) {
#if defined(HAVE_CHAMELEON) && defined(CHAMELEON_USE_HMAT)
  int ierr, rank, NB, np, dims[2] = { 0, 0 };
  double temps_initial, temps_final, temps_cpu;
  size_t vectorSize = (size_t)nbPts * nbRHS * (complexALGO ? 2 : 1);

  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> HAcaAccuracy = %.4e \n", mpf_hmat_settings.acaEpsilon) ;
  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> HRecompressionAccuracy = %.4e \n", mpf_hmat_settings.epsilon);

  //feenableexcept (FE_INVALID|FE_DIVBYZERO|FE_OVERFLOW) ;
  hmat_set_worker_index_function( get_worker_id );

  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank); CHKERRQ(ierr);

  /* Realise le produit matrice vecteur classique : A.rhs -> solCLA */

  void *solCLA = NULL;
  if ( check_result ) {
    Mpf_printf(MPI_COMM_WORLD, "\n**** Computing Classical product...\n") ;
    //void *solCLA = NULL;
    ierr = produitClassique(&solCLA, MPI_COMM_WORLD) ; CHKERRQ(ierr) ;
  }

  /* Cree la H-Matrice */

  testFEMBEM_initChameleon( stype );
  CHAMELEON_Enable( CHAMELEON_GENERIC ); // Summa is using lacpy that is not supported for hmat yet

  /* Get the default NB parameter */
  ierr = CHAMELEON_Get(CHAMELEON_TILE_SIZE, &NB);

  /* Create a 2D grid of processes */
  ierr = MPI_Comm_size(MPI_COMM_WORLD, &np); CHKERRQ(ierr) ;
  ierr = MPI_Dims_create(np, 2, dims); CHKERRQ(ierr) ;

  temps_initial = getTime ();
  Mpf_printf( MPI_COMM_WORLD, "\n**** Creating H-CHAMELEON...\n" );

  /* We use the hmat interface initialized in MPF (default is starpu, --hmat-seq or --hmat-toyrt to change it) */
  hmat_interface_t *hi = mpf_hmat_settings.interfaces[stype];

  // hmat_factorization_none mean the user did not chose a factorization so we
  // must choose one for him
  if(mpf_hmat_settings.factorization_type == hmat_factorization_none) {
    if(symMatSolver)
      // use LDLT for real matrices
      mpf_hmat_settings.factorization_type = complexALGO ? hmat_factorization_llt : hmat_factorization_ldlt;
    else
      mpf_hmat_settings.factorization_type = hmat_factorization_lu;
  }

  /* Check the coherency between the factorization chosen in MPF (with --hmat-lu/llt/ldlt)
       and the symmetry of the matrix (chosen with --sym/--nosym/--symcont) */
  if (testedAlgo==_solveHCHAMELEON) {
    switch(mpf_hmat_settings.factorization_type){
      case hmat_factorization_ldlt:
      case hmat_factorization_llt:ASSERTQ(symMatSolver); break;
      case hmat_factorization_lu:ASSERTQ(!symMatSolver); break;
      default:break;
    }
  }
  // Assembly driven by chameleon
  HCHAM_desc_t hdescA;
  HCHAMELEON_generate_matrix( chameleonType, NB, dims, &hdescA, hi );
  temps_final = getTime ();
  temps_cpu = temps_final - temps_initial ;

  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuCreation = %f \n", temps_cpu) ;

  if ( verboseTest ) {
    CHAM_desc_t *frA = HCHAMELEON_uncompress_matrix( &hdescA );
    void *lapackA = MpfCalloc( (size_t)nbPts * nbPts, sizeof(D_type) ) ; CHKPTRQ(lapackA);

    ierr = CHAMELEON_Desc2Lap( ChamUpperLower, frA, lapackA, nbPts ); CHKERRQ(ierr);
    ierr = displayArray("frA", lapackA, nbPts, nbPts) ; CHKERRQ(ierr) ;

    MpfFree( lapackA );
    CHAMELEON_Desc_Destroy( &frA );
  }

  /* Compute & display some informations (synchronous) */
  if (computeNorm) {
    temps_initial = getTime ();
    //Mpf_printf(MPI_COMM_WORLD, "Norm: %.16g\n", HCHAMELEON_norm( hdesc ));
    temps_final = getTime ();
    temps_cpu = temps_final - temps_initial ;
    Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuNorm = %f \n", temps_cpu) ;
  }
  if (computeNorm || hmat_get_sync_exec()) {
    hmat_info_t info = HCHAMELEON_getinfo( &hdescA );
    Mpf_printf(MPI_COMM_WORLD, "Compressed size: %ld\n", info.compressed_size);
    Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> AssembledSizeMb = %f \n", (double)info.compressed_size*Mpf_stype_size[stype]/1024./1024.) ;
    Mpf_printf(MPI_COMM_WORLD, "Uncompressed size: %ld\n", info.uncompressed_size);
    Mpf_printf(MPI_COMM_WORLD, "Ratio: %g\n", (double)info.compressed_size/info.uncompressed_size);
  }
  /* Release the FEM matrix (can be quite large) */
  ierr=freeFemMatrix();CHKERRQ(ierr);

  /* Switch on the various H-CHAMELEON tests */

  switch(testedAlgo) {
    case _gemmHCHAMELEON:
	{
	    HCHAM_desc_t hdescB;
	    HCHAMELEON_generate_matrix( chameleonType, NB, dims, &hdescB, hi );
	    HCHAM_desc_t hdescC;
	    HCHAMELEON_generate_matrix( chameleonType, NB, dims, &hdescC, hi );

	    /* ===================================================================================== */
	    /* Appelle le produit mat.vec : A.rhs -> solHMAT */
	    /* ===================================================================================== */
	    Mpf_printf(MPI_COMM_WORLD, "\n**** Computing HMATxHMAT product...\n") ;

	    temps_initial = getTime ();

	    /* Compute the mat.vec product A.rhs -> solCHAM */
	    if ( generate_traces ) {
		CHAMELEON_Enable( CHAMELEON_PROFILING_MODE );
	    }
	    CHAMELEON_gemm_Tile( hdescA.super, hdescB.super, hdescC.super );
	    if ( generate_traces ) {
		CHAMELEON_Disable( CHAMELEON_PROFILING_MODE );
	    }

	    temps_final = getTime ();
	    temps_cpu = temps_final - temps_initial;
	    Mpf_printf( MPI_COMM_WORLD,"<PERFTESTS> TpsCpuGEMM = %f \n", temps_cpu );

	    HCHAMELEON_destroy_matrix( &hdescB );
	    HCHAMELEON_destroy_matrix( &hdescC );
	}
	break;

    case _gemvHCHAMELEON:

      /* Appelle le produit mat.vec : A.rhs -> solCHAM */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Computing H-CHAMELEON product...\n") ;
      void *solCHAM = NULL;

      temps_initial = getTime ();
      if ( rank == 0 ) {
        solCHAM = MpfCalloc( vectorSize, sizeof(D_type) ); CHKPTRQ(solCHAM);

        if ( simplePrec ) {
          doubleToSimple( rhs, vectorSize );
        }

        /**
             * Apply permutation to rhs before computations
             * No permutation is applied to Y as we compute y = A * x  (beta = 0.)
             */
        HCHAMELEON_lapmr( 'F', 'N', nbRHS, &hdescA, rhs );
      }

      // CHAMELEON descriptor containing rhs
      CHAM_desc_t *descX;
      ierr = CHAMELEON_Desc_Create( &descX, CHAMELEON_MAT_ALLOC_GLOBAL, chameleonType,
                                    NB, NB, NB*NB,
                                    nbPts, nbRHS, 0, 0,
                                    nbPts, nbRHS, dims[0], dims[1] ); CHKERRQ(ierr);

      ierr = CHAMELEON_Lap2Desc( ChamUpperLower, rhs, nbPts, descX); CHKERRQ(ierr);

      // CHAMELEON descriptor containing A.rhs
      CHAM_desc_t *descY;
      ierr = CHAMELEON_Desc_Create( &descY, CHAMELEON_MAT_ALLOC_GLOBAL, chameleonType,
                                    NB, NB, NB*NB,
                                    nbPts, nbRHS, 0, 0,
                                    nbPts, nbRHS, dims[0], dims[1] ); CHKERRQ(ierr);

      /* Compute the mat.vec product A.rhs -> solCHAM */
      if ( generate_traces ) {
        CHAMELEON_Enable( CHAMELEON_PROFILING_MODE );
      }
      CHAMELEON_gemm_Tile( hdescA.super, descX, descY );
      if ( generate_traces ) {
        CHAMELEON_Disable( CHAMELEON_PROFILING_MODE );
      }

      /* convert back to lapack format */
      ierr = CHAMELEON_Desc2Lap( ChamUpperLower, descY, solCHAM, nbPts ); CHKERRQ(ierr);

      ierr = CHAMELEON_Desc_Destroy (&descX); CHKERRQ(ierr);
      ierr = CHAMELEON_Desc_Destroy (&descY); CHKERRQ(ierr);

      /* Apply backward permutation to rhs and solCHAM */
      if ( rank == 0 ) {
        HCHAMELEON_lapmr( 'B', 'N', nbRHS, &hdescA, rhs     );
        HCHAMELEON_lapmr( 'B', 'T', nbRHS, &hdescA, solCHAM );

        //ierr = MPI_Bcast(solCHAM, nbRHS*nbPts, Mpf_mpitype[stype], 0, MPI_COMM_WORLD ); CHKERRQ(ierr);

        if (simplePrec) {
          simpleToDouble(solCHAM, vectorSize);
          simpleToDouble(rhs, vectorSize);
        }
      }

      temps_final = getTime ();
      temps_cpu = temps_final - temps_initial;
      Mpf_printf( MPI_COMM_WORLD,"<PERFTESTS> TpsCpuGEMV = %f \n", temps_cpu );

      if ( verboseTest ) {
        ierr = displayArray("solCHAM", solCHAM, nbPts, nbRHS) ; CHKERRQ(ierr) ;
      }

      /* Compare les 2 produits matrice-vecteur */

      if ( check_result ) {
        Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
        ierr = computeRelativeError( solCHAM, solCLA, relative_error); CHKERRQ(ierr);
        Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);
      }
      if ( solCHAM ) MpfFree( solCHAM );
      solCHAM = NULL;

      break;

    case _solveHCHAMELEON:

      /* Factorize the H-matrix */

      Mpf_printf(MPI_COMM_WORLD, "\n\n**** Factorizing H-CHAMELEON...\n") ;
      if ( generate_traces ) {
        CHAMELEON_Enable( CHAMELEON_PROFILING_MODE );
      }
      temps_initial = getTime ();
      if ( symMatSolver ) {
        ierr = CHAMELEON_sytrf_Tile( ChamLower, hdescA.super ); CHKERRQ(ierr);
      } else {
        ierr = CHAMELEON_getrf_nopiv_Tile( hdescA.super ); CHKERRQ(ierr);
      }
      temps_final = getTime ();
      temps_cpu = temps_final - temps_initial ;
      if ( generate_traces ) {
        CHAMELEON_Disable( CHAMELEON_PROFILING_MODE );
      }
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuFacto = %f \n", temps_cpu) ;

      if ( verboseTest ) {
        CHAM_desc_t *frA = HCHAMELEON_uncompress_matrix( &hdescA );
        void *lapackA = MpfCalloc( (size_t)nbPts * nbPts, sizeof(D_type) ) ; CHKPTRQ(lapackA);

        ierr = CHAMELEON_Desc2Lap( ChamUpperLower, frA, lapackA, nbPts ); CHKERRQ(ierr);
        ierr = displayArray("hChamA", lapackA, nbPts, nbPts) ; CHKERRQ(ierr) ;

        MpfFree( lapackA );
        CHAMELEON_Desc_Destroy( &frA );
      }

      /* Compute & display some informations (synchronous) */
      if ( computeNorm ) {
        temps_initial = getTime ();
        //Mpf_printf(MPI_COMM_WORLD, "Norm (factorized): %.16g\n", hi->norm(hmatrix));
        temps_final = getTime ();
        temps_cpu = temps_final - temps_initial ;
        Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuNormFacto = %f \n", temps_cpu) ;
      }
      if (computeNorm || hmat_get_sync_exec()) {
        hmat_info_t info = HCHAMELEON_getinfo( &hdescA );
        Mpf_printf(MPI_COMM_WORLD, "Compressed size: %ld\n", info.compressed_size);
        Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> FactorizedSizeMb = %f \n", (double)info.compressed_size*Mpf_stype_size[stype]/1024./1024.) ;
        Mpf_printf(MPI_COMM_WORLD, "Uncompressed size: %ld\n", info.uncompressed_size);
        Mpf_printf(MPI_COMM_WORLD, "Ratio: %g\n", (double)info.compressed_size/info.uncompressed_size);
      }

      /* Solve the system : A-1.solCLA -> solCLA */

      if ( check_result ) {
        Mpf_printf(MPI_COMM_WORLD, "\n**** Solve H-CHAMELEON\n") ;

        if ( verboseTest ) {
          ierr = displayArray("b", solCLA, nbPts, nbRHS) ; CHKERRQ(ierr) ;
        }

        // CHAMELEON descriptor containing solCLA
        CHAM_desc_t *descB;
        ierr = CHAMELEON_Desc_Create( &descB, CHAMELEON_MAT_ALLOC_GLOBAL, chameleonType,
                                      NB, NB, NB*NB,
                                      nbPts, nbRHS, 0, 0,
                                      nbPts, nbRHS, 1, np ); CHKERRQ(ierr);
        if ( simplePrec ) {
          doubleToSimple(solCLA, vectorSize);
        }

        /* Apply H-Matrix permutation, and convert to tile */
        HCHAMELEON_lapmr( 'F', 'N', nbRHS, &hdescA, solCLA );
        ierr = CHAMELEON_Lap2Desc( ChamUpperLower, solCLA, nbPts, descB); CHKERRQ(ierr);

        /* Solve the system A-1.solCLA -> solCLA */
        temps_initial = getTime ();
        if (symMatSolver) {
          ierr = CHAMELEON_sytrs_Tile( ChamLower, hdescA.super, descB ); CHKERRQ(ierr);
        } else {
          ierr = CHAMELEON_getrs_nopiv_Tile( hdescA.super, descB ); CHKERRQ(ierr);
        }
        temps_final = getTime ();
        temps_cpu = temps_final - temps_initial ;
        Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuSolve = %f \n", temps_cpu) ;

        /* Convert back to lapack, and apply reverse H-Matrix permutation */
        ierr = CHAMELEON_Desc2Lap( ChamUpperLower, descB, solCLA, nbPts); CHKERRQ(ierr);
        HCHAMELEON_lapmr( 'B', 'N', nbRHS, &hdescA, solCLA );

        ASSERTQ(nbPts < INT_MAX/nbRHS);
        ierr = MPI_Bcast(solCLA, nbRHS*nbPts, Mpf_mpitype[stype], 0, MPI_COMM_WORLD ); CHKERRQ(ierr);
        if ( simplePrec ) {
          simpleToDouble( solCLA, vectorSize );
        }

        ierr = CHAMELEON_Desc_Destroy (&descB); CHKERRQ(ierr);

        if ( verboseTest ) {
          ierr = displayArray("x", solCLA, nbPts, nbRHS) ; CHKERRQ(ierr) ;
        }

        /* Compare the two vectors solCLA and rhs */
        Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
        ierr=computeRelativeError(solCLA, rhs, relative_error) ; CHKERRQ(ierr) ;
        Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> ForwardError = %.4e \n", *relative_error);
        Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> ScaledForwardError = %.4e \n", *relative_error / mpf_hmat_settings.acaEpsilon );
      }
      break ;
    default:
      break;
  }

  /* Finish execution */

  HCHAMELEON_destroy_matrix( &hdescA );
  if (solCLA) MpfFree(solCLA) ;
  solCLA=NULL ;
  hmat_tracing_dump("testHCHAMELEON_trace.json");

#else
#if !defined(CHAMELEON_USE_HMAT)
  Mpf_printf(MPI_COMM_WORLD, "No HMat support in CHAMELEON.\n");
#endif
#if !defined(HAVE_CHAMELEON)
  Mpf_printf(MPI_COMM_WORLD, "No CHAMELEON support.\n") ;
#endif
#endif

  return 0;
}
