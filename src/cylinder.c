#include "main.h"
#include <mpf_simd.h>

/*! \brief Radius of the cylinder */
double radius = 2.0 ;

/*! \brief Height of the cylinder */
double height = 4.0 ;

/*! \brief Mesh step (average distance between two points) */
double meshStep ;

/*! \brief Height of the detail on the cylinder counted in number of lambda */
double heightDetail = 0.5 ;

/*! \brief Raduis of the detail on the cylinder counted in number of lambda */
double radiusDetail = 0.02 ;

/*! \brief Mesh step of the detail (average distance between two points) */
double meshStepDetail ;

/*! \brief Number of points per loops in the BEM mesh (main cylinder) */
static int nbPtsLoop=0;

/*! \brief Number of points per loops in the BEM mesh (detail cylinder) */
static int nbPtsLoopDetail=0;

/*! \brief Computes coordinates of point number 'i'

  The points are computed on a cylinder defined by 'radius' and 'height'. The number of points is given by nbPtsMain.
  Points are located on a helix, equally spaced in direction z and orthoradial.
  \a i should be in [0, nbPtsMain[.
  \param i the number of the point (input)
  \param coord the coordinates x y z of this point (output)
  \return 0 for success
*/
inline static int computeCoordCylinderMain(int i, double *coord) {
  ASSERTQ(testedModel == _bem);
  static double angleStep=0. ;
  static double zStep=0. ;
  double theta ;

  if (i<0 || i>=nbPtsMain) {
    SETERRQ(1, "Incorrect unknown number %d, should be in [0, %d[\n", i, nbPtsMain) ;
  }

  if (angleStep==0.) {
    /* zStep and angleStep are defined by :
       angleStep*radius = zStep*nbPtsLoop (= meshStep)
       angleStep*nbPtsLoop = 2.pi */
      zStep=height/(double)(nbPtsMain-1) ;
    angleStep=sqrt(zStep*2.*M_PI/radius) ;
    meshStep=zStep*2.*M_PI/angleStep ;

    nbPtsLoop=floor(2.*M_PI/angleStep) ; // Used to write VTK file
    // Once I have 'nbPtsLoop' as an integer, I recompute meshStep and angleStep
    angleStep = 2.*M_PI/(double)nbPtsLoop;
    meshStep = angleStep*radius;
  }

  theta = (double)i * angleStep ;
  mpf_simd_sincos(theta, coord, coord+1);
  coord[0] *= radius;
  coord[1] *= radius;
  coord[2] = (double)i*zStep ;

  return 0 ;
}

/*! \brief Computes coordinates of point number 'i' of the detail placed on the main cylinder.

  The points are computed on a cylinder defined by 'heightDetail' and 'radiusDetail' (values in number of wavelength).
  The number of points is given by nbPts - nbPtsMain.
  Points are located on a helix, equally spaced in direction y and orthoradial.
  \a i should be in [0, nbPts - nbPtsMain[.
  \param i the number of the point in the detail (input)
  \param coord the coordinates x y z of this point (output)
  \return 0 for success
*/
inline static int computeCoordCylinderDetail(int i, double *coord) {
  ASSERTQ(testedModel == _bem);
  static double angleStep = 0. ;
  static double zStep = 0. ;
  static double heightD = 0.;
  static double radiusD = 0.;
  double theta ;

  if (i<0 || i>=(nbPts - nbPtsMain)) {
    SETERRQ(1, "Incorrect unknown number %d, should be in [0, %d[\n", i, nbPts - nbPtsMain) ;
  }

  if (angleStep == 0.) {
      heightD = lambda * heightDetail ;
      radiusD = lambda * radiusDetail ;
    /* zStep and angleStep are defined by :
       angleStep*radius = zStep*nbPtsLoopDetail (= meshStep)
       angleStep*nbPtsLoopDetail = 2.pi */
    zStep = heightD/(double)(nbPts - nbPtsMain-1) ;
    angleStep = sqrt(zStep*2.*M_PI/radiusD) ;
    meshStepDetail = zStep*2.*M_PI/angleStep ;

    nbPtsLoopDetail=floor(2.*M_PI/angleStep) ; // Used to write VTK file
    // Once I have 'nbPtsLoopDetail' as an integer, I recompute meshStepDetail and angleStep
    angleStep = 2.*M_PI/(double)nbPtsLoopDetail;
    meshStepDetail = angleStep*radius;
  }

  /* Le detail est oriente selon y, perpendiculaire au cylindre principal, a mi-hauteur */
  theta = (double)i * angleStep ;
  mpf_simd_sincos(theta, coord, coord+2);
  coord[0] *= radiusD ;
  coord[1] = zStep * (double)(i+1) + radius ; // 'i+1' instead of 'i' to avoid that the detail touches the main cylinder
  coord[2] = radiusD * coord[2] + 0.5 * height ;

  return 0 ;
}

/*! \brief Computes coordinates of point number 'i' on the object (main cylinder or detail).

  \a i should be in [0, nbPts[.
  \param i the number of the point in the detail (input)
  \param coord the coordinates x y z of this point (output)
  \return 0 for success
*/
MPF_SIMD_DISP(int computeCoordCylinder(int i, double *coord)) {
    ASSERTQ(i<nbPtsBEM && i>=0);
    if (testedModel == _fembem)
        return computeCoordPipe(i, coord);
    if (useDetail && i >= nbPtsMain) {
        i = i - nbPtsMain;
        return computeCoordCylinderDetail(i, coord);
    } else {
        return computeCoordCylinderMain(i, coord);
    }
}

double* createCylinder(void) {
  double* result = MpfCalloc((size_t)3 * nbPts, sizeof(double));
  int i, ierr;
#pragma omp parallel for private(ierr) schedule(dynamic,10000)
  for (i = 0; i < nbPts; i++) {
    ierr=computeCoordCylinder(i, &(result[(size_t)3*i]) ) ; CHKERRA(ierr) ;
  }

  int rank;
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank==0 && writeMesh) {
    FILE *fvtk=fopen("mesh.vtu", "w");
    // VTK Header
    fprintf(fvtk, "<?xml version=\"1.0\"?>\n");
    fprintf(fvtk, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" "
                  "byte_order=\"LittleEndian\" compressor=\"vtkZLibDataCompressor\">\n");
    fprintf(fvtk, "\t<UnstructuredGrid>\n");

    ASSERTA(nbPtsMain-nbPtsLoop-1 < INT_MAX/2);
    int nbTria = 2*(nbPtsMain-nbPtsLoop-1);
    if (nbPtsLoopDetail>0) {
      ASSERTA(nbTria < INT_MAX-2*(nbPts-nbPtsLoopDetail-nbPtsMain-1));
      nbTria += 2*(nbPts-nbPtsLoopDetail-nbPtsMain-1);
    }
    fprintf(fvtk, "\t\t<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n", nbPts, nbTria);
    // Write the vertices
    fprintf(fvtk, "\t\t\t<Points>\n");
    fprintf(fvtk, "\t\t\t\t<DataArray type=\"Float32\""
                  " NumberOfComponents=\"3\" format=\"ascii\">\n");
    double* coord=result;
    for (int iVtk = 0; iVtk < nbPts; iVtk++, coord+=3) {
      fprintf(fvtk, "%g %g %g\n", (float) (coord[0]), (float)(coord[1]), (float)(coord[2]));
    }
    fprintf(fvtk, "\t\t\t\t</DataArray>\n");
    fprintf(fvtk, "\t\t\t</Points>\n");
    // VTK Elements
    fprintf(fvtk, "\t\t\t<Cells>\n");
    fprintf(fvtk, "\t\t\t\t<DataArray "
                  "type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");
    /* Loop on the triangles (main cylinder) */
    for (int iVtk = 0; iVtk+nbPtsLoop+1 < nbPtsMain; iVtk++) {
      // 1st triangle
      fprintf(fvtk, "%d %d %d\n", iVtk, iVtk+1, iVtk+nbPtsLoop);
      // 2nd Triangle
      fprintf(fvtk, "%d %d %d\n", iVtk+1, iVtk+nbPtsLoop, iVtk+nbPtsLoop+1);
    }
    /* Loop on the triangles (detail) */
    for (int iVtk = nbPtsMain; iVtk+nbPtsLoopDetail+1 < nbPts; iVtk++) {
      // 1st triangle
      fprintf(fvtk, "%d %d %d\n", iVtk, iVtk+1, iVtk+nbPtsLoopDetail);
      // 2nd Triangle
      fprintf(fvtk, "%d %d %d\n", iVtk+1, iVtk+nbPtsLoopDetail, iVtk+nbPtsLoopDetail+1);
    }
    fprintf(fvtk, "\t\t\t\t</DataArray>\n");

    /* Ecriture des offsets */
    fprintf(fvtk, "\t\t\t\t<DataArray "
                  "type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
    for (int iVtk = 0; iVtk < nbTria; iVtk++) {
      fprintf(fvtk, "%d ", iVtk*3+3);
      if (iVtk % 10 == 9)
        fprintf(fvtk, "\n");
    }
    fprintf(fvtk, "\t\t\t\t</DataArray>\n");

    /* Ecriture des types d'elements */
    fprintf(fvtk, "\t\t\t\t<DataArray "
                  "type=\"UInt8\" Name=\"types\" format=\"ascii\">\n");
    for (int iVtk = 0; iVtk < nbTria; iVtk++) {
      fprintf(fvtk, "%d ", 5); // For VTK_TRIANGLE
      if (iVtk % 10 == 9)
        fprintf(fvtk, "\n");
    }
    fprintf(fvtk, "\t\t\t\t</DataArray>\n");
    fprintf(fvtk, "\t\t\t</Cells>\n");
    fprintf(fvtk, "\t\t</Piece>\n\t</UnstructuredGrid>\n</VTKFile>\n");
    fclose(fvtk);
    Mpf_printf(MPI_COMM_WORLD, "Done writing 'mesh.vtu'\n") ;
  }

  if (rank==0 && writeMeshUNV) {
#define sNODE_UNV_ID "  2411"
#define sELT_UNV_ID  "  2412"
#define sGRP_UNV_ID  "  2435"
#define sUNV_SEPARATOR    "    -1"
#define sNODE_UNV_DESCR   "         1         1        11"
#define sELT_TRIA3_DESC   "        91         1         1         5         3"
#define sELT_TETRA4_DESC  "       111         1         2         9         4"
    Mpf_printf(MPI_COMM_WORLD, "Writing 'mesh.unv'... ") ;

    FILE *funv=fopen("mesh.unv", "w");
    /* -------- */
    /* Vertices */
    /* -------- */
    fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
    fprintf(funv, "%s\n", sNODE_UNV_ID );
    double* coord=result;
    for( i = 1; i<=nbPts; i++, coord+=3 ) {
      fprintf(funv, "%10d%s\n", i, sNODE_UNV_DESCR );
      fprintf(funv, "%25.16E%25.16E%25.16E\n", coord[0], coord[1], coord[2]) ;
    }
    fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
    /* -------- */
    /* Elements */
    /* -------- */
    fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
    fprintf(funv, "%s\n", sELT_UNV_ID );
    int nbTria=0;
    /* Loop on the triangles (main cylinder) */
    for (int iVtk = 1; iVtk+nbPtsLoop+1 <= nbPtsMain; iVtk++) {
      // 1st triangle
      fprintf(funv, "%10d%s\n", ++nbTria, sELT_TRIA3_DESC ) ;
      fprintf(funv, "%10d%10d%10d\n", iVtk, iVtk+1, iVtk+nbPtsLoop) ;
      // 2nd Triangle
      fprintf(funv, "%10d%s\n", ++nbTria, sELT_TRIA3_DESC ) ;
      fprintf(funv, "%10d%10d%10d\n", iVtk+1, iVtk+nbPtsLoop, iVtk+nbPtsLoop+1);
    }
    /* Loop on the triangles (detail) */
    for (int iVtk = nbPtsMain+1; iVtk+nbPtsLoopDetail+1 <= nbPts; iVtk++) {
      // 1st triangle
      fprintf(funv, "%10d%s\n", ++nbTria, sELT_TRIA3_DESC ) ;
      fprintf(funv, "%10d%10d%10d\n", iVtk, iVtk+1, iVtk+nbPtsLoopDetail) ;
      // 2nd Triangle
      fprintf(funv, "%10d%s\n", ++nbTria, sELT_TRIA3_DESC ) ;
      fprintf(funv, "%10d%10d%10d\n", iVtk+1, iVtk+nbPtsLoopDetail, iVtk+nbPtsLoopDetail+1);
    }
    fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
    /* ------ */
    /* Groups */
    /* ------ */
    fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
    fprintf(funv, "%s\n", sGRP_UNV_ID );
    /* Group C_EXT for all the triangles */
    fprintf(funv, "%10d%10d%10d%10d%10d%10d%10d%10d\n", 1, 0, 0, 0, 0, 0, 0, nbTria) ;
    fprintf(funv, "C_EXT\n") ;
    for (i=1 ; i<=nbTria ; i++) {
      fprintf(funv, "%10d%10d%10d%10d", 8, i, 0, 0) ;
      if ( (i%2==0) || (i==nbTria) )
        fprintf(funv, "\n") ;
    }
    fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
    fclose(funv);
    Mpf_printf(MPI_COMM_WORLD, "Done.\n") ;
  }

  // In case we pass a second time in this routine
  writeMesh = 0;
  writeMeshUNV = 0;

  return result;
}

/*! \brief Reads command line arguments defining the shape of the cylinder

  \return 0 for success
*/
int initCylinder(int *argc, char ***argv) {
  int ierr ;
  double coord[3] ;

  if (MpfArgGetDouble(argc, *argv, 1, "-radius", &radius)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading radius = %f\n", radius) ;
  }
  if (MpfArgGetDouble(argc, *argv, 1, "-height", &height)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading height = %f\n", height) ;
  }

  /* Calcule les coordonnees d'un point, afin de fixer meshStep (qui peut servir a fixer lambda) */
  ierr=computeCoordCylinder(0, &(coord[0])) ; CHKERRQ(ierr) ;

  return 0 ;
}

int initCylinderDetail(int *argc, char ***argv) {
  int ierr ;
  double coord[3] ;
  double step=0;

  // ATTENTION : LES MESURES DU CYLINDRE DU DETAIL SONT EN NOMBRE DE LAMBDA

  if (MpfArgGetDouble(argc, *argv, 1, "-radius_detail", &radiusDetail)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading radius of the detail = %f\n", radiusDetail) ;
  }
  if (MpfArgGetDouble(argc, *argv, 1, "-height_detail", &heightDetail)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading height of the detail = %f\n", heightDetail) ;
  }
  if (MpfArgGetDouble(argc, *argv, 1, "-ptsperlambda_detail", &ptsPerLambdaDetail)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading Points per wavelength of the detail = %f\n", ptsPerLambdaDetail) ;
  }

  if (ptsPerLambdaDetail == -1) {
    getMeshStep(&step);
    ptsPerLambdaDetail = 10*lambda/step; // par defaut, on est 10 fois plus maille
  }

  step = lambda/ptsPerLambdaDetail;

  /* Nombre de points sur le detail = superficie du cylindre (2.pi.R.h.lambda^2) divise par superficie d'une "maille" (step^2) */
  nbPts += ceil(2*M_PI*lambda*lambda*heightDetail*radiusDetail/(step*step));
  nbPtsBEM = nbPts ;

  /* Calcule les coordonnees d'un point, afin de fixer meshStepDetail */
  ierr = computeCoordCylinder(nbPtsMain, &(coord[0]) ) ; CHKERRQ(ierr) ;

  return 0 ;
}

int getMeshStep(double *m) {
  if (meshStep==0.)
    SETERRQ(1, "meshStep not yet initialized. Come back later.") ;

  *m = meshStep ;
  return 0 ;
}

int getMeshStepDetail(double *m) {
  if (meshStepDetail == 0.)
    SETERRQ(1, "meshStepDetail not yet initialized. Come back later.") ;

  *m = meshStepDetail ;
  return 0 ;
}
