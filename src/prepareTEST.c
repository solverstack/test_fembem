#include "main.h"

int prepareTEST(void) {
  enter_context("prepareTEST()");
  double step;
  int ierr;

  Mpf_printf(MPI_COMM_WORLD, "Number of points for the test = %d\n", nbPts) ;
  Mpf_printf(MPI_COMM_WORLD, "Number of right hand sides for the test = %d\n", nbRHS) ;

  getMeshStep(&step) ;

  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> StepMesh = %e\n" , step);
  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> NbPts = %d \n", nbPts);
  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> NbPtsBEM = %d \n", nbPtsBEM);
  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> NbPtsFEM = %d \n", nbPts-nbPtsBEM);
  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> NbRhs = %d \n", nbRHS);
  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> nbPtLambda = %e \n", ptsPerLambda);
  if (useDetail)
    Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> nbPtLambdaDetail = %e \n", ptsPerLambdaDetail);
  Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Lambda = %e \n", lambda);

  /* Cree le second membre du produit mat-vec */
  Mpf_printf(MPI_COMM_WORLD, "Computing RHS...\n") ;
  rhs = MpfCalloc((size_t)nbPts*nbRHS, complexALGO ? sizeof(Z_type) : sizeof(D_type)) ;  CHKPTRQ(rhs) ;
  ierr = computeRhs() ; CHKERRQ(ierr) ;
  if (verboseTest) {
    ierr = displayArray("rhs", rhs, nbPts, nbRHS) ; CHKERRQ(ierr) ;
  }

  leave_context();
  return 0;
}

double getTime() {
  return (time_in_s(get_time())) ;
}

int displayArray(char *s, void *f, int m, int n) {
  int i, j ;
  Z_type *z_f=f ;
  D_type *d_f=f ;

  for (j=0 ; j<n ; j++)
    for (i=0 ; i<m ; i++)
      if (complexALGO)
        Mpf_printf(MPI_COMM_WORLD, "%s[%d, %d]=(%e, %e)\n", s, i, j, z_f[(size_t)j*m+i].r, z_f[(size_t)j*m+i].i) ;
      else
        Mpf_printf(MPI_COMM_WORLD, "%s[%d, %d]=%e\n", s, i, j, d_f[(size_t)j*m+i]) ;
  return 0 ;
}
