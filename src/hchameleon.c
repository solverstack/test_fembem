#include "config.h"

#if defined(HAVE_MKL_H)
#include <mkl.h>
#else
#if !defined(LAPACK_COMPLEX_CPP)
#include <lapacke.h>
#endif
#endif
#include "main.h"

#if defined(HAVE_CHAMELEON) && defined(CHAMELEON_USE_HMAT)

#include <coreblas.h>
#include <pthread.h>

/**
 * Detect if the tile is local or not
 */
#if 0
inline static int chameleon_desc_islocal( const CHAM_desc_t *A, int m, int n )
{
#if defined(CHAMELEON_USE_MPI)
  return (A->myrank == A->get_rankof(A, m, n));
#else
  (void)A; (void)m; (void)n;
  return 1;
#endif /* defined(CHAMELEON_USE_MPI) */
}
#endif

// indices are 0 based, bounds included
/* Initialize a given tile with an h-matrix, given the row and cluster tree */
static int
HCHAMELEON_build_callback_FEMBEM( void *op_args,
                                  cham_uplo_t uplo, int m, int n, int ndata,
                                  const CHAM_desc_t *desc, CHAM_tile_t *tile, ... )
{
  contextTestFEMBEM       ctx_fembem;
  struct mpf_hmat_create_compression_args_t compression_ctx;
  hmat_assemble_context_t ctx_assembly;
  hmat_matrix_t *hmatrix;
  HCHAM_desc_t  *hdesc = (HCHAM_desc_t*)op_args;

  /* fprintf(stderr, "I'm initializing (%d, %d)\n", m, n ); */

  compression_ctx.method = mpf_hmat_settings.compressionMethod;
  compression_ctx.threshold = mpf_hmat_settings.acaEpsilon;
  mpf_hmat_compression(&compression_ctx);
  /**
     * Initialize the assembly context
     */
  hmat_assemble_context_init( &(ctx_assembly) );
  ctx_assembly.progress = &mpf_hmat_settings.progress;
  ctx_assembly.progress->user_data = (void*)MPI_COMM_WORLD;

  ctx_assembly.factorization   = hmat_factorization_none;
  ctx_assembly.lower_symmetric = ( m == n ) ? symMatSolver : 0;

  ctx_assembly.compression = (hmat_compression_algorithm_t*) compression_ctx.output;

  /* if ( use_simple_assembly ) { */
  ctx_assembly.simple_compute = interactionKernel;
  /* } else { */
  /* 	ctx_assembly.prepare          = prepare_hmat; */
  /* 	ctx_assembly.advanced_compute = advanced_compute_hmat; */
  /* } */

  memset( &ctx_fembem, 0, sizeof(contextTestFEMBEM) );
  ctx_fembem.colDim = nbPts;
  ctx_assembly.user_context = &ctx_fembem;

  /* The offset is stored in the submatrix, so let's keep it to 0 */
  ctx_fembem.colOffset = 0;
  ctx_fembem.rowOffset = 0;
  hmatrix = hdesc->hi->get_child( hdesc->hmatrix, m, n );

  assert( hmatrix );
  hdesc->hi->assemble_generic( hmatrix, &ctx_assembly );

  tile->format = CHAMELEON_TILE_HMAT;
  tile->mat    = hmatrix;

  hmat_delete_compression(ctx_assembly.compression);

  return 0;
}

int HCHAMELEON_generate_matrix( cham_flttype_t flttype, int NB, int PQ[2],
HCHAM_desc_t     *hdescA,
hmat_interface_t *hi )
{
  CHAM_desc_t *descA;
  hmat_cluster_tree_builder_t *ct_builder;
  hmat_admissibility_param_t admissibility_param;
  hmat_cluster_tree_t **clusters;
  CHAM_tile_t *tile;
  double *points;
  int i, j, ierr, nbnode = 0;
  int N = nbPts;

  hdescA->hi = hi;

  /**
     * Let's first create the Chameleon descriptor
     */
  ierr = CHAMELEON_Desc_Create( &(hdescA->super), CHAMELEON_MAT_ALLOC_TILE, flttype,
                                NB, NB, NB*NB,  // Tiles' dimensions
                                N, N,           // Matrix's dimension
                                0, 0, N, N,     // Work with the whole matrix
                                PQ[0], PQ[1] ); // number of rows and columns of the 2D distribution grid
  CHKERRQ(ierr);

  descA = hdescA->super;
  ASSERTA( descA->lmt == descA->lnt );

  tile = descA->tiles;
  for(j=0; j < descA->lnt; j++) {
    for(i=0; i < descA->lmt; i++, tile++) {
      tile->format = CHAMELEON_TILE_HMAT;
      tile->mat = NULL;
    }
  }

  /**
     * Define problem points coords
     */
  points = testedModel == _bem ? createCylinder() : createPipe();

  /**
     * Create the admissibility condition parameter
     */
  hmat_init_admissibility_param(&admissibility_param);
  admissibility_param.eta = 3.0;
  hdescA->admissibilityCondition = hmat_create_admissibility( &admissibility_param );

  /**
     * Initialize the clustering method
     */
  hmat_clustering_algorithm_t *ntd_clustering = hmat_create_clustering_ntilesrecursive( NB );
  hmat_clustering_algorithm_t *med_clustering = hmat_create_clustering_median();
  hmat_set_clustering_divider( med_clustering, divider );

  ct_builder = hmat_create_cluster_tree_builder( ntd_clustering );
  hmat_cluster_tree_builder_add_algorithm( ct_builder, 1, med_clustering );

  hmat_delete_clustering( ntd_clustering );
  hmat_delete_clustering( med_clustering );

  /**
     * Create the cluster tree
     *   We exploit the fact that row and column distributions are
     *   identical to initialize only on rows.
     */
  hdescA->perm = MpfCalloc( descA->lm, sizeof(int) );

  hmat_cluster_tree_t *main_cluster = (hmat_cluster_tree_t *) hmat_create_cluster_tree_from_builder( points, 3, nbPts,
                                                                                ct_builder );

  /* Let's copy the permutation array as we need it in fortran numbering for th permutation */
  memcpy( hdescA->perm,hmat_cluster_get_indices( main_cluster ),
          descA->lm * sizeof(int) );

  hdescA->clusters = MpfCalloc( descA->lmt + 1, sizeof(hmat_cluster_tree_t *) );
  clusters = hdescA->clusters;
  if ( descA->lmt == 1 ) {
    *clusters = main_cluster;
    clusters++;
  }
  else {
    for( i=0; i < descA->lmt; i++, clusters++ ) {
      *clusters = (hmat_cluster_tree_t *) hmat_cluster_get_son( main_cluster, i );
    }
  }
  *clusters = main_cluster;
  nbnode = hmat_tree_nodes_count( main_cluster );

  hdescA->hmatrix = hi->create_empty_hmatrix_admissibility( main_cluster,
                                                            main_cluster,
                                                            symMatSolver,
                                                            hdescA->admissibilityCondition );

  hi->set_low_rank_epsilon(hdescA->hmatrix, mpf_hmat_settings.epsilon);

  for( i=0; i < descA->lm; i++ ) {
    hdescA->perm[i]++;
  }

  hmat_delete_cluster_tree_builder( ct_builder );
  MpfFree(points);
  Mpf_printf(MPI_COMM_WORLD, "ClusterTree node count = %d\n", nbnode);

  /**
     * Assemble h-tiles using HCHAMELEON_build_callback_FEMBEM() defined above
     */
#if 1
  cham_uplo_t uplo = symMatSolver ? ChamLower : ChamUpperLower;
  cham_map_data_t data;
  cham_map_operator_t op;

  data.access = ChamW;
  data.desc   = descA;

  op.name     = "BuildFEMBEM-H";
  op.cpufunc  = HCHAMELEON_build_callback_FEMBEM;
  op.cudafunc = NULL;
  op.hipfunc  = NULL;

  ierr = CHAMELEON_mapv_Tile( uplo, 1, &data, &op, hdescA );
  CHKERRQ(ierr);
#else
  tile = descA->tiles;
  for(j=0; j < descA->lnt; j++) {
    for(i=0; i < descA->lmt; i++, tile++) {
      if ( !chameleon_desc_islocal( descA, i, j ) ) {
        continue;
      }
      HCHAMELEON_build_callback_FEMBEM( descA, ChamUpperLower, i, j,
                                        tile, hdescA );
    }
  }
#endif
  return 0;
}

int
HCHAMELEON_destroy_matrix( HCHAM_desc_t *hdescA )
{
  CHAM_desc_t *descA = hdescA->super;
  CHAM_tile_t *tile;
  int i, j, ierr;

  hmat_delete_admissibility( hdescA->admissibilityCondition );

  hdescA->hi->dump_info( hdescA->hmatrix, "testHCHAMELEON_matrix");

  /* Destroys the HMAT structure */
  tile = descA->tiles;
  for(j=0; j < descA->lnt; j++) {
    for(i=0; i < descA->lmt; i++, tile++) {
      if ( (tile->format & CHAMELEON_TILE_HMAT) &&
           (tile->mat != NULL) )
      {
        if ( descA->myrank == descA->get_rankof( descA, i, j ) ) {
          hdescA->hi->destroy_child( tile->mat );
        }
        else {
          fprintf( stderr, "It should have been cleaned up by StarPU\n" );
        }
      }
    }
  }
  hdescA->hi->destroy( hdescA->hmatrix );
  hmat_delete_cluster_tree( hdescA->clusters[descA->lmt] );

  ierr = CHAMELEON_Desc_Destroy( &(hdescA->super) );
  CHKERRQ(ierr);

  MpfFree( hdescA->perm );
  hdescA->perm = NULL;

  MpfFree( hdescA->clusters );
  hdescA->clusters = NULL;

  return 0;
}

/* Provides a flat matrix (array of values) given an hmatrix structure */
typedef	void (*core_lacpy_fct_t)( cham_uplo_t, int, int, const void *, int, void *, int );

static void
TCORE_uncompress( HCHAM_desc_t *hdescA,
                  int Am, int An,
                  CHAM_tile_t  *tileA,
                  CHAM_tile_t  *tileB )
{
  int m = tileA->m;
  int n = tileA->n;

  ASSERTA( tileB->format & CHAMELEON_TILE_FULLRANK );
  if ( tileA->format & CHAMELEON_TILE_HMAT ) {
    struct hmat_get_values_context_t ctxt_getval;
    int *row_ptr = malloc(m * sizeof(int));
    int *col_ptr = malloc(n * sizeof(int));
    ASSERTA( tileB->ld == m );

    ctxt_getval.matrix = tileA->mat;
    ctxt_getval.values = tileB->mat;
    memset( ctxt_getval.values, 0, m * n * sizeof(double) );

    /*
         * Offsets are not used by the get_value function, they need
         * to be stored in the row/ptr arrays
         *
         * We get the hmat numbering, meaning PA, and not A, as it is
         * impossible to get the correct numbering to extract a single
         * tile with the actual interface.
         */
    ctxt_getval.row_offset = 0;
    ctxt_getval.col_offset = 0;
    ctxt_getval.row_size   = m;
    ctxt_getval.col_size   = n;
    //ctxt_getval.hmat_numbering = 1; NEED THAT to uncompress correctly

    for (int k=0; k<m; k++) row_ptr[k] = Am * hdescA->super->mb + k + 1;
    for (int k=0; k<n; k++) col_ptr[k] = An * hdescA->super->nb + k + 1;

    ctxt_getval.row_indices = row_ptr;
    ctxt_getval.col_indices = col_ptr;

    hdescA->hi->get_values( &ctxt_getval );

    free(row_ptr);
    free(col_ptr);
  }
  else {
    void (*CORE_lacpy)( cham_uplo_t, int, int,
                       const void *, int, void *, int ) = NULL;
    switch( hdescA->super->dtyp ) {
#if defined(CHAMELEON_PREC_S)
      case ChamRealFloat:
        CORE_lacpy = (core_lacpy_fct_t)CORE_slacpy;
        break ;
#endif
#if defined(CHAMELEON_PREC_D)
      case ChamRealDouble:
        CORE_lacpy = (core_lacpy_fct_t)CORE_dlacpy;
        break ;
#endif
#if defined(CHAMELEON_PREC_C)
      case ChamComplexFloat:
        CORE_lacpy = (core_lacpy_fct_t)CORE_clacpy;
        break ;
#endif
#if defined(CHAMELEON_PREC_Z)
      case ChamComplexDouble:
        CORE_lacpy = (core_lacpy_fct_t)CORE_zlacpy;
        break ;
#endif
      default :
        SETERRQ(1, "HCHAMELEON_uncompress_matrix: unknown scalar type\n") ;
        break ;
    }
    CORE_lacpy( ChamUpperLower, tileA->m, tileA->n,
                tileA->mat, tileA->ld,
                tileB->mat, tileB->ld );
  }
  tileB->format = CHAMELEON_TILE_FULLRANK;
}

CHAM_desc_t *HCHAMELEON_uncompress_matrix( HCHAM_desc_t *hdesc )
{
  CHAM_desc_t *descA = hdesc->super;
  CHAM_desc_t *descB;
  CHAM_tile_t *tileA;
  CHAM_tile_t *tileB;
  int i, j;

  CHAMELEON_Desc_Create( &descB,
                         CHAMELEON_MAT_ALLOC_GLOBAL, descA->dtyp,
                         // Tiles' dimensions
                         descA->mb,
                         descA->nb,
                         descA->bsiz,
                         // Matrix's dimension
                         descA->m,
                         descA->n,
                         // Work with the whole matrix
                         0, 0,
                         descA->m,
                         descA->n,
                         // number of rows and columns of the 2D distribution grid
                         descA->p,
                         descA->q );

  tileA = descA->tiles;
  tileB = descB->tiles;

  // Store in desB all tiles in full rank format
  for(j=0; j < descA->lnt; j++) {
    for(i=0; i < descA->lmt; i++) {
      fprintf( stderr, "Uncompress Tile( %d, %d ) from hmat to full rank\n", i, j );

      TCORE_uncompress( hdesc, i, j, tileA, tileB );

      tileA++; tileB++;
    }
  }

  return descB;
}

typedef struct getinfo_args_s {
  HCHAM_desc_t   *hdesc;
  size_t          compress_size;
  size_t          uncompress_size;
  pthread_mutex_t mutex;
} getinfo_args_t;

static int
HCHAMELEON_map_getinfo( void *op_args,
                        cham_uplo_t uplo, int m, int n, int ndata,
                        const CHAM_desc_t *desc, CHAM_tile_t *tile, ... )
{
  getinfo_args_t *getinfo = (getinfo_args_t*)op_args;
  hmat_info_t     info;

  getinfo->hdesc->hi->get_info( tile->mat, &info );
  pthread_mutex_lock( &(getinfo->mutex) );
  getinfo->compress_size   += info.compressed_size;
  getinfo->uncompress_size += info.uncompressed_size;
  pthread_mutex_unlock( &(getinfo->mutex) );

  (void)ndata;
  (void)uplo;
  (void)m;
  (void)n;
  return 0;
}

hmat_info_t HCHAMELEON_getinfo( HCHAM_desc_t *hdesc )
{
  hmat_info_t ginfo;
  getinfo_args_t op_args =
  {
    .hdesc = hdesc,
    .compress_size = 0,
    .uncompress_size = 0,
    .mutex = PTHREAD_MUTEX_INITIALIZER,
  };
  cham_uplo_t uplo = symMatSolver ? ChamLower : ChamUpperLower;
  cham_map_data_t data;
  cham_map_operator_t op;

  data.access = ChamR;
  data.desc   = hdesc->super;

  op.name     = "HCHAM_getInfo";
  op.cpufunc  = HCHAMELEON_map_getinfo;
  op.cudafunc = NULL;
  op.hipfunc  = NULL;

  CHAMELEON_mapv_Tile( uplo, 1, &data, &op, &op_args );

  MPI_Allreduce( &(op_args.compress_size), &ginfo.compressed_size, 1,
                 MPI_UNSIGNED_LONG, MPI_SUM, MPI_COMM_WORLD );
  MPI_Allreduce( &(op_args.uncompress_size), &ginfo.uncompressed_size, 1,
                 MPI_UNSIGNED_LONG, MPI_SUM, MPI_COMM_WORLD );

  return ginfo;
}

void HCHAMELEON_lapmr( char forward, char trans, int nrhs,
                       HCHAM_desc_t *hdesc,
                       char *Bptr )
{
  CHAM_desc_t *descA = hdesc->super;
  int *perm = hdesc->perm;
  int fwd = ( forward == 'F' ) || ( forward == 'f' );

  // Store in desB all tiles in full rank format
  switch( descA->dtyp ) {
    case ChamRealFloat:
      LAPACKE_slapmr_work( LAPACK_COL_MAJOR, fwd, descA->lm, nrhs,
                           (float*)Bptr, descA->lm, perm );
      break ;
    case ChamRealDouble:
      LAPACKE_dlapmr_work( LAPACK_COL_MAJOR, fwd, descA->lm, nrhs,
                           (double*)Bptr, descA->lm, perm );
      break ;
    case ChamComplexFloat:
      LAPACKE_clapmr_work( LAPACK_COL_MAJOR, fwd, descA->lm, nrhs,
                           (lapack_complex_float*)Bptr, descA->lm, perm );
      break ;
    case ChamComplexDouble:
      LAPACKE_zlapmr_work( LAPACK_COL_MAJOR, fwd, descA->lm, nrhs,
                           (lapack_complex_double*)Bptr, descA->lm, perm );
      break ;
    default :
      //SETERRQ(1, "HCHAMELEON_uncompress_matrix: unknown scalar type\n") ;
      break ;
  }
  return;
}
#endif
