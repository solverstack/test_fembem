#include "main.h"

/*! \brief Logical to redirect error and warning messages toward stdout (used in SYS/SRC/error.c) */
Logical MPF_errorStreamIsStdout = MPF_FALSE ;

/* MPF_errorStreamIsStdout sert a renvoyer les messages d'erreur dans stdout */
#define MPF_errorStream ((MPF_errorStreamIsStdout)?(stdout):(stderr))

/* ================================================================================== */
/*! \brief Displays an error message with the location of this error in the source code

This routine is mainly called by the macros defined in mpf.h : SETERRA, SETERRQ, CHKERRA, CHKERRQ, CHKPTRA and CHKPTRQ.
It displays the processor rank followed by a complete error message defining and locating the error that occured.
\param line the line of the source file where the error occured
\param package the package where the source file is located (can be NULL if not known)
\param file the name of the source file
\param number the number of the error
\param message the error message (can be NULL if there is no message)
*/
int MpfError(int line,char *package,char *file,const char *function, int number, ...) {
  int flag, rank=-1;
  va_list args;
  char *message, *pos, *pos2 ;

  va_start (args,number) ;
  message = va_arg(args, char *) ;

  MPI_Initialized(&flag);
  if (flag) {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  }

  if (rank == -1)
    fprintf(MPF_errorStream,"[-] ERROR ");
  else
    fprintf(MPF_errorStream,"[%d] ERROR ", rank);

  if (strcmp("-",function))
    fprintf(MPF_errorStream,"in %30s() ",function) ;

  /* Cherche tous les "/src/" dans __FILE__, et si j'en trouve, j'affiche seulement a partir du dernier "src/..." */
  pos=file ;
  while ((pos2=strstr(pos, "/src/"))) {
    pos=pos2+1 ;
  }

  if (line != -1) {
    if (!package)
      fprintf(MPF_errorStream,"([%s,%d] : ", pos, line) ;
    else
      fprintf(MPF_errorStream,"[%s : %s,%d] : ",package,pos,line) ;
  }
  if (message)
    vfprintf(MPF_errorStream, message, args);
  fprintf(MPF_errorStream,"\n");
  fflush(MPF_errorStream);

  va_end(args);

  return number ;
}
/* ================================================================================== */
/*! \brief Displays a warning message with the location of this error in the source code

This routine is mainly called by the macros defined in mpf.h : SETWARN.
It displays the processor rank followed by a complete error message defining and locating the error that occured.
\param line the line of the source file where the error occured
\param package the package where the source file is located (can be NULL if not known)
\param file the name of the source file
\param number the number of the error
\param message the error message (can be NULL if there is no message)
*/
int MpfWarning(int line,char *package,char *file,const char *function,int number, ...) {
  int flag, rank=-1;
  va_list args;
  char *message, *pos, *pos2 ;

  va_start (args,number) ;
  message = va_arg(args, char *) ;

  MPI_Initialized(&flag);
  if (flag) {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  }

  if (rank == -1)
    fprintf(MPF_errorStream,"[-] WARNING ");
  else
    fprintf(MPF_errorStream,"[%d] WARNING ", rank);

  if (strcmp("-",function))
    fprintf(MPF_errorStream,"in %30s() ",function) ;

  /* Cherche tous les "/src/" dans __FILE__, et si j'en trouve, j'affiche seulement a partir du dernier "src/..." */
  pos=file ;
  while ((pos2=strstr(pos, "/src/"))) {
    pos=pos2+1 ;
  }

  if (line != -1) {
    if (!package)
      fprintf(MPF_errorStream,"([%s,%d] : ", pos, line) ;
    else
      fprintf(MPF_errorStream,"[%s : %s,%d] : ",package,pos,line) ;
  }
  if (message)
    vfprintf(MPF_errorStream, message, args);
  fprintf(MPF_errorStream,"\n");
  fflush(MPF_errorStream);

  va_end(args);

  return number ;
}
/* ================================================================================== */
/*! \brief Print on screen a progress bar

For an assembly operation for instance, this routine can be used to display a nice progress indicator on screen.
\a currentValue must vary between 0 and \a maximumValue (included). It can be for instance the number of blocks assembled and total.
\param comm the communicator (for printf)
\param currentValue the current value of the progress parameter
\param maximumValue the maximum and final value of the progress parameter
\return 0 for success
*/
int Mpf_progressBar(MPI_Comm comm, int currentValue, int maximumValue) {
  div_t res ;
  int j ;
  static int progress = -1; /* varie de 0 a 100 au cours du calcul concerne */

  if (maximumValue==0)
    res.quot=100 ;
  else {
    if (currentValue<INT_MAX/100)
      res=div(currentValue*100, maximumValue) ;
    /* Calcule le quotient de la division */
    else
      res.quot=(int)( (long long) currentValue*100/maximumValue) ;
  }

  for (j=progress+1 ; j<=res.quot ; j++)
    if (j % 10 == 0)
      Mpf_printf(comm, " %2d%% ", j ) ;
    else
      Mpf_printf(comm, ".") ;
  progress=res.quot ;
  if (currentValue>=maximumValue) {
    /* termine la barre de progression */
    Mpf_printf(comm, " Done.\n") ;
    progress=-1 ;
  }

  return 0 ;
}
/* ================================================================================== */
int Mpf_printf(MPI_Comm comm, const char *fmt, ...)
{
  int rank ;

  MPI_Comm_rank(comm, &rank);
  if (!rank) {
    va_list argp;
    va_start(argp, fmt);
    vfprintf(stdout, fmt, argp);
    fflush(stdout);
    va_end(argp);
  }
  return 0;
}
/* ================================================================================== */
/*! \brief Convert string to double

like atof libc version but
- allows fortran style exponent beginning with 'D' or 'd'
- does not set errno.
*/
static double MpfAtof(const char *str) {
  int exponent, negative, n, num_digits, num_decimals ;
  char *p ;
  double number, p10 ;

  p = (char *) str;
  /* Skip leading whitespace */
  while (isspace(*p)) p++;

  /* Handle optional sign */
  negative = 0;
  switch (*p) {
    case '-': negative = 1;
      /* Fall through to increment position */
    case '+': p++;
  }

  number = 0.;
  exponent = 0;
  num_digits = 0;
  num_decimals = 0;

  /* Process string of digits */
  while (isdigit(*p)) {
    number = number * 10. + (*p - '0');
    p++;
    num_digits++;
  }

  /* Process decimal part */
  if ( (*p == '.') || (*p == ',') ) {
    p++;

    while (isdigit(*p)) {
      number = number * 10. + (*p - '0');
      p++;
      num_digits++;
      num_decimals++;
    }

    exponent -= num_decimals;
  }

  if (num_digits == 0)
    return 0.0;

  /* Correct for sign */
  if (negative) number = -number;

  /* Process an exponent string */
  if (*p == 'e' || *p == 'E' || *p == 'd' || *p == 'D') {
    /* Handle optional sign */
    negative = 0;
    switch(*++p) {
      case '-': negative = 1;
        /* Fall through to increment position */
      case '+': p++;
    }

    /* Process string of digits */
    n = 0;
    while (isdigit(*p)) {
      n = n * 10 + (*p - '0');
      p++;
    }

    if (negative)
      exponent -= n;
    else
      exponent += n;
  }

  if (exponent < DBL_MIN_EXP  || exponent > DBL_MAX_EXP) {
    return HUGE_VAL;
  }

  /* Scale the result */
  p10 = 10.;
  n = exponent;
  if (n < 0) n = -n;
  while (n) {
    if (n & 1) {
      if (exponent < 0)
        number /= p10;
      else
        number *= p10;
    }
    n >>= 1;
    p10 *= p10;
  }

  return number;
}
/* ================================================================================== */
#define MPF_OPT_RFLAG 1
/* ================================================================================== */
static void MpfArgSqueeze( int  *Argc, char **argv ) {
  int argc, i, j;

  /* Compress out the eliminated args */
  argc = *Argc;
  j    = 0;
  i    = 0;
  while (j < argc) {
    while (j < argc && argv[j] == 0) j++;
    if (j < argc) argv[i++] = argv[j++];
  }
  /* Back off the last value if it is null */
  if (!argv[i-1]) i--;
  *Argc = i;
}
/* ================================================================================== */
/*! \brief Find a name in an argument list.

\param argc number of arguments
\param argv argument vector
\param name name to find
\return index in argv of name; -1 if name is not in argv
*/
static int MpfArgFindName(int argc, char **argv, const char *name ) {
  int  i;

  for (i=0; i<argc; i++) {
    // Skip the initial(s) '-'
    char *argv_i=argv[i];
    while (argv_i && argv_i[0]!=0 && argv_i[0]=='-') argv_i++;
    while (name && name[0]!=0 && name[0]=='-') name++;
    if (strcmp( argv_i, name ) == 0) return i;
  }
  return -1;
}
/* ================================================================================== */
/*! \brief Get the value (double) of a named parameter.

\param Argc pointer to argument count
\param argv argument vector
\param rflag if true, remove the argument and its value from argv
\param name name to find
\param val pointer to value (will be set only if found)
\return 1 on success
*/
int MpfArgGetDouble( int *Argc, char **argv, int rflag, const char *name, double *val ) {
  int idx;

  idx = MpfArgFindName( *Argc, argv, name );
  if (idx < 0) return 0;

  if (idx + 1 >= *Argc) {
    SETERRA(1,"Missing value for argument \"%s\".", name );
    return 0;
  }

  *val = MpfAtof( argv[idx+1] );
  if (MPF_OPT_RFLAG && rflag) {
    argv[idx]   = 0;
    argv[idx+1] = 0;
    MpfArgSqueeze( Argc, argv );
  }
  return 1;
}
/* ================================================================================== */
/*! \brief Return 1 if name is in argument list

\param Argc pointer to argument count
\param argv argument vector
\param rflag if true, remove the argument and its value from argv
\param name name to search for
\return 1 on success
*/
int MpfArgHasName( int *Argc, char **argv, int rflag, const char *name ) {
  int idx;

  idx = MpfArgFindName( *Argc, argv, name );
  if (idx < 0) return 0;

  if (MPF_OPT_RFLAG && rflag) {
    argv[idx]   = 0;
    MpfArgSqueeze( Argc, argv );
  }
  return 1;
}
/* ================================================================================== */
/*! \brief Get the value (integer) of a named parameter.

This routine handles both decimal and hexidecimal integers.
\param Argc pointer to argument count
\param argv argument vector
\param rflag if true, remove the argument and its value from argv
\param name name to find
\param val pointer to value (will be set only if found)
\return 1 on success
*/
int MpfArgGetInt( int  *Argc, char **argv, int rflag, const char *name, int  *val ) {
  int idx;
  char *p;

  idx = MpfArgFindName( *Argc, argv, name );
  if (idx < 0) return 0;

  if (idx + 1 >= *Argc) {
    SETERRA(1,"Missing value for argument \"%s\".", name );
    return 0;
  }

  p = argv[idx+1];
  /* Check for hexidecimal value */
  if ((strlen(p) > 1) && p[0] == '0' && p[1] == 'x') {
    sscanf( p, "%i", val );
  } else {
    if (strlen(p) > 1 && p[0] == '-' && p[1] >= 'A' && p[1] <= 'z') {
      SETERRA(1,"Missing value for argument \"%s\".", name );
      return 0;
    }
    *val = atoi( p );
  }

  if (MPF_OPT_RFLAG && rflag) {
    argv[idx]   = 0;
    argv[idx+1] = 0;
    MpfArgSqueeze( Argc, argv );
  }
  return 1;
}
/* ================================================================================== */
/*! \brief Get the value (string) of a named parameter.

\param Argc pointer to argument count
\param argv argument vector
\param rflag if true, remove the argument and its value from argv
\param name name to find
\param val pointer to buffer to hold value (will be set only if found).
\return 1 on success
*/
int MpfArgGetString( int *Argc, char **argv, int rflag, const char *name, char **val ) {
  int idx;

  idx = MpfArgFindName( *Argc, argv, name );
  if (idx < 0) return 0;

  if (idx + 1 >= *Argc) {
    SETERRA(1,"Missing value for argument \"%s\".", name );
    return 0;
  }

  *val = (char *)strdup(argv[idx+1]);
  if (MPF_OPT_RFLAG && rflag) {
    argv[idx]   = 0;
    argv[idx+1] = 0;
    MpfArgSqueeze( Argc, argv );
  }
  return 1;
}
/* ================================================================================== */
/*! \brief Equivalent of MPI_Allreduce() with a 64 bit \a count argument
   */
int MPI_AllreduceL(const void *sendbuf, void *recvbuf, ssize_t count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm) {
  int ierr, scalsize;
  size_t pos=0, pak=0;
  /* MPI maximum message size (in bytes) */
  static size_t PACKET_SIZE=1e8;

  enter_context("MPI_AllreduceL");

  ierr = MPI_Type_size(datatype, &scalsize); CHKERRQ(ierr);
  pak = PACKET_SIZE/scalsize; // number of 'datatype' to send in each message
  while (count>0) {
    // We communicate by packet of 100 Mbytes
    void *src = sendbuf==MPI_IN_PLACE ? MPI_IN_PLACE : (void*)((char*)sendbuf+pos) ;
    void *dest = (void*)((char*)recvbuf+pos) ;
    ierr = MPI_Allreduce(src, dest, (int)MIN(pak,count), datatype, op, comm); CHKERRQ(ierr);
    pos += pak*scalsize;
    count -= pak;
  }

  leave_context();

  return 0;
}
/* ================================================================================== */
/*! \brief Sort the array mat, remove the zeros and sum the elements placed at the same position

  For simple precision only (S)
\param mat the sparse matrix to sort
\param size the size (modified on output)
\return 0 for success
*/
int S_sortAndCompact(SnonZero *mat, int *size) {
  int ie, il ;

  /* Trie le tableau */
  qsort(mat, *size, sizeof(SnonZero), compare_SnonZero_Line) ;

  /* somme les valeurs situees a la meme position et enleve les zeros */
  ie=-1 ; /* ie = position d'ecriture */
  for (il=0 ; il<*size ; il++) /* il = position de lecture */
    if (mat[il].v) {
      if (ie>=0 && compare_SnonZero_Line( &(mat[ie]), &(mat[il]) ) == 0) /* s'ils sont a la meme position, on somme */
        mat[ie].v += mat[il].v ;
      else { /* sinon, on garde l'element 'il' en le recopiant dans une nouvelle case */
        ie ++ ;
        if (il>ie) mat[ie] = mat[il] ;
      }
    }
  *size=ie+1 ;
  return 0 ;
}
/* ================================================================================== */
/*! \brief Sort the array mat, remove the zeros and sum the elements placed at the same position

  For double precision only (D)
\param mat the sparse matrix to sort
\param size the size (modified on output)
\return 0 for success
*/
int sortAndCompact(DnonZero *mat, int *size) {
  int ie, il ;

  /* Trie le tableau */
  qsort(mat, *size, sizeof(DnonZero), compare_DnonZero_Line) ;

  /* somme les valeurs situees a la meme position et enleve les zeros */
  ie=-1 ; /* ie = position d'ecriture */
  for (il=0 ; il<*size ; il++) /* il = position de lecture */
    if (mat[il].v) {
      if (ie>=0 && compare_DnonZero_Line( &(mat[ie]), &(mat[il]) ) == 0) /* s'ils sont a la meme position, on somme */
        mat[ie].v += mat[il].v ;
      else { /* sinon, on garde l'element 'il' en le recopiant dans une nouvelle case */
        ie ++ ;
        if (il>ie) mat[ie] = mat[il] ;
      }
    }
  *size=ie+1 ;
  return 0 ;
}
/* ================================================================================== */
/*! \brief Sort the array mat, remove the zeros and sum the elements placed at the same position

  For simple complex only (C)
\param mat the sparse matrix to sort
\param size the size (modified on output)
\return 0 for success
*/
int C_sortAndCompact(CnonZero *mat, int *size) {
  int ie, il ;

  /* Trie le tableau */
  qsort(mat, *size, sizeof(CnonZero), compare_CnonZero_Line) ;

  /* somme les valeurs situees a la meme position et enleve les zeros */
  ie=-1 ; /* ie = position d'ecriture */
  for (il=0 ; il<*size ; il++) /* il = position de lecture */
    if (mat[il].v.r || mat[il].v.i) {
      if (ie>=0 && compare_CnonZero_Line( &(mat[ie]), &(mat[il]) ) == 0) { /* s'ils sont a la meme position, on somme */
        mat[ie].v.r += mat[il].v.r ;
        mat[ie].v.i += mat[il].v.i ;
      } else { /* sinon, on garde l'element 'il' en le recopiant dans une nouvelle case */
        ie ++ ;
        if (il>ie) mat[ie] = mat[il] ;
      }
    }
  *size=ie+1 ;
  return 0 ;
}
/* ================================================================================== */
/*! \brief Sort the array mat, remove the zeros and sum the elements placed at the same position

  For double complex only (Z)
\param mat the sparse matrix to sort
\param size the size (modified on output)
\return 0 for success
*/
int Z_sortAndCompact(ZnonZero *mat, int *size) {
  int ie, il ;

  /* Trie le tableau */
  qsort(mat, *size, sizeof(ZnonZero), compare_ZnonZero_Line) ;

  /* somme les valeurs situees a la meme position et enleve les zeros */
  ie=-1 ; /* ie = position d'ecriture */
  for (il=0 ; il<*size ; il++) /* il = position de lecture */
    if (mat[il].v.r || mat[il].v.i) {
      if (ie>=0 && compare_ZnonZero_Line( &(mat[ie]), &(mat[il]) ) == 0) { /* s'ils sont a la meme position, on somme */
        mat[ie].v.r += mat[il].v.r ;
        mat[ie].v.i += mat[il].v.i ;
      } else { /* sinon, on garde l'element 'il' en le recopiant dans une nouvelle case */
        ie ++ ;
        if (il>ie) mat[ie] = mat[il] ;
      }
    }
  *size=ie+1 ;
  return 0 ;
}
/* ================================================================================== */
/*! \brief Sort the array mat, remove the zeros and sum the elements placed at the same position

  For double complex only (Z) with size_t indices
\param mat the sparse matrix to sort
\param size the size (modified on output)
\return 0 for success
*/
int Z_sortAndCompactL(ZnonZero *mat, size_t *size) {
  ssize_t ie, il ;

  /* Trie le tableau */
  qsort(mat, *size, sizeof(ZnonZero), compare_ZnonZero_Line) ;

  /* somme les valeurs situees a la meme position et enleve les zeros */
  ie=-1 ; /* ie = position d'ecriture */
  for (il=0 ; il<*size ; il++) /* il = position de lecture */
    if (mat[il].v.r || mat[il].v.i) {
      if (ie>=0 && compare_ZnonZero_Line( &(mat[ie]), &(mat[il]) ) == 0) { /* s'ils sont a la meme position, on somme */
        mat[ie].v.r += mat[il].v.r ;
        mat[ie].v.i += mat[il].v.i ;
      } else { /* sinon, on garde l'element 'il' en le recopiant dans une nouvelle case */
        ie ++ ;
        if (il>ie) mat[ie] = mat[il] ;
      }
    }
  *size=ie+1 ;
  return 0 ;
}
/* ================================================================================== */
/*! \brief compares two SnonZeros by lines

\param pt1 pointer on the first SnonZero
\param pt2 pointer on the second SnonZero
\return -1, 0 or +1 if \a pt1 is before, equal or after \a pt2.
*/
int compare_SnonZero_Line(const void *pt1, const void *pt2) {
  SnonZero *s1=(SnonZero*)pt1 ;
  SnonZero *s2=(SnonZero*)pt2 ;

  if (s1->i > s2->i)
    return(+1) ;
  if (s1->i < s2->i)
    return(-1) ;

  if (s1->j > s2->j)
    return(+1) ;
  if (s1->j < s2->j)
    return(-1) ;

  return(0) ;
}
/* ================================================================================== */
/*! \brief compares two DnonZeros by lines

\param pt1 pointer on the first DnonZero
\param pt2 pointer on the second DnonZero
\return -1, 0 or +1 if \a pt1 is before, equal or after \a pt2.
*/
int compare_DnonZero_Line(const void *pt1, const void *pt2) {
  DnonZero *d1=(DnonZero*)pt1 ;
  DnonZero *d2=(DnonZero*)pt2 ;

  if (d1->i > d2->i)
    return(+1) ;
  if (d1->i < d2->i)
    return(-1) ;

  if (d1->j > d2->j)
    return(+1) ;
  if (d1->j < d2->j)
    return(-1) ;

  return(0) ;
}
/* ================================================================================== */
/*! \brief compares two CnonZeros by lines

\param pt1 pointer on the first CnonZero
\param pt2 pointer on the second CnonZero
\return -1, 0 or +1 if \a pt1 is before, equal or after \a pt2.
*/
int compare_CnonZero_Line(const void *pt1, const void *pt2) {
  CnonZero *c1=(CnonZero*)pt1 ;
  CnonZero *c2=(CnonZero*)pt2 ;

  if (c1->i > c2->i)
    return(+1) ;
  if (c1->i < c2->i)
    return(-1) ;

  if (c1->j > c2->j)
    return(+1) ;
  if (c1->j < c2->j)
    return(-1) ;

  return(0) ;
}
/* ================================================================================== */
/*! \brief compares two ZnonZeros by lines

\param pt1 pointer on the first ZnonZero
\param pt2 pointer on the second ZnonZero
\return -1, 0 or +1 if \a pt1 is before, equal or after \a pt2.
*/
int compare_ZnonZero_Line(const void *pt1, const void *pt2) {
  ZnonZero *z1=(ZnonZero*)pt1 ;
  ZnonZero *z2=(ZnonZero*)pt2 ;

  if (z1->i > z2->i)
    return(+1) ;
  if (z1->i < z2->i)
    return(-1) ;

  if (z1->j > z2->j)
    return(+1) ;
  if (z1->j < z2->j)
    return(-1) ;

  return(0) ;
}
/* ================================================================================== */
Time get_time() {
  Time result ;
#ifdef _WIN32
  int64_t frequency;
  int64_t value;
  double dTime;
  QueryPerformanceFrequency(&frequency);
  QueryPerformanceCounter(&value);
  dTime = ((double)value) / ((double)frequency);
  result.tv_sec = (int64_t) dTime;
  result.tv_nsec = (int64_t) (1.e9 * (dTime - result.tv_sec));
  if (result.tv_nsec >= 1000000000) {
    result.tv_sec += 1;
    result.tv_nsec = 0;
  }
#elif defined(HAVE_LIBRT)
  int ierr ;
  ierr=clock_gettime(CLOCK_MONOTONIC, &result); CHKERRA(ierr);
#elif defined(HAVE_MACH_MACH_TIME_H) /* Version OSX */
  static double timeConvert = 0.0;
  if ( timeConvert == 0.0 )
  {
    mach_timebase_info_data_t timeBase;
    (void)mach_timebase_info( &timeBase );
    timeConvert = (double)timeBase.numer /
        (double)timeBase.denom /
        1000000000.0;
  }
  double t = (double)mach_absolute_time( ) * timeConvert;
  result.tv_sec  = (long)floor(t) ; /* seconds */
  result.tv_nsec = (long)floor( (t-(double)result.tv_sec)*1.e9 ) ;  /* uSecs */
#else
  int ierr ;
  struct rusage temp;
  ierr=getrusage(RUSAGE_SELF,&temp); CHKERRA(ierr);
  result.tv_sec  = (time_t)(temp.ru_utime.tv_sec) ; /* seconds */
  result.tv_nsec = (long)(1000*temp.ru_utime.tv_usec) ;  /* uSecs */
#endif /* _WIN32 */
  return result;
}
/* ================================================================================== */
Time time_interval(Time t1, Time t2) {
  Time result;
  result.tv_sec = t2.tv_sec - t1.tv_sec;
  result.tv_nsec = t2.tv_nsec - t1.tv_nsec;
  if (result.tv_nsec < 0) {
    result.tv_sec -= 1;
    result.tv_nsec += 1000000000;
  }
  return result;
}
/* ================================================================================== */
Time add_times(Time t1, Time t2) {
  Time result;
  result.tv_sec = t1.tv_sec + t2.tv_sec;
  result.tv_nsec = t1.tv_nsec + t2.tv_nsec;
  if (result.tv_nsec >= 1000000000) {
    result.tv_sec += 1;
    result.tv_nsec -= 1000000000;
  }
  return result;
}
/* ================================================================================== */
double time_in_s(Time t) {
  return t.tv_sec + (((double)t.tv_nsec) / 1.e9);
}
/* ================================================================================== */
double time_interval_in_seconds(Time t1, Time t2) {
  Time interval = time_interval(t1, t2);
  return time_in_s(interval);
}
/* ================================================================================== */
/* Convertit de simple vers double precision. Attention : Le tableau 'data' doit etre suffisamment grand */
void simpleToDouble(void *data, size_t n) {
  ssize_t i ;
  double *dest ;
  float tmp, *src ;

  dest =(double*)(data) ;
  src =(float*)(data)  ;
  /* Commence la conversion par la fin pour ne pas ecraser les donnees */
  for (i=n-1 ; i>=0 ; i--) {
    tmp=src[i] ;
    dest[i]=0. ;
    dest[i]=(double)tmp ;
  }

}
/* ================================================================================== */
/* Convertit de double vers simple precision. */
void doubleToSimple(void *data, size_t n) {
  size_t i ;
  double tmp, *src =(double*)data ;
  float *dest =(float*)data  ;

  for (i=0 ; i<n ; i++) {
    tmp=src[i] ;
    dest[i]=0. ;
    dest[i]=(float)tmp ;
  }

}

#ifdef HAVE_HMAT
/* ================================================================================== */
int hmat_get_sync_exec(void) {
  return 1;
}
/* ================================================================================== */
static void update_progress(hmat_progress_t * ctx) {
  MPI_Comm comm = (MPI_Comm)((intptr_t)(ctx->user_data));
  Mpf_progressBar(comm, ctx->current, ctx->max);
}
/* ================================================================================== */
void mpf_hmat_compression(struct mpf_hmat_create_compression_args_t *args) {
  // Compression algorithm
  switch (args->method) {
    case hmat_compress_svd:
      args->output = hmat_create_compression_svd(args->threshold);
      break;
    case hmat_compress_aca_full:
      args->output = hmat_create_compression_aca_full(args->threshold);
      break;
    case hmat_compress_aca_partial:
      args->output = hmat_create_compression_aca_partial(args->threshold);
      break;
    case hmat_compress_aca_plus:
      args->output = hmat_create_compression_aca_plus(args->threshold);
      break;
    case hmat_compress_aca_random:
      args->output = hmat_create_compression_aca_random(args->threshold);
      break;
    default: SETWARN(1, "unknown compression method");
  }
}
#endif
/* ================================================================================== */
static int MPI_MUST_DIE=0 ;
#ifdef HAVE_CHAMELEON
static int chameleon_initialized = 0;
#endif
/* ================================================================================== */
int SCAB_Init(int* argc, char*** argv) {
  int flag, ierr;
  int required = MPI_THREAD_MULTIPLE; /* MPI thread level required */
  int provided = -1; /* MPI thread level provided */

  MPI_Initialized(&flag);
  if (!flag) {
    MPI_MUST_DIE=1 ;
    ierr = MPI_Init_thread(argc, argv, required, &provided);CHKERRA(ierr);
  }
  /* Display the level of support of threads by MPI */
  switch (provided) {
    case MPI_THREAD_SINGLE:
      Mpf_printf(MPI_COMM_WORLD,"MPI thread level provided = MPI_THREAD_SINGLE\n");
      break;
    case MPI_THREAD_FUNNELED:
      Mpf_printf(MPI_COMM_WORLD,"MPI thread level provided = MPI_THREAD_FUNNELED\n");
      break;
    case MPI_THREAD_SERIALIZED:
      Mpf_printf(MPI_COMM_WORLD,"MPI thread level provided = MPI_THREAD_SERIALIZED\n");
      break;
    case MPI_THREAD_MULTIPLE:
      Mpf_printf(MPI_COMM_WORLD,"MPI thread level provided = MPI_THREAD_MULTIPLE\n");
      break;
    default:
      Mpf_printf(MPI_COMM_WORLD,"MPI thread level provided = ???\n");
  }

  /* -------------------------------- */
  /* -- Gestion du solveur HMATRIX -- */
  /* -------------------------------- */
#ifdef HAVE_HMAT
  Mpf_printf(MPI_COMM_WORLD, "HMATRIX solver\n");
  hmat_settings_t settings;
  hmat_get_parameters(&settings);

  memset(&mpf_hmat_settings, 0, sizeof(mpf_hmat_settings));
  mpf_hmat_settings.factorization_type = hmat_factorization_none;
  mpf_hmat_settings.acaEpsilon = 1e-3;
  mpf_hmat_settings.compressionMethod = hmat_compress_aca_plus;
  mpf_hmat_settings.epsilon = 1e-3;
  mpf_hmat_settings.max_leaf_size = -1;
  if(strcmp(hmat_get_version(), HMAT_VERSION))
    Mpf_printf(MPI_COMM_WORLD, "***\n*** hmat version %s (compiled with version %s)\n", hmat_get_version(), HMAT_VERSION);
  else
    Mpf_printf(MPI_COMM_WORLD, "***\n*** hmat version %s\n", hmat_get_version());
  const char *d, *t;
  hmat_get_build_date(&d, &t);
  Mpf_printf(MPI_COMM_WORLD,"*** Built on %s at %s\n***\n", d, t);

  if (MpfArgGetDouble( argc, *argv, 1, "--hmat-eps-assemb" , &mpf_hmat_settings.acaEpsilon  )) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Compression epsilon: %e\n", mpf_hmat_settings.acaEpsilon );
  }
  if (MpfArgGetDouble( argc, *argv, 1, "--hmat-eps-recompr" , &mpf_hmat_settings.epsilon  )) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Recompression epsilon: %e\n", mpf_hmat_settings.epsilon );
  }

  if (MpfArgHasName( argc, *argv, 1, "--hmat-svd" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Using SVD compression\n");
    mpf_hmat_settings.compressionMethod = hmat_compress_svd;
  }
  if (MpfArgHasName( argc, *argv, 1, "--hmat-aca-partial" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Using ACA with partial pivoting\n");
    mpf_hmat_settings.compressionMethod = hmat_compress_aca_partial;
  }
  if (MpfArgHasName( argc, *argv, 1, "--hmat-aca-full" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Using ACA with full pivoting\n");
    mpf_hmat_settings.compressionMethod = hmat_compress_aca_full;
  }
  if (MpfArgHasName( argc, *argv, 1, "--hmat-aca-plus" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Using ACA+\n");
    mpf_hmat_settings.compressionMethod = hmat_compress_aca_plus;
  }

  if (MpfArgHasName( argc, *argv, 1, "--hmat-lu" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Force LU decomposition\n");
    mpf_hmat_settings.factorization_type = hmat_factorization_lu;
  }
  if (MpfArgHasName( argc, *argv, 1, "--hmat-llt" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Force LLt decomposition (for symmetric matrices)\n");
    mpf_hmat_settings.factorization_type = hmat_factorization_llt;
  }
  if (MpfArgHasName( argc, *argv, 1, "--hmat-ldlt" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Force LDLt decomposition (for symmetric matrices)\n");
    mpf_hmat_settings.factorization_type = hmat_factorization_ldlt;
  }

  Logical rt_set=MPF_FALSE;
  if (MpfArgHasName( argc, *argv, 1, "--hmat-toyrt" ) > 0) {
#ifdef HMAT_HAVE_TOYRT
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Use the ToyRT runtime\n");
    mpf_hmat_settings.engine = mpf_hmat_toyrt;
    rt_set=MPF_TRUE;
#else
    Mpf_printf(MPI_COMM_WORLD,"[HMat] ToyRT runtime not available\n");
#endif
  } else if (MpfArgHasName( argc, *argv, 1, "--hmat-starpu" ) > 0) {
#ifdef HMAT_HAVE_STARPU
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Use the starpu runtime\n");
    mpf_hmat_settings.engine = mpf_hmat_starpu;
    rt_set=MPF_TRUE;
#else
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Starpu runtime not available\n");
#endif
  }
  if (MpfArgHasName( argc, *argv, 1, "--hmat-seq" ) > 0 || !rt_set) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Use the sequential runtime\n");
    mpf_hmat_settings.engine = mpf_hmat_seq;
  }

#ifdef HMAT_PARALLEL
  mpf_hmat_settings.l0size = -1;
  if(MpfArgGetInt ( argc, *argv, 1, "--hmat-l0", &mpf_hmat_settings.l0size))
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Maximum number of parallel leaves: %d\n",
               mpf_hmat_settings.l0size);
#endif

  if (MpfArgGetInt(argc, *argv, 1, "--hmat-leaf-size", &mpf_hmat_settings.max_leaf_size))
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Maximum leaf size: %d\n", mpf_hmat_settings.max_leaf_size);

  if (MpfArgHasName( argc, *argv, 1, "--hmat-coarsening" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Coarsening of the HMatrix\n");
    settings.coarsening = 1;
  }

  /* Validate the detection of null rows and columns */
  if (MpfArgHasName( argc, *argv, 1, "--hmat-val-null" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Validate the detection of null rows and columns\n");
    settings.validateNullRowCol = 1;
  }

  /* Validation of the Rk compression (with SVD, ACA, ...) */
  if (MpfArgHasName( argc, *argv, 1, "--hmat-val" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Validate the rk-matrices after compression \n");
    settings.validateCompression = 1;
  }
  if (MpfArgGetDouble( argc, *argv, 1, "--hmat-val-threshold", &settings.validationErrorThreshold )) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Error threshold for the compression validation  : %e\n", settings.validationErrorThreshold);
  }
  if (MpfArgHasName( argc, *argv, 1, "--hmat-dump-trace" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] Dump trace at the end of the algorithms (depends on the runtime)\n");
    settings.dumpTrace = 1;
  }

  if (MpfArgHasName( argc, *argv, 1, "--hmat-val-rerun" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] For blocks above error threshold, re-run the compression algorithm \n");
    settings.validationReRun = 1;
  }
  if (MpfArgHasName( argc, *argv, 1, "--hmat-val-dump" ) > 0) {
    Mpf_printf(MPI_COMM_WORLD,"[HMat] For blocks above error threshold, dump the faulty block to disk\n");
    settings.validationDump = 1;
  }

  hmat_set_parameters(&settings);

  mpf_hmat_settings.progress.update = update_progress;
#endif


  /* ---------------------------------- */
  /* -- Gestion du solveur CHAMELEON -- */
  /* ---------------------------------- */
  if (MpfArgHasName(argc, *argv, 1, "--chameleon") > 0) {
#ifdef HAVE_CHAMELEON
    Mpf_printf(MPI_COMM_WORLD, "CHAMELEON solver\n");

    /* Number of GPUs and CPUs for the CHAMELEON solver.
       Default value for nCPUs: OMP_NUM_THREADS (or 12 if it is not set)
                     for NGPUS: 0
     */
    int nbCPUs=12, nbGPUs=0;
    char *s_num;
    if ( ((s_num=getenv("OMP_NUM_THREADS")) != NULL) && (atoi(s_num) != 0) )
      nbCPUs=atoi(s_num) ;
    MpfArgGetInt( argc, *argv, 1, "--chameleon-ngpus", &nbGPUs);
    MpfArgGetInt( argc, *argv, 1, "--chameleon-ncpus", &nbCPUs);
    ierr = CHAMELEON_Init (nbCPUs, nbGPUs); CHKERRQ(ierr);
    Mpf_printf(MPI_COMM_WORLD, "[Chameleon] Using lib CHAMELEON with %d cores, %d gpus.\n", nbCPUs, nbGPUs);

    setenv ("STARPU_HISTORY_MAX_ERROR", "32000", 1); // Otherwise, StarPU will print a lot of useless warnings.

    /* Tile size for the CHAMELEON solver */
    if (MpfArgGetInt( argc, *argv, 1, "--chameleon-tile", &tileSize)) {
      Mpf_printf(MPI_COMM_WORLD, "[Chameleon] Tile size for the CHAMELEON solver : %d\n", tileSize );
    }
    ierr = CHAMELEON_Set(CHAMELEON_TILE_SIZE, tileSize);  CHKERRQ(ierr);

    // Activate warnings and errors in Chameleon
    ierr = CHAMELEON_Enable(CHAMELEON_WARNINGS); CHKERRQ(ierr);

    chameleon_initialized=1;
#else
    SETERRQ(1, "CHAMELEON solver not available");
#endif
  }

  /* Option to set environment variable on the command line
     '--putenv toto=titi' will set the variable environment 'toto' to the value 'titi' through a call to putenv().
     This option can appear several time, hence the 'while'. */
  char *variable;
  while (MpfArgGetString( argc, *argv, 1, "--putenv" , &variable  )) {
    Mpf_printf(MPI_COMM_WORLD,"Setting environment variable: %s \n", variable) ;
    ierr=putenv(variable); CHKERRQ(ierr);
  }

  /*! \brief Abort timer (in seconds)

     Abort timer: it is the initial value of the countdown timer started by setitimer().
     When it expires, a SIGALRM signal is delivered,
     and (with linux) the execution is stopped. This option can be used to enforce termination after a
     given time and avoid deadlocks. Set this value with --abort-timer X
  */
#if !defined(_WIN32)
  int chrono_abort_timer=1;
  if (MpfArgGetInt(argc, *argv, 1, "--abort-timer", &chrono_abort_timer) > 0) {
    Mpf_printf(MPI_COMM_WORLD, "Setting abort timer to %d s.\n", chrono_abort_timer);
    struct itimerval tv ;
    tv.it_interval.tv_sec = chrono_abort_timer ;
    tv.it_interval.tv_usec = 0;
    tv.it_value.tv_sec = chrono_abort_timer ;
    tv.it_value.tv_usec = 0;
    ierr=setitimer(ITIMER_REAL, &tv, NULL) ; CHKERRQ(ierr) ;
  }
#endif

  return 0;
}
/* ================================================================================== */
int SCAB_Exit(int* argc, char** argv) {
  int ierr=0;

#ifdef HAVE_HMAT
  if (mpf_hmat_settings.interface_initialized) {
    int i;
    for(i = HMAT_SIMPLE_PRECISION; i <= HMAT_DOUBLE_COMPLEX; i++)
      mpf_hmat_settings.interfaces[i]->finalize();
    mpf_hmat_settings.interface_initialized=0;
  }
#endif

#ifdef HAVE_CHAMELEON
  if (chameleon_initialized) {
    ierr = CHAMELEON_Finalize (); CHKERRQ(ierr);
    chameleon_initialized=0;
  }
#endif

  if (MPI_MUST_DIE) {
    ierr = MPI_Finalize(); CHKERRA(ierr);
    MPI_MUST_DIE=0 ;
  }

  return 0 ;
}
/* ================================================================================== */
