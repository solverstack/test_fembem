#include "main.h"

/*! \brief Runs the test using COMPOSYX solver
 */
#if defined(HAVE_COMPOSYX)

char * C_PACKAGE_NAME = const_cast<char *>(PACKAGE_NAME) ;
char * C_FILE = const_cast<char *>(__FILE__);

// Redefine the macros to be C++ compatible

/*! \brief Displays an error message and that's all. */
#define C_SETWARN(n,s, ...)   {MpfWarning(__LINE__,C_PACKAGE_NAME,C_FILE,__func__,n,s, ## __VA_ARGS__);}
/*! \brief Displays an error message and quits the current routine. */
#define C_SETERRQ(n,s, ...)   {int _ierr = MpfError(__LINE__,C_PACKAGE_NAME,C_FILE,__func__,n,s, ## __VA_ARGS__); debug_break(); return _ierr;}
/*! \brief Displays an error message and aborts the code. */
#define C_SETERRA(n,s, ...)   {int _ierr = MpfError(__LINE__,C_PACKAGE_NAME,C_FILE,__func__,n,s, ## __VA_ARGS__);mpf_backtrace();debug_break();MPI_Abort(MPI_COMM_WORLD,_ierr);}
/*! \brief Checks the integer n, if not zero then it displays an error message and quits the current routine. */
#define C_CHKERRQ(n)     {if (n) C_SETERRQ(n,(char *)0);}
/*! \brief Checks the integer n, if not zero then it displays an error message and aborts the code. */
#define C_CHKERRA(n)     {if (n) C_SETERRA(n,(char *)0);}
/*! \brief Checks the pointer p, if it is NULL then it displays an error message and quits the current routine. */
#define C_CHKPTRQ(p)     {if (!(p)) C_SETERRQ(1,"Unable to allocate requested memory");}
/*! \brief Checks the pointer p, if it is NULL then it displays an error message and aborts the code. */
#define C_CHKPTRA(p)     {if (!(p)) C_SETERRA(1,"Unable to allocate requested memory");}
/*! \brief Evaluates the expression x, if it is false then it displays an error message and quits the current routine. */
#define C_ASSERTQ(x) { if (!(x)) { C_SETERRQ(1, "Assert failure %s", #x); }}
/*! \brief Evaluates the expression x, if it is false then it displays an error message and aborts the code. */
#define C_ASSERTA(x) { if (!(x)) { C_SETERRA(1, "Assert failure %s", #x); }}

#include <composyx.hpp>
#include <composyx/solver/GMRES.hpp>
#include <memory>
#include <cstring>
#include <string>
#include <span>

using namespace composyx;

template<typename Scalar>
struct nonzero_type { using type = std::false_type; };
template<> struct nonzero_type<float> { using type = SnonZero; };
template<> struct nonzero_type<double> { using type = DnonZero; };
template<> struct nonzero_type<std::complex<float>> { using type = CnonZero; };
template<> struct nonzero_type<std::complex<double>> { using type = ZnonZero; };

template<typename Scalar>
int testCOMPOSYX_FEM(double * relative_error){

  using NonZero = typename nonzero_type<Scalar>::type;

  int ierr = 0;
  double temps_initial, temps_final, temps_cpu;
  size_t vectorSize = (size_t) nbPts*nbRHS*(complexALGO?2:1);

  // Realise le produit matrice vecteur classique : A.rhs -> solCLA

  Mpf_printf(MPI_COMM_WORLD, "\n**** Computing Classical product...\n") ;
  void *solCLA=NULL;
  ierr = produitClassique(&solCLA, MPI_COMM_WORLD) ; C_CHKERRQ(ierr) ;

  const int N = nbPts;

  auto myCtx = std::make_unique<contextTestFEMBEM>();
  std::memset(myCtx.get(), 0, sizeof(contextTestFEMBEM));
  myCtx->colDim = N;

  temps_initial = getTime ();
  Mpf_printf( MPI_COMM_WORLD, "\n**** Creating COMPOSYX ...\n" );

  int row_min = 1;
  int row_max = N;
  int col_min = 1;
  int col_max = N;
  int nnz = 0; // Must be initialized to 0
  NonZero * matComp = (NonZero *) calloc(1, sizeof(NonZero)); // For some reason, realloc is used on this

  freeFemMatrix();
  createFemMatrix();
  computeSparseBlockFEMBEM(&row_min, &row_max, &col_min, &col_max,
                           &Mpf_i_zero, &Mpf_i_one, &Mpf_i_one, &Mpf_i_one,
                           &nnz, (void **) &matComp, (void *) myCtx.get());
  freeFemMatrix();

  std::span<NonZero> matspan(matComp, nnz);

  std::vector<int> Ai(nnz), Aj(nnz);
  std::vector<Scalar> Av(nnz);
  int k = 0;
  for(auto elt : matspan){
    Ai[k] = elt.i; Aj[k] = elt.j;
    if constexpr(is_complex<Scalar>::value) {
      Av[k] =  Scalar{elt.v.r, elt.v.i};
    }
    else{
      Av[k] = elt.v;
    }
    k++;
  }

  free(matComp);

  SparseMatrixCOO<Scalar> A(N, N, static_cast<size_t>(nnz), std::move(Ai), std::move(Aj), std::move(Av));

  temps_final = getTime();
  temps_cpu = temps_final - temps_initial ;
  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuCOMPOSYXCreation = %f \n", temps_cpu) ;

  DenseMatrix<Scalar> B;
  DenseMatrix<Scalar> X;
  void *solCOMPOSYX = nullptr;

  switch(testedAlgo){
  case _gemvCOMPOSYX:

    Mpf_printf(MPI_COMM_WORLD, "\n**** Computing COMPOSYX product...\n") ;
    temps_initial = getTime();

    if (simplePrec)
      doubleToSimple(rhs, vectorSize);

    B = DenseMatrix<Scalar>::view(N, nbRHS, (Scalar*) rhs, N);

    solCOMPOSYX = MpfCalloc(vectorSize, sizeof(D_type)) ; C_CHKPTRQ(solCOMPOSYX) ;
    X = DenseMatrix<Scalar>::view(N, nbRHS, (Scalar*) solCOMPOSYX, N);

    X = A * B;

    temps_final = getTime();
    temps_cpu = temps_final - temps_initial ;

    if (simplePrec) {
      simpleToDouble(solCOMPOSYX, vectorSize);
      simpleToDouble(rhs, vectorSize);
    }

    Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuGEMVCOMPOSYX= %f \n", temps_cpu) ;

    // Compare les 2 produits matrice-vecteur

    Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
    ierr=computeRelativeError(solCOMPOSYX, solCLA, relative_error); C_CHKERRQ(ierr);

    Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);

    if(solCOMPOSYX) MpfFree(solCOMPOSYX);

    break;

  case _solveCOMPOSYX:
    {
      Mpf_printf(MPI_COMM_WORLD, "\n\n**** Factorizing COMPOSYX Mat...\n") ;
      temps_initial = getTime();

#ifdef COMPOSYX_USE_PASTIX
      Pastix<SparseMatrixCOO<Scalar>, DenseMatrix<Scalar>> solver(A);
      Mpf_printf(MPI_COMM_WORLD, "Using Pastix\n") ;
#elif defined COMPOSYX_USE_MUMPS
      Mumps<SparseMatrixCOO<Scalar>, DenseMatrix<Scalar>> solver(A);
      Mpf_printf(MPI_COMM_WORLD, "Using Mumps\n") ;
#else
      C_SETERRQ(1, "testCOMPOSYX : No direct solver (Pastix or Mumps) available in Composyx\n") ;
#endif
      solver.factorize();

      /*using Real = typename arithmetic_real<Scalar>::type;
      const Real tol = 1.0e-6;
      GMRES<SparseMatrixCOO<Scalar>, DenseMatrix<Scalar>> solver(A);
      solver.setup(parameters::verbose{true},
		   parameters::max_iter{500},
		   parameters::tolerance{tol});
		   */

      temps_final = getTime();
      temps_cpu = temps_final - temps_initial;

      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuCOMPOSYXFacto = %f \n", temps_cpu) ;
      temps_initial = getTime();

      if (simplePrec)
	doubleToSimple(solCLA, vectorSize);

      B = DenseMatrix<Scalar>::view(N, nbRHS, (Scalar*) solCLA, N);

      solCOMPOSYX = MpfCalloc(vectorSize, sizeof(D_type)) ; C_CHKPTRQ(solCOMPOSYX) ;
      X = DenseMatrix<Scalar>::view(N, nbRHS, (Scalar*) solCOMPOSYX, N);

      X = solver * B;

      temps_final = getTime();
      temps_cpu = temps_final - temps_initial;

      if (simplePrec) {
	simpleToDouble(solCOMPOSYX, vectorSize);
      }

      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuSolveCOMPOSYX = %f \n", temps_cpu);
      // Compare the two vectors solCOMPOSYX and rhs
      Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
      ierr=computeRelativeError(solCOMPOSYX, rhs, relative_error) ; C_CHKERRQ(ierr) ;
      Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);
    }

    break;
  default:
    break;
  }

  return 0;
}

template<typename Scalar>
int testCOMPOSYX_BEM(double * relative_error){

  int ierr = 0;
  double temps_initial, temps_final, temps_cpu;
  size_t vectorSize = (size_t) nbPts*nbRHS*(complexALGO?2:1);

  /* Realise le produit matrice vecteur classique : A.rhs -> solCLA */

  Mpf_printf(MPI_COMM_WORLD, "\n**** Computing Classical product...\n") ;
  void *solCLA=NULL;
  ierr = produitClassique(&solCLA, MPI_COMM_WORLD) ; C_CHKERRQ(ierr) ;

  const int N = nbPts;
  DenseMatrix<Scalar> A(N, N);

  auto myCtx = std::make_unique<contextTestFEMBEM>();
  std::memset(myCtx.get(), 0, sizeof(contextTestFEMBEM));
  myCtx->colDim = N;

  temps_initial = getTime ();
  Mpf_printf( MPI_COMM_WORLD, "\n**** Creating COMPOSYX ...\n" );

  int row_min = 1;
  int row_max = N;
  int col_min = 1;
  int col_max = N;
  int size_of_buffer = N * N;
  int ld = A.get_leading_dim();

  (simplePrec ?
   computeDenseBlockFEMBEM_Cprec :
   computeDenseBlockFEMBEM)(&row_min, &row_max, &col_min, &col_max,
			    &Mpf_i_zero, &Mpf_i_one, &Mpf_i_one, &Mpf_i_one,
			    &size_of_buffer, &ld, (void *) get_ptr(A), (void *) myCtx.get());

  if(symMatSolver){
    if(symMatContent == 1){
      A.set_property(MatrixSymmetry::symmetric);
    } else if(symMatContent == 2){
      A.set_spd(MatrixStorage::full);
    }
  }

  temps_final = getTime ();
  temps_cpu = temps_final - temps_initial ;
  if ( verboseTest ) {
    std::cout << A.properties_str() << '\n';
    const int nrows_show = std::min(N, 5);
    auto A_first_block = A.get_block_view(0, 0, nrows_show, nrows_show);
    A_first_block.display("First block 5x5");
  }
  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuCOMPOSYXCreation = %f \n", temps_cpu) ;

  if (computeNorm) {
    temps_initial = getTime();
    Mpf_printf(MPI_COMM_WORLD, "Norm: %.16g\n", A.norm());
    temps_final = getTime ();
    temps_cpu = temps_final - temps_initial ;
    Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuNorm = %f \n", temps_cpu) ;
  }

  DenseMatrix<Scalar> B;
  DenseMatrix<Scalar> X;
  void *solCOMPOSYX = nullptr;

  switch(testedAlgo){
  case _gemvCOMPOSYX:

    Mpf_printf(MPI_COMM_WORLD, "\n**** Computing COMPOSYX product...\n") ;
    temps_initial = getTime();

    if (simplePrec)
      doubleToSimple(rhs, vectorSize);

    B = DenseMatrix<Scalar>::view(N, nbRHS, (Scalar*) rhs, N);
    //B.display("B");

    solCOMPOSYX = MpfCalloc(vectorSize, sizeof(D_type)) ; C_CHKPTRQ(solCOMPOSYX) ;
    X = DenseMatrix<Scalar>::view(N, nbRHS, (Scalar*) solCOMPOSYX, N);

    X = A * B;

    temps_final = getTime();
    temps_cpu = temps_final - temps_initial ;

    if (simplePrec) {
      simpleToDouble(solCOMPOSYX, vectorSize);
      simpleToDouble(rhs, vectorSize);
    }

    Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuGEMVCOMPOSYX= %f \n", temps_cpu) ;

    /* Compare les 2 produits matrice-vecteur */

    Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
    ierr=computeRelativeError(solCOMPOSYX, solCLA, relative_error); C_CHKERRQ(ierr);

    Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);

    if (solCOMPOSYX) MpfFree(solCOMPOSYX);

    break;

  case _solveCOMPOSYX:
    {
      Mpf_printf(MPI_COMM_WORLD, "\n\n**** Factorizing COMPOSYX Mat...\n") ;
      temps_initial = getTime();

      BlasSolver<DenseMatrix<Scalar>, DenseMatrix<Scalar>> solver(A);
      solver.factorize();

      temps_final = getTime();
      temps_cpu = temps_final - temps_initial;

      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuCOMPOSYXFacto = %f \n", temps_cpu) ;
      temps_initial = getTime();

      if (simplePrec)
	doubleToSimple(solCLA, vectorSize);

      B = DenseMatrix<Scalar>::view(N, nbRHS, (Scalar*) solCLA, N);
      //B.display("B");

      solCOMPOSYX = MpfCalloc(vectorSize, sizeof(D_type)) ; C_CHKPTRQ(solCOMPOSYX) ;
      X = DenseMatrix<Scalar>::view(N, nbRHS, (Scalar*) solCOMPOSYX, N);

      X = solver * B;

      temps_final = getTime();
      temps_cpu = temps_final - temps_initial;

      if (simplePrec) {
	simpleToDouble(solCOMPOSYX, vectorSize);
      }

      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuSolveCOMPOSYX = %f \n", temps_cpu);
      /* Compare the two vectors solCOMPOSYX and rhs */
      Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
      ierr=computeRelativeError(solCOMPOSYX, rhs, relative_error) ; C_CHKERRQ(ierr) ;
      Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);
    }

    break;
  default:
    break;
  }

  return 0;
}

int testCOMPOSYX(double * relative_error){

  int ierr;

  if(testedModel == _bem){
    switch(stype) {
    case (SIMPLE_PRECISION) :
      ierr = testCOMPOSYX_BEM<float>(relative_error);
      break ;
    case (DOUBLE_PRECISION) :
      ierr = testCOMPOSYX_BEM<double>(relative_error);
      break ;
    case (SIMPLE_COMPLEX) :
      ierr = testCOMPOSYX_BEM<std::complex<float>>(relative_error);
      break ;
    case (DOUBLE_COMPLEX) :
      ierr = testCOMPOSYX_BEM<std::complex<double>>(relative_error);
      break ;
    default :
      C_SETERRQ(1, "testCOMPOSYX : unknown scalar type\n") ;
      break ;
    }
  }
  else{ // if(testedModel == _fem)
    switch(stype) {
    case (SIMPLE_PRECISION) :
      ierr = testCOMPOSYX_FEM<float>(relative_error);
      break ;
    case (DOUBLE_PRECISION) :
      ierr = testCOMPOSYX_FEM<double>(relative_error);
      break ;
    case (SIMPLE_COMPLEX) :
      ierr = testCOMPOSYX_FEM<std::complex<float>>(relative_error);
      break ;
    case (DOUBLE_COMPLEX) :
      ierr = testCOMPOSYX_FEM<std::complex<double>>(relative_error);
      break ;
    default :
      C_SETERRQ(1, "testCOMPOSYX : unknown scalar type\n") ;
      break ;
    }
  }

  return ierr;
}

#else // HAVE_COMPOSYX not defined
  int testCOMPOSYX(double *){ return 0; }
#endif // defined(HAVE_COMPOSYX)
