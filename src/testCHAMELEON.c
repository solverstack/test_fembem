#include "main.h"

/*! \brief Runs the test using CHAMELEON solver
 */
int testCHAMELEON(double * relative_error) {
#ifdef HAVE_CHAMELEON
  int ierr;
  double temps_initial, temps_final, temps_cpu;
  size_t vectorSize = (size_t)nbPts * nbRHS * (complexALGO ? 2 : 1);

  /* Realise le produit matrice vecteur classique : A.rhs -> solCLA */

  Mpf_printf(MPI_COMM_WORLD, "\n**** Computing Classical product...\n") ;
  void *solCLA=NULL;
  ierr = produitClassique(&solCLA, MPI_COMM_WORLD) ; CHKERRQ(ierr) ;

  /* Cree la Matrice CHAMELEON */

  testFEMBEM_initChameleon( stype );

  /* Get the default NB parameter */
  int NB ;
  ierr = CHAMELEON_Get(CHAMELEON_TILE_SIZE, &NB);

  /* Create a 2D grid of processes */
  int dims[2]={0,0}, np;
  ierr = MPI_Comm_size(MPI_COMM_WORLD, &np); CHKERRQ(ierr) ;
  ierr = MPI_Dims_create(np, 2, dims); CHKERRQ(ierr) ;

  temps_initial = getTime ();
  Mpf_printf( MPI_COMM_WORLD, "\n**** Creating MAT CHAMELEON...\n" );

  // CHAMELEON matrix descriptor for A
  CHAM_desc_t *descA;
  CHAMELEON_generate_matrix( chameleonType, NB, dims, &descA );
  temps_final = getTime ();
  temps_cpu = temps_final - temps_initial ;
  if ( verboseTest ) {
    void *lapackA = MpfCalloc( (size_t)nbPts * nbPts, sizeof(D_type) ) ; CHKPTRQ(lapackA);

    ierr = CHAMELEON_Desc2Lap( ChamUpperLower, descA, lapackA, nbPts ); CHKERRQ(ierr);
    ierr = displayArray("frA", lapackA, nbPts, nbPts) ; CHKERRQ(ierr) ;

    MpfFree( lapackA );
  }
  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuCHAMELEONCreation = %f \n", temps_cpu) ;

  /* Switch on the various CHAMELEON tests */

  switch(testedAlgo) {
    case _gemvCHAMELEON:

      /* Appelle le produit mat.vec : A.rhs -> solCHAM */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Computing CHAMELEON product...\n") ;
      temps_initial = getTime ();

      if (simplePrec)
        doubleToSimple(rhs, vectorSize);

      // CHAMELEON descriptor containing rhs
      CHAM_desc_t *descX;
      ierr = CHAMELEON_Desc_Create( &descX, CHAMELEON_MAT_ALLOC_TILE, chameleonType,
                                    NB, NB, NB*NB,
                                    nbPts, nbRHS, 0, 0,
                                    nbPts, nbRHS, 1, np ); CHKERRQ(ierr);
      ierr = CHAMELEON_Lap2Desc( ChamUpperLower, rhs, nbPts, descX); CHKERRQ(ierr);

      // CHAMELEON descriptor containing A.rhs
      CHAM_desc_t *descY;
      ierr = CHAMELEON_Desc_Create( &descY, CHAMELEON_MAT_ALLOC_GLOBAL, chameleonType,
                                    NB, NB, NB*NB,
                                    nbPts, nbRHS, 0, 0,
                                    nbPts, nbRHS, 1, np ); CHKERRQ(ierr);

      /* Compute the mat.vec product A.rhs -> solCHAM */
      if ( generate_traces ) {
        CHAMELEON_Enable( CHAMELEON_PROFILING_MODE );
      }
      CHAMELEON_gemm_Tile( descA, descX, descY );
      if ( generate_traces ) {
        CHAMELEON_Disable( CHAMELEON_PROFILING_MODE );
      }

      temps_final = getTime ();
      temps_cpu = temps_final - temps_initial ;
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuGEMVCHAMELEON = %f \n", temps_cpu) ;

      void *solCHAM = MpfCalloc( vectorSize, sizeof(D_type) ); CHKPTRQ(solCHAM);

      /* convert back to lapack format */
      ierr = CHAMELEON_Desc2Lap( ChamUpperLower, descY, solCHAM, nbPts); CHKERRQ(ierr);

      ierr = CHAMELEON_Desc_Destroy (&descX); CHKERRQ(ierr);
      ierr = CHAMELEON_Desc_Destroy (&descY); CHKERRQ(ierr);

      ASSERTQ(nbPts < INT_MAX/nbRHS);
      ierr = MPI_Bcast(solCHAM, nbRHS*nbPts, Mpf_mpitype[stype], 0, MPI_COMM_WORLD ); CHKERRQ(ierr);

      if (simplePrec) {
        simpleToDouble(solCHAM, vectorSize);
        simpleToDouble(rhs, vectorSize);
      }

      if ( verboseTest ) {
        ierr = displayArray("solCHAM", solCHAM, nbPts, nbRHS) ; CHKERRQ(ierr) ;
      }
      /* Compare the two vectors solCLA and solCHAM */
      Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
      ierr = computeRelativeError( solCHAM, solCLA, relative_error); CHKERRQ(ierr);

      Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);
      if (solCHAM) MpfFree(solCHAM) ;
      solCHAM=NULL ;

      break;

    case _solveCHAMELEON:

      /* Factorize the CHAMELEON Matrix */

      Mpf_printf(MPI_COMM_WORLD, "\n\n**** Factorizing CHAMELEON Mat...\n") ;
      if ( generate_traces ) {
        CHAMELEON_Enable( CHAMELEON_PROFILING_MODE );
      }
      temps_initial = getTime ();
      if (symMatSolver) {
        ierr = CHAMELEON_sytrf_Tile (ChamLower, descA); CHKERRQ(ierr);
      } else {
        ierr = CHAMELEON_getrf_nopiv_Tile (descA); CHKERRQ(ierr);
      }
      temps_final = getTime ();
      temps_cpu = temps_final - temps_initial ;
      if ( generate_traces ) {
        CHAMELEON_Disable( CHAMELEON_PROFILING_MODE );
      }
      if ( verboseTest ) {
        void *lapackA = MpfCalloc( (size_t)nbPts * nbPts, sizeof(D_type) ) ; CHKPTRQ(lapackA);

        ierr = CHAMELEON_Desc2Lap( ChamUpperLower, descA, lapackA, nbPts ); CHKERRQ(ierr);
        ierr = displayArray("chamA", lapackA, nbPts, nbPts) ; CHKERRQ(ierr) ;

        MpfFree( lapackA );
      }
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuCHAMELEONFacto = %f \n", temps_cpu) ;

      // CHAMELEON descriptor containing solCLA
      CHAM_desc_t *descB;
      ierr = CHAMELEON_Desc_Create( &descB, CHAMELEON_MAT_ALLOC_GLOBAL, chameleonType,
                                    NB, NB, NB*NB,
                                    nbPts, nbRHS, 0, 0,
                                    nbPts, nbRHS, 1, np ); CHKERRQ(ierr);
      if (simplePrec)
        doubleToSimple(solCLA, vectorSize);
      ierr = CHAMELEON_Lap2Desc( ChamUpperLower, solCLA, nbPts, descB); CHKERRQ(ierr);

      /* Solve the system A-1.solCLA -> solCLA */
      temps_initial = getTime ();
      if (symMatSolver) {
        ierr = CHAMELEON_sytrs_Tile( ChamLower, descA, descB ); CHKERRQ(ierr);
      } else {
        ierr = CHAMELEON_getrs_nopiv_Tile( descA, descB ); CHKERRQ(ierr);
      }
      temps_final = getTime ();
      temps_cpu = temps_final - temps_initial ;
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuSolveCHAMELEON = %f \n", temps_cpu) ;

      /* Convert back to lapack */
      ierr = CHAMELEON_Desc2Lap( ChamUpperLower, descB, solCLA, nbPts); CHKERRQ(ierr);

      ASSERTQ(nbPts < INT_MAX/nbRHS);
      ierr = MPI_Bcast(solCLA, nbRHS*nbPts, Mpf_mpitype[stype], 0, MPI_COMM_WORLD ); CHKERRQ(ierr);
      if (simplePrec)
        simpleToDouble(solCLA, vectorSize);

      ierr = CHAMELEON_Desc_Destroy (&descB); CHKERRQ(ierr);

      if ( verboseTest ) {
        ierr = displayArray("x", solCLA, nbPts, nbRHS) ; CHKERRQ(ierr) ;
      }

      /* Compare the two vectors solCLA and rhs */
      Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
      ierr=computeRelativeError(solCLA, rhs, relative_error) ; CHKERRQ(ierr) ;
      Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);
      break ;
    default:
      break;
  }

  /* Finish execution */

  ierr = CHAMELEON_Desc_Destroy (&descA); CHKERRQ(ierr);
  if (solCLA) MpfFree(solCLA) ;
  solCLA=NULL ;

#else
  Mpf_printf(MPI_COMM_WORLD, "No CHAMELEON support.\n") ;
#endif

  return 0;
}
