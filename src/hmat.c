#include "main.h"

int init_hmat_interface() {
#ifdef HAVE_HMAT
  if(mpf_hmat_settings.interface_initialized)
    return 0;
  for(int i = HMAT_SIMPLE_PRECISION; i <= HMAT_DOUBLE_COMPLEX; i++) {
    mpf_hmat_settings.interfaces[i] = MpfCalloc(1, sizeof(hmat_interface_t));
    switch(mpf_hmat_settings.engine) {
      case mpf_hmat_seq: hmat_init_default_interface(mpf_hmat_settings.interfaces[i], i); break;
#ifdef HMAT_HAVE_TOYRT
      case mpf_hmat_toyrt: hmat_init_toyrt_interface(mpf_hmat_settings.interfaces[i], i); break;
#endif
#ifdef HMAT_HAVE_STARPU
      case mpf_hmat_starpu: hmat_init_starpu_interface(mpf_hmat_settings.interfaces[i], i); break;
#endif
      default: break;
    }
    if (!mpf_hmat_settings.interfaces[i]->init)
      SETERRQ(1, "No valid HMatrix interface. Incorrect parameters ?");
    mpf_hmat_settings.interfaces[i]->init();
  }
  mpf_hmat_settings.interface_initialized = true;
#endif
  return 0;
}

int readFlagsTestHMAT(int *argc, char ***argv) {

  /* Flag to use optimized assembly or simple assembly */
  if (MpfArgHasName(argc, *argv, 1, "--use_simple_assembly")) {
    use_simple_assembly=1 ;
    Mpf_printf(MPI_COMM_WORLD, "Using simple assembly routine for HMAT\n") ;
  }

  /* Value of the Hmat clustering divider (number of children per box) */
  if (MpfArgGetInt(argc, *argv, 1, "--hmat_divider", &divider)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading Hmat clustering divider = %d\n", divider) ;
    ASSERTQ(divider>1);
  }

  /* Flag to compute the residual norm after the Hmat solve */
  if (MpfArgHasName(argc, *argv, 1, "--hmat_residual")) {
    hmat_residual=1;
    Mpf_printf(MPI_COMM_WORLD, "Computing the residual norm after the Hmat solve\n") ;
  }

  /* Flag to use Hmat iterative refinement */
  if (MpfArgHasName(argc, *argv, 1, "--hmat_refine")) {
    hmat_refine=1;
    Mpf_printf(MPI_COMM_WORLD, "Using Hmat iterative refinement\n") ;
    hmat_residual=1; // Iterative refinement needs the residual computation
  }

  /* Flag to use Hmat shuffle clustering */
  if (MpfArgHasName(argc, *argv, 1, "--use_hmat_shuffle")) {
    use_hmat_shuffle=1;
    Mpf_printf(MPI_COMM_WORLD, "Using Hmat shuffle clustering\n") ;
  }

  /* Value of the Hmat shuffle clustering minimum divider (minimum number of children per box) */
  if (MpfArgGetInt(argc, *argv, 1, "--hmat_divider_min", &divider_min)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading Hmat shuffle clustering minimum divider = %d\n", divider_min) ;
  }

  /* Value of the Hmat shuffle clustering maximum divider (maximum number of children per box) */
  if (MpfArgGetInt(argc, *argv, 1, "--hmat_divider_max", &divider_max)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading Hmat shuffle clustering maximum divider = %d\n", divider_max) ;
    ASSERTQ(divider_max>1);
    ASSERTQ(divider_max>=divider_min);
  }

  /* Flag to use HODLR storage and algorithms */
  if (MpfArgHasName(argc, *argv, 1, "--hodlr")) {
    use_hodlr=1;
    Mpf_printf(MPI_COMM_WORLD, "Using HODLR\n") ;
  }

  return 0;
}

/* Simple assembly function, as needed by hmat */
void interactionKernel(void* user_context, int i, int j, void* result) {
  int ierr;
  double coord_i[3], coord_j[3];
  double _Complex Aij = 0, Bij = 0;
  contextTestFEMBEM *myCtx = (contextTestFEMBEM *)user_context;

  // Update indices (if we compute only a sub-part of the whole matrix)
  i += myCtx->rowOffset;
  j += myCtx->colOffset;

  // BEM part
  if (i<nbPtsBEM && j<nbPtsBEM) { // if testedModel is not _bem or _fembem, nbPtsBEM is 0
    ierr=computeCoordCylinder(i, coord_i); CHKERRA(ierr);
    ierr=computeCoordCylinder(j, coord_j); CHKERRA(ierr);
    ierr=computeKernelBEM(&(coord_i[0]), &(coord_j[0]), i==j, &Aij); CHKERRA(ierr);
  }

  // FEM part
  if (testedModel == _fembem || testedModel == _fem) {
    ierr=computeKernelFEM(i, j, (Z_type*)&Bij); CHKERRA(ierr);
    Aij += Bij;
  }

  switch(stype) {
    case SIMPLE_PRECISION:
    case DOUBLE_PRECISION:
      *((double*)result) = Aij;
      break;
    case SIMPLE_COMPLEX:
    case DOUBLE_COMPLEX:
      *((double _Complex*)result) = Aij;
      break;
    default:
      SETERRA(1, "Unknown stype");
  }

  return;
}

/**
  This structure is a glue between hmat library and application.
*/
typedef struct {
  int row_start;
  int col_start;
  int row_count;
  int col_count;
  int leadingDim;
  int* row_hmat2client;
  int* col_hmat2client;
  double (*row_points)[3];
  double (*col_points)[3];
  void* user_context;
  // To tag dofs used
  int *dof_col_used, *dof_row_used;
} block_data_t;

static void free_block_data(void *v) {
  block_data_t* p = (block_data_t*) v;
  if (p->col_points) MpfFree(p->col_points);
  if (p->row_points) MpfFree(p->row_points);
  if (p->dof_col_used) MpfFree(p->dof_col_used);
  if (p->dof_row_used) MpfFree(p->dof_row_used);
  if (p) MpfFree(p);
}

/** Callback for hmat block info */
static char is_guaranteed_null_row(const hmat_block_info_t * info, int index, int stratum) {
  block_data_t* b_data = (block_data_t*) info->user_data;
  ASSERTA(b_data);
  ASSERTA(b_data->dof_row_used);
  ASSERTA(index>=0 && index<b_data->row_count);
  ASSERTA (stratum == -1);
  return(!b_data->dof_row_used[index]);
}

/** Callback for hmat block info */
static char is_guaranteed_null_col(const hmat_block_info_t * info, int index, int stratum) {
  block_data_t* b_data = (block_data_t*) info->user_data;
  ASSERTA(b_data);
  ASSERTA(b_data->dof_col_used);
  ASSERTA(index>=0 && index<b_data->col_count);
  ASSERTA (stratum == -1);
  return(!b_data->dof_col_used[index]);
}

/**
  prepare_hmat is called by hmat library to prepare assembly of
  a cluster block.  We allocate a block_data_t, which is then
  passed to the advanced_compute_hmat function.  We have to store all
  datas needed by advanced_compute_hmat into this block_data_t structure.
*/
void
prepare_hmat(int row_start,
             int row_count,
             int col_start,
             int col_count,
             int *row_hmat2client,
             int *row_client2hmat,
             int *col_hmat2client,
             int *col_client2hmat,
             void *user_context,
             hmat_block_info_t * info)
{
  // silent unused variables warning
  (void)row_client2hmat;
  ASSERTA(user_context);
  contextTestFEMBEM *myCtx = (contextTestFEMBEM *)user_context;

  info->user_data = MpfCalloc(1, sizeof(block_data_t));
  block_data_t* bdata = (block_data_t*) info->user_data;

  bdata->row_start       = row_start;
  bdata->col_start       = col_start;
  bdata->row_count       = row_count;
  bdata->col_count       = col_count;
  bdata->leadingDim      = row_count; // default value
  bdata->row_hmat2client = row_hmat2client;
  bdata->col_hmat2client = col_hmat2client;
  bdata->user_context    = (void*) user_context;
  info->release_user_data = free_block_data;

  /* Arrays to store non-null rows and columns */
  bdata->dof_row_used = MpfCalloc(row_count, sizeof(int)); CHKPTRA(bdata->dof_row_used);
  bdata->dof_col_used = MpfCalloc(col_count, sizeof(int)); CHKPTRA(bdata->dof_col_used);

  /* precompute and store the coordinates of the points (rows and columns)
     and tag BEM rows and columns as non-null
  */
  int i, j, col, row, ierr;
  size_t pos;
  bdata->row_points=(double(*)[3])MpfCalloc(row_count, 3*sizeof(double)); CHKPTRA(bdata->row_points);
  for (i = 0; i < row_count; ++i) {
    row = bdata->row_hmat2client ? bdata->row_hmat2client[i + bdata->row_start] : i + bdata->row_start;
    int row_g = row + myCtx->rowOffset;
    if (row_g < nbPtsBEM) {
      ierr=computeCoordCylinder(row_g, (double*)&(bdata->row_points[i])); CHKERRA(ierr);
      bdata->dof_row_used[i]=1; // tag this row as non-null
    }
  }
  bdata->col_points=(double(*)[3])MpfCalloc(col_count, 3*sizeof(double)); CHKPTRA(bdata->col_points);
  for (j = 0; j < col_count; ++j) {
    col = bdata->col_hmat2client ? bdata->col_hmat2client[j + bdata->col_start] : j + bdata->col_start;
    int col_g = col + myCtx->colOffset;
    if (col_g < nbPtsBEM) {
      ierr=computeCoordCylinder(col_g, (double*)&(bdata->col_points[j])); CHKERRA(ierr);
      bdata->dof_col_used[j]=1; // tag this column as non-null
    }
  }

  /* Identify non-null rows and columns in the FEM matrix */
  if (testedModel == _fembem || testedModel == _fem) {
    ASSERTA(z_matComp_FEM);
    ASSERTA(myCtx->colDim>0);
    for (i = 0; i < row_count; ++i) {
      // row index: 'i+row_start' in h-numbering, 'row' in client numbering
      row = bdata->row_hmat2client ? bdata->row_hmat2client[i + row_start] : i + row_start;
      int row_g = row + myCtx->rowOffset;
      // Line 'row' begins at position ptr_ordered_FEM[row] and ends before ptr_ordered_FEM[row+1]
      for (pos = ptr_ordered_FEM[row_g]; pos < ptr_ordered_FEM[row_g+1]; ++pos) {
        ASSERTA(z_matComp_FEM[pos].i == row_g);
        // column index: 'j+col_start' in h-numbering, 'col' in client numbering
        col = z_matComp_FEM[pos].j - myCtx->colOffset;
        if (col >= 0 && col < myCtx->colDim) { // this must be tested because 'z_matComp_FEM' can extend before and after the matrix currently assembled
          j = (col_client2hmat ? col_client2hmat[col] : col) - col_start;
          if (j >= 0 && j < col_count) {
            // we have a non zero element in this block at position (i,j)
            bdata->dof_row_used[i]=1;
            bdata->dof_col_used[j]=1;
          }
        }
      }
    }
  }

  info->is_guaranteed_null_col = is_guaranteed_null_col;
  info->is_guaranteed_null_row = is_guaranteed_null_row;

}

/**
  Compute all values of a block and store them into an array,
  which had already been allocated by hmat.  There is no
  padding, all values computed in this block are contiguous,
  and stored in a column-major order.
  This block is not necessarily identical to the one previously
  processed by prepare_hmat, it may be a sub-block.  In fact,
  it is either the full block, or a full column, or a full row.
*/
void
advanced_compute_hmat(struct hmat_block_compute_context_t *b) {
  int i, j, ierr;
  double *dValues = (double *) b->block;
  block_data_t* bdata = (block_data_t*) b->user_data;
  int step = complexALGO ? 2 : 1;
  double (*row_point)[3];
  double (*col_point)[3];

  ASSERTA(bdata->user_context);
  contextTestFEMBEM *myCtx = (contextTestFEMBEM *)bdata->user_context;

  col_point=&(bdata->col_points[b->col_start]);
  for (j = 0; j < b->col_count; ++j, col_point++) { // We could restrict to the col tagged in bdata->dof_col_used[]
    int col = bdata->col_hmat2client ? bdata->col_hmat2client[j + b->col_start + bdata->col_start] : j + b->col_start + bdata->col_start;
    int col_g = col + myCtx->colOffset;

    row_point=&(bdata->row_points[b->row_start]);
    for (i = 0; i < b->row_count; ++i, dValues+=step, row_point++) { // Idem with dof_row_used[]
      int row = bdata->row_hmat2client ? bdata->row_hmat2client[i + b->row_start + bdata->row_start] : i + b->row_start + bdata->row_start;
      int row_g = row + myCtx->rowOffset;
      double _Complex Aij = 0, Bij = 0;

      // BEM part
      if (row_g < nbPtsBEM && col_g < nbPtsBEM) {
        ierr=computeKernelBEM((double*)row_point, (double*)col_point, row_g==col_g, &Aij); CHKERRA(ierr);
      }

      // FEM part
      if (testedModel == _fembem || testedModel == _fem) {
        ierr=computeKernelFEM(row_g, col_g, (Z_type*)&Bij); CHKERRA(ierr);
        Aij += Bij;
      }

      switch(stype) {
        case SIMPLE_PRECISION:
        case DOUBLE_PRECISION:
          *((double*)dValues) = Aij;
          break;
        case SIMPLE_COMPLEX:
        case DOUBLE_COMPLEX:
          *(double _Complex*)dValues = Aij;
          break;
        default:
          SETERRA(1, "Unknown stype");
      }

    } /* for (i = 0; ... */

    // Advance to the start of the next column
    dValues += step * (bdata->leadingDim-bdata->row_count);

  } /* for (j = 0; ... */
}

/*! \brief Computes a block for the matrix of interactions.

  This routines works for double complex and double precision matrices only. It is compliant with
  the description given \ref pageCalculerBloc "here"

  \param LigInf first row of the block
  \param LigSup last row of the block
  \param ColInf first column of the block
  \param ColSup last column of the block
  \param ___Inf first matrix to compute
  \param ___Sup last matrix to compute
  \param iloc initial row for writing in \a bloc
  \param jloc initial column for writing in \a bloc
  \param tailleS dimension of each page of  \a bloc
  \param tailleL leading dimension of \a bloc
  \param bloc   output buffer
  \param context Pointer on the user's context, set with MpfSetUserContext()
*/
void computeDenseBlockFEMBEM(int *LigInf, int *LigSup, int *ColInf, int *ColSup,
                             int *___Inf, int *___Sup, int *iloc, int *jloc,
                             int *tailleS, int *tailleL, void *bloc, void *context) {
  ASSERTA(*iloc==1);
  ASSERTA(*jloc==1);
  ASSERTA(*___Inf==0);
  ASSERTA(*___Sup==1);
  ASSERTA(context);

  // Call prepare_block() to initialize the data for computing this matrix block
  hmat_block_info_t block_info;
  memset(&block_info, 0, sizeof(hmat_block_info_t));
  prepare_hmat(*LigInf-1, *LigSup-*LigInf+1, *ColInf-1, *ColSup-*ColInf+1,
               NULL, NULL, NULL, NULL, // All the mappings are NULL
               context, &block_info);
  // Set the actual leading dimension of 'bloc'
  ((block_data_t*)block_info.user_data)->leadingDim = *tailleL;

  struct hmat_block_compute_context_t b;
  b.user_data = block_info.user_data;
  b.row_start = 0;
  b.row_count = *LigSup-*LigInf+1;
  b.col_start = 0;
  b.col_count = *ColSup-*ColInf+1;
  b.block = bloc;
  b.stratum = -1;

  advanced_compute_hmat(&b);

  // We free the data allocated by prepare_block()
  free_block_data(block_info.user_data);
}

/*! \brief Computes a block for the matrix of interactions.

  This is exactly like computeDenseBlockFEMBEM() but in simple complex or simple precision arithmetics.
*/
void computeDenseBlockFEMBEM_Cprec(int *LigInf, int *LigSup, int *ColInf, int *ColSup,
                                   int *___Inf, int *___Sup, int *iloc, int *jloc,
                                   int *tailleS, int *tailleL, void *bloc, void *context) {
  int id, ic, il, inxBloc ;
  ASSERTA(context);

  void * zbloc = MpfCalloc((size_t)(*tailleS)*(*___Sup - *___Inf), sizeof(double)*(complexALGO?2:1)); CHKPTRA(zbloc) ;

  /* Appelle la routine en double precision */
  computeDenseBlockFEMBEM( LigInf, LigSup, ColInf, ColSup,
                           ___Inf, ___Sup, iloc, jloc,
                           tailleS, tailleL, zbloc, context) ;

  for (id = 0 ; id < *___Sup - *___Inf ; id++) {
    for (ic = ColInf[0]-1; ic < ColSup[0] ; ic++) {
      for (il = LigInf[0]-1; il < LigSup[0] ; il++) {
        inxBloc = (*tailleL)*(ic-ColInf[0] +1)  + il - LigInf[0] + 1 + (*iloc   - 1) + (*jloc   - 1) * (*tailleL) +
            (*tailleS)*id;
        if (stype == SIMPLE_COMPLEX) {
          ((C_type*)bloc)[inxBloc].r = (float)(((Z_type*)zbloc)[inxBloc].r) ;
          ((C_type*)bloc)[inxBloc].i = (float)(((Z_type*)zbloc)[inxBloc].i) ;
        } else {
          ((float*)bloc)[inxBloc]   = (float)(((double*)zbloc)[inxBloc]) ;
        }
      }
    }
  }

  MpfFree(zbloc);
}

#ifdef HAVE_HMAT
HMAT_desc_t *HMAT_generate_matrix( hmat_interface_t *hi ) {

  HMAT_desc_t *hdesc = MpfCalloc( 1, sizeof(HMAT_desc_t) );

  /* Create a cluster tree with a clustering algorithm */
  hmat_clustering_algorithm_t *clustering = NULL;
  hmat_cluster_tree_builder_t *ct_builder;

  if (use_hmat_shuffle) {
    clustering = hmat_create_shuffle_clustering( hmat_create_clustering_median(),
                                                 divider_min, divider_max );
    ct_builder = hmat_create_cluster_tree_builder( clustering );
  }
  else if ( use_ntiles_rec ) {
    hmat_clustering_algorithm_t *ntd_clustering = hmat_create_clustering_ntilesrecursive( tileSize );
    hmat_clustering_algorithm_t *med_clustering = hmat_create_clustering_median();
    hmat_set_clustering_divider( med_clustering, divider );
    //hmat_set_clustering_maxLeafSize( med_clustering, 10 );

    ct_builder = hmat_create_cluster_tree_builder( ntd_clustering );
    hmat_cluster_tree_builder_add_algorithm( ct_builder, 1, med_clustering );
  }
  else {
    clustering = hmat_create_clustering_median();
    if(mpf_hmat_settings.max_leaf_size > 0) {
      Mpf_printf(MPI_COMM_WORLD, "HMat maximum leaf size: %d\n", mpf_hmat_settings.max_leaf_size);
      clustering = hmat_create_clustering_max_dof(clustering, mpf_hmat_settings.max_leaf_size);
    }
    hmat_set_clustering_divider(clustering, divider);
    ct_builder = hmat_create_cluster_tree_builder( clustering );
  }

  double *points = testedModel==_bem ? createCylinder() : createPipe();
  hmat_cluster_tree_t *cluster_tree = hmat_create_cluster_tree_from_builder( points, 3, nbPts,
                                                                             ct_builder );
  MpfFree(points); points=NULL;
  hmat_delete_cluster_tree_builder( ct_builder );

  Mpf_printf(MPI_COMM_WORLD, "ClusterTree node count = %d\n", hmat_tree_nodes_count(cluster_tree));

  /* Create the H-matrix with this cluster tree and an admissibility criteria */
  hmat_admissibility_t *admissibilityCondition;
  if (use_hodlr)
    admissibilityCondition = hmat_create_admissibility_hodlr();
  else {
    hmat_admissibility_param_t admissibility_param;
    hmat_init_admissibility_param(&admissibility_param);
    admissibility_param.eta = 3.0;
    admissibilityCondition = hmat_create_admissibility(&admissibility_param);
  }

  //  hmat_admissibility_t *admissibilityCondition = hmat_create_admissibility_standard(3.0);
  hmat_matrix_t* hmatrix = hi->create_empty_hmatrix_admissibility(cluster_tree, cluster_tree, symMatSolver, admissibilityCondition);

  hi->set_low_rank_epsilon(hmatrix, mpf_hmat_settings.epsilon);

#ifdef HMAT_PARALLEL
  struct hmat_parallel_interface_t pi;
  hmat_parallel_interface(hi, &pi);
  if (pi.parallel_leaves)
    pi.parallel_leaves(hmatrix, mpf_hmat_settings.l0size);
#endif

  /* Assembly the Matrix */
  struct mpf_hmat_create_compression_args_t compression_ctx;
  compression_ctx.method = mpf_hmat_settings.compressionMethod;
  compression_ctx.threshold = mpf_hmat_settings.acaEpsilon;
  mpf_hmat_compression(&compression_ctx);

  hmat_assemble_context_t ctx;
  hmat_assemble_context_init(&ctx);
  ctx.progress = &mpf_hmat_settings.progress;
  ctx.progress->user_data = (void*)MPI_COMM_WORLD;
  ctx.factorization = hmat_factorization_none;
  ctx.lower_symmetric = symMatSolver;
  ctx.compression = (hmat_compression_algorithm_t*) compression_ctx.output;
  if (use_simple_assembly) {
    ctx.simple_compute = interactionKernel;
  } else {
    ctx.prepare = prepare_hmat;
    ctx.advanced_compute = advanced_compute_hmat;
  }
  contextTestFEMBEM *myCtx = MpfCalloc(1, sizeof(contextTestFEMBEM)) ; CHKPTRA(myCtx);
  myCtx->colDim = nbPts;
  ctx.user_context = myCtx;
  hi->assemble_generic(hmatrix, &ctx);

  hmat_delete_compression(ctx.compression);

  hdesc->clustering = clustering;
  hdesc->cluster_tree = cluster_tree;
  hdesc->admissibilityCondition = admissibilityCondition;
  hdesc->myCtx = myCtx;
  hdesc->hmatrix = hmatrix;

  return hdesc;
}

void HMAT_destroy_matrix( hmat_interface_t *hi,
                          HMAT_desc_t      *hdesc )
{
  /* Destroys the HMAT structure */
  if (hdesc) {
    hi->destroy( hdesc->hmatrix );
    hmat_delete_admissibility(hdesc->admissibilityCondition);
    hmat_delete_cluster_tree(hdesc->cluster_tree);
    hmat_delete_clustering(hdesc->clustering);
    if (hdesc->myCtx) MpfFree(hdesc->myCtx);
    MpfFree(hdesc);
  }
}
#endif
