#include "main.h"

/*! \brief Runs the test of H-matrices : gemv, solve
 */
int testHMAT(double * relative_error) {
#ifdef HAVE_HMAT
  enter_context("testHMAT()");
  int ierr, rank ;
  double temps_initial, temps_final, temps_cpu;
  size_t vectorSize = (size_t)nbPts*nbRHS*(complexALGO?2:1);

  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> HAcaAccuracy = %.4e \n", mpf_hmat_settings.acaEpsilon);
  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> HRecompressionAccuracy = %.4e \n", mpf_hmat_settings.epsilon);

  // String to append to the timer printed when doing asynchronous computation
  char *postfix_async = hmat_get_sync_exec() ? "" : "_submission";

  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank); CHKERRQ(ierr);

  /* Realise le produit matrice vecteur classique : A.rhs -> solCLA */

  Mpf_printf(MPI_COMM_WORLD, "\n**** Computing Classical product...\n") ;
  void *solCLA=NULL;
  ierr = produitClassique(&solCLA, MPI_COMM_WORLD) ; CHKERRQ(ierr) ;

  /* Cree la H-Matrice */

  temps_initial = getTime ();
  Mpf_printf(MPI_COMM_WORLD, "\n**** Creating HMAT...\n") ;

  /* We use the hmat interface initialized in MPF (default is starpu, --hmat-seq or --hmat-toyrt to change it) */
  hmat_interface_t *hi = mpf_hmat_settings.interfaces[stype];

  // hmat_factorization_none mean the user did not chose a factorization so we
  // must choose one for him
  if(mpf_hmat_settings.factorization_type == hmat_factorization_none) {
    if (use_hodlr) {
      if(symMatSolver)
        mpf_hmat_settings.factorization_type = hmat_factorization_hodlrsym;
      else
        mpf_hmat_settings.factorization_type = hmat_factorization_hodlr;
    } else {
      if(symMatSolver)
        // use LDLT for real matrices
        mpf_hmat_settings.factorization_type = complexALGO ? hmat_factorization_llt : hmat_factorization_ldlt;
      else
        mpf_hmat_settings.factorization_type = hmat_factorization_lu;
    }
  }

  /* Check the coherency between the factorization chosen in MPF (with --hmat-lu/llt/ldlt/hodlr/hodlr-sym)
     and the symmetry of the matrix (chosen with --sym/--nosym/--symcont) */
  if (testedAlgo==_solveHMAT)
    switch(mpf_hmat_settings.factorization_type){
      case hmat_factorization_hodlrsym:
      case hmat_factorization_ldlt:
      case hmat_factorization_llt:ASSERTQ(symMatSolver); break;
      // No 'hmat_factorization_hodlr' here, because it works on sym matrices
      case hmat_factorization_lu:ASSERTQ(!symMatSolver); break;
      default:break;
    }

  HMAT_desc_t *hdesc;
  hdesc = HMAT_generate_matrix( hi );
  hmat_matrix_t *hmatrix = hdesc->hmatrix;
  temps_final = getTime ();
  temps_cpu = temps_final - temps_initial ;
  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuCreation%s = %f \n", postfix_async, temps_cpu) ;

  if ( verboseTest ) {
    struct hmat_get_values_context_t ctxt_getval;
    size_t mtxSize = (size_t)nbPts*nbPts*(complexALGO?2:1);

    ctxt_getval.matrix = hmatrix;
    ctxt_getval.values = MpfCalloc( mtxSize, sizeof(D_type) );
    ctxt_getval.row_offset = 0;
    ctxt_getval.col_offset = 0;
    ctxt_getval.row_size   = nbPts;
    ctxt_getval.col_size   = nbPts;
    //ctxt_getval.hmat_numbering = 1;

    int *row_ptr = malloc(nbPts * sizeof(int));
    int *col_ptr = malloc(nbPts * sizeof(int));
    for (int k=0; k<nbPts; k++) row_ptr[k] = k+1;
    for (int k=0; k<nbPts; k++) col_ptr[k] = k+1;

    ctxt_getval.row_indices = row_ptr;
    ctxt_getval.col_indices = col_ptr;

    int ret = hi->get_values( &ctxt_getval );
    printf("get_values() returned %d\n", ret);

    free(row_ptr);
    free(col_ptr);

    displayArray( "frA", ctxt_getval.values, nbPts, nbPts );

    MpfFree( ctxt_getval.values );
  }

  /* Compute & display some informations (synchronous) */
  if (computeNorm) {
    temps_initial = getTime ();
    Mpf_printf(MPI_COMM_WORLD, "Norm: %.16g\n", hi->norm(hmatrix));
    temps_final = getTime ();
    temps_cpu = temps_final - temps_initial ;
    Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuNorm = %f \n", temps_cpu) ;
  }
  if (computeNorm || hmat_get_sync_exec()) {
    hmat_info_t info;
    hi->get_info(hmatrix, &info);
    Mpf_printf(MPI_COMM_WORLD, "Compressed size: %ld\n", info.compressed_size);
    Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> AssembledSizeMb = %f \n", (double)info.compressed_size*Mpf_stype_size[stype]/1024./1024.) ;
    Mpf_printf(MPI_COMM_WORLD, "Uncompressed size: %ld\n", info.uncompressed_size);
    Mpf_printf(MPI_COMM_WORLD, "Ratio: %g\n", (double)info.compressed_size/info.uncompressed_size);
  }

  /* Release the FEM matrix (can be quite large) */
  ierr=freeFemMatrix();CHKERRQ(ierr);

  /* Switch on the various HMAT tests */

  switch(testedAlgo) {
    case _gemvHMAT:

      /* Appelle le produit mat.vec : A.rhs -> solHMAT */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Computing HMAT product...\n") ;
      void *solHMAT = MpfCalloc(vectorSize, sizeof(D_type)) ; CHKPTRQ(solHMAT) ;
      temps_initial = getTime ();

      if (simplePrec)
        doubleToSimple(rhs, vectorSize);

      /* Compute the mat.vec product A.rhs -> solHMAT */
      ierr = hi->gemv('N', Mpf_pone[stype], hmatrix, rhs, Mpf_zero[stype], solHMAT, nbRHS);

      if (simplePrec) {
        simpleToDouble(solHMAT, vectorSize);
        simpleToDouble(rhs, vectorSize);
      }

      if (rank) {
        if (solHMAT) MpfFree(solHMAT);
        solHMAT=NULL;
      }
      temps_final = getTime ();
      temps_cpu = (temps_final - temps_initial) ;
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuGEMV%s = %f \n", postfix_async, temps_cpu) ;
      if (verboseTest) {
        ierr = displayArray("solHMAT", solHMAT, nbPts, nbRHS) ; CHKERRQ(ierr) ;
      }

      /* Compare les 2 produits matrice-vecteur */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
      ierr=computeRelativeError(solHMAT, solCLA, relative_error); CHKERRQ(ierr);

      Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);
      if (solHMAT) MpfFree(solHMAT);
      solHMAT=NULL ;
      break;

    case _solveHMAT:
      Mpf_printf(MPI_COMM_WORLD, "\n") ;

      hmat_matrix_t* hmatrix_orig = NULL;
      void *solCLA_orig = NULL;
      void *solCLA_sum = NULL;
      double normRes, normOrig, temps_refine=-getTime();
      int nbIter = 0;
      if (hmat_residual) {
        hmatrix_orig = hi->copy(hmatrix); // ideally, we do a copy with lower accuracy for factorisation
        /* Vector to duplicate solCLA */
        solCLA_orig = MpfCalloc(vectorSize, sizeof(D_type)) ; CHKPTRQ(solCLA_orig) ;
        /* Norm of the originaal RHS */
        ierr = computeVecNorm(solCLA, &normOrig); CHKERRQ(ierr);
      }
      if (hmat_refine) {
        /* Vector to accumulate solCLA */
        solCLA_sum = MpfCalloc(vectorSize, sizeof(D_type)) ; CHKPTRQ(solCLA_sum) ;
      }

      /* Factorize the H-matrix */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Factorizing HMAT...\n") ;
      temps_initial = getTime ();

      hmat_factorization_context_t ctx;
      hmat_factorization_context_init(&ctx);
      ctx.progress = &mpf_hmat_settings.progress;
      ctx.progress->user_data = (void*)MPI_COMM_WORLD;
      ctx.factorization = mpf_hmat_settings.factorization_type;
      ierr = hi->factorize_generic(hmatrix, &ctx); CHKERRQ(ierr);

      temps_final = getTime ();
      temps_cpu = (temps_final - temps_initial) ;
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuFacto%s = %f \n", postfix_async, temps_cpu) ;

      temps_refine -= temps_cpu; // We do not count facto in the iterative refinement timer

      if ( verboseTest )
      {
        struct hmat_get_values_context_t ctxt_getval;
        size_t mtxSize = (size_t)nbPts*nbPts*(complexALGO?2:1);

        ctxt_getval.matrix = hmatrix;
        ctxt_getval.values = MpfCalloc( mtxSize, sizeof(D_type) );
        ctxt_getval.row_offset = 0;
        ctxt_getval.col_offset = 0;
        ctxt_getval.row_size   = nbPts;
        ctxt_getval.col_size   = nbPts;
        //ctxt_getval.hmat_numbering = 1;

        int *row_ptr = malloc(nbPts * sizeof(int));
        int *col_ptr = malloc(nbPts * sizeof(int));
        for (int k=0; k<nbPts; k++) row_ptr[k] = k+1;
        for (int k=0; k<nbPts; k++) col_ptr[k] = k+1;

        ctxt_getval.row_indices = row_ptr;
        ctxt_getval.col_indices = col_ptr;

        int ret = hi->get_values( &ctxt_getval );
        printf("get_values() returned %d\n", ret);

        free(row_ptr);
        free(col_ptr);

        displayArray( "hmatA", ctxt_getval.values, nbPts, nbPts );

        MpfFree( ctxt_getval.values );
      }

      /* Compute & display some informations (synchronous) */
      if (computeNorm) {
        temps_initial = getTime ();
        Mpf_printf(MPI_COMM_WORLD, "Norm (factorized): %.16g\n", hi->norm(hmatrix));
        temps_final = getTime ();
        temps_cpu = temps_final - temps_initial ;
        Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuNormFacto = %f \n", temps_cpu) ;
      }
      if (computeNorm || hmat_get_sync_exec()) {
        hmat_info_t info;
        hi->get_info(hmatrix, &info);
        Mpf_printf(MPI_COMM_WORLD, "Compressed size: %ld\n", info.compressed_size);
        Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> FactorizedSizeMb = %f \n", (double)info.compressed_size*Mpf_stype_size[stype]/1024./1024.) ;
        Mpf_printf(MPI_COMM_WORLD, "Uncompressed size: %ld\n", info.uncompressed_size);
        Mpf_printf(MPI_COMM_WORLD, "Ratio: %g\n", (double)info.compressed_size/info.uncompressed_size);
      }

      /* Solve the system : A-1.solCLA -> solCLA */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Solve HMAT%s...\n", hmat_refine ? " with Iterative Refinement" : "") ;

      if ( verboseTest ) {
        ierr = displayArray("b", solCLA, nbPts, nbRHS) ; CHKERRQ(ierr) ;
      }

_refineLoop: // Iterative Refinement loop
      nbIter++ ;
      if (hmat_residual) {
        /* Duplicate the RHS solCLA into solCLA_orig */
        memcpy(solCLA_orig, solCLA, vectorSize*sizeof(D_type));
      }

      temps_initial = getTime ();

      if (simplePrec)
        doubleToSimple(solCLA, vectorSize);

      ierr = hi->solve_systems(hmatrix, solCLA, nbRHS); CHKERRQ(ierr);

      if (simplePrec)
        simpleToDouble(solCLA, vectorSize);

      temps_final = getTime ();
      temps_cpu = (temps_final - temps_initial) ;
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuSolve%s = %f \n", postfix_async, temps_cpu) ;

      /* Compute the residual solCLA_orig-A_orig.solCLA */

      if (hmat_residual) {
        temps_initial = getTime ();
        if (simplePrec) {
          doubleToSimple(solCLA, vectorSize);
          doubleToSimple(solCLA_orig, vectorSize);
        }
        ierr = hi->gemv('N', Mpf_mone[stype], hmatrix_orig, solCLA, Mpf_pone[stype], solCLA_orig, nbRHS);
        if (simplePrec) {
          simpleToDouble(solCLA, vectorSize);
          simpleToDouble(solCLA_orig, vectorSize);
        }
        temps_final = getTime ();
        temps_cpu = (temps_final - temps_initial) ;
        Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuGEMV%s = %f \n", postfix_async, temps_cpu) ;
        ierr = computeVecNorm(solCLA_orig, &normRes); CHKERRQ(ierr);
        Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Residual_%d = %.4e \n", nbIter, normRes/normOrig);
        // Test if convergence is achieved or if iteration number is too large
        if (normRes/normOrig < mpf_hmat_settings.acaEpsilon/10 || nbIter > 20) {
          // TODO stop the computation if the residual norm increases
          Mpf_printf(MPI_COMM_WORLD, "\n") ;
          Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> NbIterRefinement = %d \n", nbIter);
          hmat_refine=0;
        }
      }
      if (solCLA_sum) {
        /* Compute the updated solution solCLA_sum += solCLA */
        if (rank==0) {
          size_t k;
          for (k=0 ; k<vectorSize ; k++)
            ((double*)solCLA_sum)[k] += ((double*)solCLA)[k];
        }
        ierr=computeRelativeError(solCLA_sum, rhs, relative_error) ; CHKERRQ(ierr) ;
        Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error_%d = %.4e \n", nbIter, *relative_error);
      }
      if (hmat_refine) {
        /* Copy the updated RHS of the system solCLA_orig into solCLA */
        memcpy(solCLA, solCLA_orig, vectorSize*sizeof(D_type));
        goto _refineLoop;
      }

      if (hmatrix_orig) hi->destroy(hmatrix_orig);
      hmatrix_orig=NULL;
      if (solCLA_orig) MpfFree(solCLA_orig);
      solCLA_orig=NULL ;
      if (solCLA_sum) { // If solCLA_sum has been allocated, I use it as a replacement for solCLA
        MpfFree(solCLA) ;
        solCLA=solCLA_sum ;
        temps_refine += getTime();
        Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuRefinement%s = %f \n", postfix_async, temps_refine) ;

      }

      if ( verboseTest ) {
        ierr = displayArray("x", solCLA, nbPts, nbRHS) ; CHKERRQ(ierr) ;
      }

      /* Compare the two vectors solCLA and rhs */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
      ierr=computeRelativeError(solCLA, rhs, relative_error); CHKERRQ(ierr);

      Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);
      break;
    case _inverseHMAT:

      /* Inverse the H-matrix */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Inversing HMAT...\n") ;
      temps_initial = getTime ();

      hi->inverse(hmatrix);

      temps_final = getTime ();
      temps_cpu = (temps_final - temps_initial) ;
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuInversion%s = %f \n", postfix_async, temps_cpu) ;

      /* Compute the gemv : A-1.solCLA -> solCLA */

      Mpf_printf(MPI_COMM_WORLD, "\n**** GEMV HMAT...\n") ;
      temps_initial = getTime ();

      if (simplePrec)
        doubleToSimple(solCLA, vectorSize);

      void *solCLA2 = MpfCalloc(vectorSize, sizeof(D_type)) ; CHKPTRQ(solCLA) ;
      memcpy(solCLA2, solCLA, vectorSize*sizeof(D_type));
      ierr = hi->gemv('N', Mpf_pone[stype], hmatrix, solCLA2, Mpf_zero[stype], solCLA, nbRHS);
      MpfFree(solCLA2);

      if (simplePrec)
        simpleToDouble(solCLA, vectorSize);

      temps_final = getTime ();
      temps_cpu = (temps_final - temps_initial) ;
      Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuGEMV%s = %f \n", postfix_async, temps_cpu) ;

      /* Compare the two vectors solCLA and rhs */

      Mpf_printf(MPI_COMM_WORLD, "\n**** Comparing results...\n") ;
      ierr=computeRelativeError(solCLA, rhs, relative_error); CHKERRQ(ierr);

      Mpf_printf(MPI_COMM_WORLD, "<PERFTESTS> Error = %.4e \n", *relative_error);
      break;
    default:
      break;
  } /* switch(testedAlgo) */

#ifdef TESTFEMBEM_DUMP_HMAT
  hi->dump_info(hmatrix, "testHMAT_matrix");
#endif
  /* Destroys the HMAT structure */
  HMAT_destroy_matrix( hi, hdesc );
  MpfFree(solCLA);
  hmat_tracing_dump("testHMAT_trace.json");

  leave_context();

#else
  Mpf_printf(MPI_COMM_WORLD, "No HMat support.\n") ;
#endif

  return 0;
}
