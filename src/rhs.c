#include "main.h"
#include <mpf_simd.h>

extern double height;

MPF_SIMD_DISP(int computeRhs(void)) {
  double coord[3], OP[3] ;
  int i, j, ierr ;
  double complex *z_rhs=rhs ;
  D_type *d_rhs=rhs ;
  double small = simplePrec ? 1.e-20 : 1.e-100;
  double k=2.*M_PI/lambda;
  int (*computeCoord)(int, double*) = testedModel == _bem ? computeCoordCylinder : computeCoordPipe ;

  for (i=0 ; i<nbPts ; i++)
    if (i % sparseRHS == 0) {
      // Insert non-zero values on 1 value every 'sparseRHS'
      ierr = computeCoord(i, &(coord[0])) ; CHKERRQ(ierr);
      for (j=0 ; j<nbRHS ; j++) {
        if (newRHS) {
          if (complexALGO) {
            // OP = fake incident plane wave coming from direction rotating around the object
            ierr = computeCoord( (size_t)j*nbPts/nbRHS%nbPts , &(OP[0])) ; CHKERRQ(ierr);
            OP[2] -= height/2.; // to have plane waves coming from below too
            z_rhs[(size_t)j * nbPts + i] = mpf_simd_cexp(
              I * k * (coord[0] * OP[0] + coord[1] * OP[1] + coord[2] * OP[2]));
          } else {
            d_rhs[(size_t)j*nbPts+i]=cos(coord[0]/(j+1)+coord[1]+sin(j)*coord[2])+sin(j) ;
          }
        } else {
          // Old way of computing RHS. They were actually always rank 2! Not suitable to really test mat-rhs...
          if (complexALGO) {
            z_rhs[(size_t)j * nbPts + i] =
                cos(coord[0] + coord[1] + coord[2]) + sin(j) +
                I * (coord[0] - coord[1] + coord[2] - cos(j));
          } else {
            d_rhs[(size_t)j*nbPts+i]=cos(coord[0]+coord[1]+coord[2])+sin(j) ;
          }
        }
      }
    } else {
      // Insert zero everywhere else (in fact we put a 'very small' value
      // instead of zero, otherwise run-time optimisations produce unrealistic timers with FMM)
      for (j=0 ; j<nbRHS ; j++) {
        if (complexALGO) {
          z_rhs[(size_t)j * nbPts + i] = small + I * small;
        } else {
          d_rhs[(size_t)j*nbPts+i]=small ;
        }
      }
    }

  return 0 ;
}

void computeDenseBlockData (int *LigInf, int *LigSup, int *ColInf, int *ColSup, int *___Inf, int *___Sup, int *iloc, int *jloc, int *tailleS, int *tailleL, void *bloc, void *context) {
  int tailleL__  ;
  int iloc_____  ;
  int jloc_____  ;
  int il,ic,inxBloc;
  size_t inxGlob ;
  S_type *s_bloc=bloc ;
  D_type *d_bloc=bloc ;
  C_type *c_bloc=bloc ;
  Z_type *z_bloc=bloc ;
  ASSERTA(context);
  contextTestFEMBEM *myCtx = (contextTestFEMBEM *)context;
  Z_type *z_data=(Z_type *)myCtx->dataVec ;
  D_type *d_data=(D_type *)myCtx->dataVec ;

  tailleL__ = *tailleL   ;
  iloc_____ = *iloc   - 1;
  jloc_____ = *jloc   - 1;

  for (ic = ColInf[0]-1; ic < ColSup[0] ; ic++) {
    for (il = LigInf[0]-1; il < LigSup[0] ; il++) {
      inxBloc = tailleL__*(ic-ColInf[0]+1)  + il-LigInf[0]+1  + iloc_____ + jloc_____ * tailleL__ ;
      inxGlob = (size_t)(ic + myCtx->colOffset) * nbPts + (il  + myCtx->rowOffset);

      switch (stype) {
        case SIMPLE_PRECISION:
          s_bloc[inxBloc] = (S_type)d_data[inxGlob];
          break;
        case DOUBLE_PRECISION:
          d_bloc[inxBloc] = d_data[inxGlob];
          break;
        case SIMPLE_COMPLEX:
          c_bloc[inxBloc].r = (S_type)z_data[inxGlob].r;
          c_bloc[inxBloc].i = (S_type)z_data[inxGlob].i;
          break;
        case DOUBLE_COMPLEX:
          z_bloc[inxBloc] = z_data[inxGlob];
          break;
        default:
          break;
      } /* switch (stype) ... */
    }
  }
}
