#include "main.h"

extern double radius, height, meshStep;

/*! \brief Number of point on one loop around the pipe */
static int n_l=0;
/*! \brief Number of point on one radius of the pipe */
static int n_r=0;
/*! \brief Number of point on one height the pipe */
static int n_z=0;

/* routines de tri dans MAT/IMPLS/SPARSE/matutils_sparse.c */
extern int compare_ZnonZero_Line(const void *pt1, const void *pt2) ;

/*! \brief Reads command line arguments defining the shape of the cylinder

  \return 0 for success
*/
int initPipe(int *argc, char ***argv) {
  int ierr;
  double coord[3];

  if (MpfArgGetDouble(argc, *argv, 1, "-radius", &radius)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading radius = %f\n", radius);
  }
  if (MpfArgGetDouble(argc, *argv, 1, "-height", &height)) {
    Mpf_printf(MPI_COMM_WORLD, "Reading height = %f\n", height);
  }

  /* Calcule les coordonnees d'un point, afin de fixer meshStep (qui peut servir a fixer lambda) */
  ierr=computeCoordPipe(0, &(coord[0])); CHKERRQ(ierr);

  ierr=createFemMatrix();CHKERRQ(ierr);
  return 0;
}

/*! \brief Computes coordinates of point number 'i'

  The points are computed on a pipe defined by 'radius' and 'height'. The number of points is given by nbPts.
  Points are located on several helices, equally spaced in direction z, radial and orthoradial.
  \a i should be in [0, nbPts[.
  \param i the number of the point (input)
  \param coord the coordinates x y z of this point (output)
  \return 0 for success
*/
int computeCoordPipe(int i, double *coord) {
  static double angleStep=0.;
  static double zStep=0.;
  double theta;

  if (angleStep==0.) {
    /* The pipe is defined as follows: its height is 'height', the points are located between 'radius'
       and 'radius/2'. We have n_r, n_z, n_l points in the direction radial, z and orthoradial.
       total number of points nbPts=n_r.n_z.n_l
       2.pi*radius/n_l = heigth/n_z = (radius/2)/n_r = meshStep
       (rigorously, we should solve: 2.pi*radius/n_l = heigth/(n_z-1) = (radius/2)/(n_r-1) = meshStep)
       We compute the number of points as double and round them up to integers.
       angleStep*n_l = 2.pi
       zStep=heigth/(n_z.n_l)
    */
    double dn_z=pow(height*height*(double)nbPts/M_PI/radius/radius, 1./3.);
    double dn_r=radius*dn_z/2./height;
    double dn_l=4.*M_PI*dn_r;
    n_l=(int)ceil(dn_l);
    if (conformalMesh && n_l%2 == 0) n_l++; // To create a conformal mesh, n_l must be odd
    n_r=(int)ceil(dn_r);
    n_z=(int)ceil(dn_z);
    while(n_l*n_r*n_z < nbPts) n_z++;
    meshStep=height/(double)n_z;
    zStep=meshStep/(double)n_l;
    angleStep=2.*M_PI/(double)n_l;

    // In FEM-BEM, only the outer helix uses BEM. In pure FEM, no BEM at all.
    nbPtsBEM = testedModel==_fembem ? n_z*n_l : 0;
    nbPtsMain = nbPtsBEM;

    Mpf_printf(MPI_COMM_WORLD, "Pipe dimensions: dn_r=%g n_r=%d dn_l=%g n_l=%d dn_z=%g n_z=%d meshStep=%g zStep=%g angleStep=%g\n", dn_r, n_r, dn_l, n_l, dn_z, n_z, meshStep, zStep, angleStep);
  }

  if (i<0 || i>=n_z*n_l*n_r) {
    SETERRQ(1, "Incorrect unknown number %d, should be in [0, %d[\n", i, n_z*n_l*n_r);
  }

  /* The number of point per helix is n_z.n_l
     The remainder of the division gives the index local to the helix (between 0 and n_z.n_l-1),
     the quotient gives the number of the helix (between 0 and n_r-1).
     To have exactly nbPts points, the last helix might be incomplete.
     */
  div_t res;
  res=div(i, n_z*n_l);

  theta = (double)res.rem * angleStep;
  double rad = radius - (double)res.quot * meshStep;
  coord[0] = rad * sin(theta);
  coord[1] = rad * cos(theta);
  coord[2] = (double)res.rem * zStep;

  return 0;
}

double* createPipe(void) {
  double* result = MpfCalloc((size_t)3 * nbPts, sizeof(double)); CHKPTRA(result);
  int i, ierr;
#pragma omp parallel for private(ierr) schedule(dynamic,10000)
  for (i = 0; i < nbPts; i++) {
    ierr=computeCoordPipe(i, &(result[(size_t)3*i]) ) ; CHKERRA(ierr) ;
  }
  return result;
}

/*! \brief  allocated size of z_matComp_FEM[] */
static size_t nbEl_FEM = 0;

static int addValue(int lig, int col, double val) {

  // With option partial_fembem, we nullify two thirds of the interaction between BEM and FEM unknowns:
  // 2/3 of the rows in BEM x FEM, of the cols in FEM x BEM
  if (partial_fembem)
    if ( ( lig < nbPtsBEM && col >= nbPtsBEM && lig % 3 != 0 ) ||
         ( lig >= nbPtsBEM && col < nbPtsBEM && col % 3 != 0 ) )
      return 0;

  static int SPARSE_ASSEMBLY_CHUNK=100000;
  if (nnz_FEM == nbEl_FEM){
    nbEl_FEM=nnz_FEM+SPARSE_ASSEMBLY_CHUNK; /* nbEl contient la taille allouee pour d_matComp */
    z_matComp_FEM=(ZnonZero*)MpfRealloc(z_matComp_FEM,nbEl_FEM*sizeof(ZnonZero)); CHKPTRQ(z_matComp_FEM) ;
  }
  /* ajoute le nouvel element */
  z_matComp_FEM[nnz_FEM].i = lig;
  z_matComp_FEM[nnz_FEM].j = col;
  z_matComp_FEM[nnz_FEM].v.r = val;
  z_matComp_FEM[nnz_FEM].v.i = -val/3.;
  nnz_FEM++;
  return 0;
}

int freeFemMatrix() {
  if (z_matComp_FEM) MpfFree(z_matComp_FEM);
  z_matComp_FEM=NULL;
  if (ptr_ordered_FEM) MpfFree(ptr_ordered_FEM);
  ptr_ordered_FEM=NULL;
  nnz_FEM=0;
  nbEl_FEM=0;
  return 0;
}

int createFemMatrix() {
  int ierr, rank, iVtk;
  // in a conformal mesh, we alternatively have 'A' and 'B' hexa to match diagonal edges between consecutive hexa
  // in a non-conformal mesh (the old way), we use only 'A' hexa
  // we can switch between the 2 types with --conformal-mesh and --no-conformal-mesh, conformal is the default
  int defTetra[2][5][4]= { { {0,1,2,4}, {1,4,5,7}, {1,2,3,7}, {2,4,6,7}, {1,2,4,7}}, // 'A' hexa: face 0246 is split along 24
                            { {0,1,3,5}, {0,2,3,6}, {0,4,5,6}, {3,5,6,7}, {0,3,5,6}} }; // 'B' hexa: face 0246 is split along 06
  FILE *fvtk=NULL, *funv=NULL;
  int nbTetra=0, nbTria=0;
  fpos_t posNbTetra;
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank==0) {
    if (writeMesh) {
      fvtk=fopen("mesh.vtu", "w");
      // VTK Header
      fprintf(fvtk, "<?xml version=\"1.0\"?>\n");
      fprintf(fvtk, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" "
                    "byte_order=\"LittleEndian\" compressor=\"vtkZLibDataCompressor\">\n");
      fprintf(fvtk, "\t<UnstructuredGrid>\n");
      fprintf(fvtk, "\t\t<Piece NumberOfPoints=\"%d\" NumberOfCells=\"", nbPts);
      fgetpos(fvtk, &posNbTetra); // we will write the exact number of tetra later, within the empty space left below
      fprintf(fvtk, "                    >\n");

      // Write the vertices
      fprintf(fvtk, "\t\t\t<Points>\n");
      fprintf(fvtk, "\t\t\t\t<DataArray type=\"Float32\""
                    " NumberOfComponents=\"3\" format=\"ascii\">\n");
      double coord[3];
      for (iVtk = 0; iVtk < nbPts; iVtk++) {
        ierr = computeCoordPipe(iVtk, coord); CHKERRQ(ierr);
        fprintf(fvtk, "%g %g %g\n", (float) (coord[0]), (float)(coord[1]), (float)(coord[2]));
      }
      fprintf(fvtk, "\t\t\t\t</DataArray>\n");
      fprintf(fvtk, "\t\t\t</Points>\n");
      // VTK Elements
      fprintf(fvtk, "\t\t\t<Cells>\n");
      fprintf(fvtk, "\t\t\t\t<DataArray "
                    "type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");
    }
    if (writeMeshUNV) {
#define sNODE_UNV_ID "  2411"
#define sELT_UNV_ID  "  2412"
#define sGRP_UNV_ID  "  2435"
#define sUNV_SEPARATOR    "    -1"
#define sNODE_UNV_DESCR   "         1         1        11"
#define sELT_TRIA3_DESC   "        91         1         1         5         3"
#define sELT_TETRA4_DESC  "       111         1         2         9         4"

      Mpf_printf(MPI_COMM_WORLD, "Writing 'mesh.unv'... ") ;
      funv=fopen("mesh.unv", "w");
      /* -------- */
      /* Vertices */
      /* -------- */
      fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
      fprintf(funv, "%s\n", sNODE_UNV_ID );
      double coord[3];
      for(int i = 1; i<=nbPts; i++ ) {
        ierr = computeCoordPipe(i-1, coord); CHKERRQ(ierr);
        fprintf(funv, "%10d%s\n", i, sNODE_UNV_DESCR );
        fprintf(funv, "%25.16E%25.16E%25.16E\n", coord[0], coord[1], coord[2]) ;
      }
      fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
      /* -------- */
      /* Elements */
      /* -------- */
      fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
      fprintf(funv, "%s\n", sELT_UNV_ID );
    }

    /* Loop on the hexaedre: We loop on the bottom-left-exterior corner of the hexaedre and check
     that the 7 other vertices are valid.
     */
    int i_hexa;
    for (i_hexa=0 ; i_hexa<nbPts ; i_hexa++) {
      div_t res=div(i_hexa, n_z*n_l);

      int i_helix = res.quot;
      // the hexaedre is based on helices 'i_helix' and 'i_helix+1', so both must be in [0, n_r-1]
      if (i_helix+1 > n_r-1) continue;

      int i_face = res.rem;
      // the outer face is based on points i_face, i_face+1, i_face+n_l, i_face+n_l+1, they must be in [0, n_z.n_l-1]
      if (i_face+n_l+1 > n_z*n_l-1) continue;

      // In a conformal mesh, we use alternatively the 2 types of hexa defined in defTetra[]
      int typeHexa = conformalMesh ? (i_face+i_helix)%2 : 0;

      /* Vertices of the current hexaedre, in [0..nbPts[ */
      int i_vert[8];
      i_vert[0] = i_helix * n_z*n_l + i_face; // actually it is i_hexa
      i_vert[1] = i_helix * n_z*n_l + i_face+1;
      i_vert[2] = i_helix * n_z*n_l + i_face+n_l;
      i_vert[3] = i_helix * n_z*n_l + i_face+n_l+1;
      i_vert[4] = (i_helix+1) * n_z*n_l + i_face;
      i_vert[5] = (i_helix+1) * n_z*n_l + i_face+1;
      i_vert[6] = (i_helix+1) * n_z*n_l + i_face+n_l;
      i_vert[7] = (i_helix+1) * n_z*n_l + i_face+n_l+1;

      /* Loop on the tetraedron inside this hexaedra (5 tetra per hexa, defTetra[i] gives the vertices
       composing each of them) */
      int i_tetra;
      for (i_tetra=0 ; i_tetra<5 ; i_tetra++) {
        int i_vert_t[4]; // vertices of the current tetraedron, in [0..nbPts[
        int i, j;
        for (i=0 ; i<4 ; i++)
          i_vert_t[i] = i_vert[defTetra[typeHexa][i_tetra][i]];

        // Loop on the unkowns (=vertices) in this tetrahedron
        // Each unknown interacts with the other unknown in the same tetrahedron
        // Value of the interaction is 0.1 for self, 0.05 otherwise
        if (i_vert_t[0] >= 0 && i_vert_t[0] < nbPts && i_vert_t[1] >= 0 && i_vert_t[1] < nbPts && i_vert_t[2] >= 0 && i_vert_t[2] < nbPts && i_vert_t[3] >= 0 && i_vert_t[3] < nbPts) {
          nbTetra++;
          for (i=0 ; i<4 ; i++)
            for (j=0 ; j<4 ; j++)
              addValue(i_vert_t[i], i_vert_t[j], i==j ? 0.1 : 0.05);

          if (writeMesh)
            fprintf(fvtk, "%d %d %d %d\n", i_vert_t[0], i_vert_t[1], i_vert_t[2], i_vert_t[3]);
          if (writeMeshUNV) {
            fprintf(funv, "%10d%s\n", nbTetra, sELT_TETRA4_DESC ) ;
            fprintf(funv, "%10d%10d%10d%10d\n", i_vert_t[0]+1, i_vert_t[1]+1, i_vert_t[2]+1, i_vert_t[3]+1);
          }
        }
      }
    }

    if (writeMesh) {
      fprintf(fvtk, "\t\t\t\t</DataArray>\n");
      /* Ecriture des offsets */
      fprintf(fvtk, "\t\t\t\t<DataArray "
                    "type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
      for (iVtk = 0; iVtk < nbTetra; iVtk++) {
        fprintf(fvtk, "%d ", iVtk*4+4);
        if (iVtk % 10 == 9)
          fprintf(fvtk, "\n");
      }
      fprintf(fvtk, "\t\t\t\t</DataArray>\n");

      /* Ecriture des types d'elements */
      fprintf(fvtk, "\t\t\t\t<DataArray "
                    "type=\"UInt8\" Name=\"types\" format=\"ascii\">\n");
      for (iVtk = 0; iVtk < nbTetra; iVtk++) {
        fprintf(fvtk, "%d ", 10); // For VTK_TETRA
        if (iVtk % 10 == 9)
          fprintf(fvtk, "\n");
      }
      fprintf(fvtk, "\t\t\t\t</DataArray>\n");
      fprintf(fvtk, "\t\t\t</Cells>\n");
      fprintf(fvtk, "\t\t</Piece>\n\t</UnstructuredGrid>\n</VTKFile>\n");

      fsetpos(fvtk, &posNbTetra); // Write a-posteriori the exact number of hexa
      fprintf(fvtk, "%d\"", nbTetra) ;

      fclose(fvtk);
      Mpf_printf(MPI_COMM_WORLD, "Done writing 'mesh.vtu'\n") ;
    }
    if (writeMeshUNV) {
      // Add the triangles on the outer helix
      // Like for tetra, the shape of triangles depends on conformal/non-conformal mesh
      int defTria[2][2][3]= { { {0,1,2}, {2,1,3}}, // 'A' hexa
                              { {0,1,3}, {2,0,3}} }; // 'B' hexa
      for (int i_face=0 ; i_face<n_z*n_l ; i_face++) {
        // the outer face is based on points i_face, i_face+1, i_face+n_l, i_face+n_l+1, they must be in [0, n_z.n_l-1]
        if (i_face+n_l+1 > n_z*n_l-1) continue;

        // In a conformal mesh, we use alternatively the 2 types of hexa defined in defTetra[]
        int typeHexa = conformalMesh ? i_face%2 : 0;

        /* Vertices of the current outer triangles in [0..n_z*n_l[ */
        int i_vert[4];
        i_vert[0] = i_face;
        i_vert[1] = i_face+1;
        i_vert[2] = i_face+n_l;
        i_vert[3] = i_face+n_l+1;

        // Add the 2 triangles using vertices defined in defTria
        for (int i_tria=0 ; i_tria<2 ; i_tria++) {
          int i_vert_t[3]; // vertices of the current triangle, in [0..n_z*n_l[
          for (int i=0 ; i<3 ; i++)
            i_vert_t[i] = i_vert[defTria[typeHexa][i_tria][i]];
          fprintf(funv, "%10d%s\n", ++nbTria + nbTetra, sELT_TRIA3_DESC ) ;
          fprintf(funv, "%10d%10d%10d\n", i_vert_t[0]+1, i_vert_t[1]+1, i_vert_t[2]+1);
        }
      }
      fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
      /* ------ */
      /* Groups */
      /* ------ */
      fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
      fprintf(funv, "%s\n", sGRP_UNV_ID );
      /* Group V_D1 for all the tetrahedra */
      fprintf(funv, "%10d%10d%10d%10d%10d%10d%10d%10d\n", 1, 0, 0, 0, 0, 0, 0, nbTetra) ;
      fprintf(funv, "V_D1\n") ;
      for (int i=1 ; i<=nbTetra ; i++) {
        fprintf(funv, "%10d%10d%10d%10d", 8, i, 0, 0) ;
        if ( (i%2==0) || (i==nbTetra) )
          fprintf(funv, "\n") ;
      }
      /* Group I_D1_EXT for all the triangles */
      fprintf(funv, "%10d%10d%10d%10d%10d%10d%10d%10d\n", 1, 0, 0, 0, 0, 0, 0, nbTria) ;
      fprintf(funv, "I_D1_EXT\n") ;
      for (int i=1 ; i<=nbTria ; i++) {
        fprintf(funv, "%10d%10d%10d%10d", 8, i+nbTetra, 0, 0) ;
        if ( (i%2==0) || (i==nbTria) )
          fprintf(funv, "\n") ;
      }
      fprintf(funv, "%s\n",sUNV_SEPARATOR ) ;
      fclose(funv);
      Mpf_printf(MPI_COMM_WORLD, "Done.\n") ;
    }

    Mpf_printf(MPI_COMM_WORLD, "Number of Tetra = %d\n", nbTetra);
    Mpf_printf(MPI_COMM_WORLD, "Number of NNZ before sort = %zu\n", nnz_FEM);
    // Passer en multithread ?
    ierr=Z_sortAndCompactL(z_matComp_FEM, &nnz_FEM) ; CHKERRQ(ierr) ;
    z_matComp_FEM=(ZnonZero*)MpfRealloc(z_matComp_FEM,nnz_FEM*sizeof(ZnonZero)); CHKPTRQ(z_matComp_FEM) ;
    Mpf_printf(MPI_COMM_WORLD, "Number of NNZ after sort = %zu\n", nnz_FEM);
  }

  // we broadcast the mesh in MPI : z_matComp_FEM, nnz_FEM.
  ierr = MPI_Bcast(&nnz_FEM, sizeof(size_t), MPI_BYTE, 0, MPI_COMM_WORLD); CHKERRQ(ierr);
  if (rank) {
    z_matComp_FEM=(ZnonZero*)MpfRealloc(z_matComp_FEM,nnz_FEM*sizeof(ZnonZero)); CHKPTRQ(z_matComp_FEM) ;
  }
  for (size_t off=0 ; off<nnz_FEM*sizeof(ZnonZero) ; off += 1e9) {
    ierr = MPI_Bcast((char*)z_matComp_FEM + off, MIN(nnz_FEM*sizeof(ZnonZero)-off, 1e9), MPI_BYTE, 0, MPI_COMM_WORLD); CHKERRQ(ierr);
  }

  /* Store in ptr_ordered_FEM the beginning of each line in z_matComp */
  ptr_ordered_FEM = MpfCalloc(nbPts+1,sizeof(size_t)); CHKPTRQ(ptr_ordered_FEM);
  size_t i, k=0 ;
  for (i=0;i<nnz_FEM;i++)
    while  (k <= z_matComp_FEM[i].i ) {
      ptr_ordered_FEM[k] = i ; /* La ligne 'k' commence a la position 'i' */
      k++ ;
    }
  while (k <= nbPts ) {
    ptr_ordered_FEM[k] = nnz_FEM ; /* Les lignes suivantes sont vides */
    k++ ;
  }

  // In case we pass a second time in this routine
  writeMesh = 0;
  writeMeshUNV = 0;

  return 0;
}

int computeKernelFEM(int i, int j, Z_type *kernel) {
  ZnonZero z_cle={i, j, { 0.}};
  ASSERTA(z_matComp_FEM);
  // Line 'i' begins at position ptr_ordered_FEM[i] and has size ptr_ordered_FEM[i+1]-ptr_ordered_FEM[i]
  ZnonZero *z_res = (ZnonZero*)bsearch(&z_cle, z_matComp_FEM+ptr_ordered_FEM[i], ptr_ordered_FEM[i+1]-ptr_ordered_FEM[i], sizeof(ZnonZero), compare_ZnonZero_Line);

  if (z_res)
    *kernel=z_res->v;

  return 0;
}

int produitClassiqueFEM(void *sol, MPI_Comm myComm) {
  int ierr, nbProcs, myProc;
  size_t l, jg;
  Z_type *z_sol=sol, *z_rhs=rhs ;
  D_type *d_sol=sol, *d_rhs=rhs ;

  if (testedModel == _bem) return 0;

  enter_context("produitClassiqueFEM");

  Mpf_printf(myComm, "FEM: ");

  ierr = MPI_Comm_rank(myComm, &myProc) ; CHKERRQ(ierr) ;
  ierr = MPI_Comm_size(myComm, &nbProcs) ; CHKERRQ(ierr) ;

  /* Global counter jg = only for progress bar */
  jg=0;
  ierr=Mpf_progressBar(myComm, 0, (int)(nnz_FEM/100)+2) ;
  /* Boucle sur les elements non nuls de la matrice */
#pragma omp parallel for private(ierr) schedule(dynamic,10000)
  for (l=0 ; l<nnz_FEM ; l++) {
    size_t i, j, k;
    Z_type Aij;

    if (z_matComp_FEM[l].j % sparseRHS == 0) {
      Aij=z_matComp_FEM[l].v;
      i=z_matComp_FEM[l].i;
      j=z_matComp_FEM[l].j;
      for (k=0 ; k<nbRHS ; k++) {
        if (complexALGO) {
#pragma omp atomic
          z_sol[(size_t)i+k*nbPts].r += Aij.r * z_rhs[(size_t)j+k*nbPts].r - Aij.i * z_rhs[(size_t)j+k*nbPts].i ;
#pragma omp atomic
          z_sol[(size_t)i+k*nbPts].i += Aij.r * z_rhs[(size_t)j+k*nbPts].i + Aij.i * z_rhs[(size_t)j+k*nbPts].r ;
        } else {
#pragma omp atomic
          d_sol[(size_t)i+k*nbPts] += Aij.r * d_rhs[(size_t)j+k*nbPts] ;
        }
      } /* for (k=0 ... */
    } /* if (z_matComp[l].j ... */

    size_t jg_bak;
#pragma omp atomic capture
    jg_bak = ++jg ;
#ifdef _OPENMP
    if (omp_get_thread_num()==0)
#endif
    {
      ierr=Mpf_progressBar(myComm, (int)(jg_bak/100)+1, (int)(nnz_FEM/100)+2) ;
    }
  }

  ierr=Mpf_progressBar(myComm, (int)(nnz_FEM/100)+2, (int)(nnz_FEM/100)+2) ;

  leave_context();

  return 0;
}

/*! \brief Computes a block for the matrix of FEM interactions.

  Row and column indices are 1-based and included.
*/
void computeSparseBlockFEMBEM ( int *LigInf, int *LigSup, int *ColInf, int *ColSup,
                                int *___Inf, int *___Sup, int *iloc, int *jloc,
                                int *nnz, void **matComp, void *context) {

  /* Extract in the FEM/BEM matrix the elements located in these intervals of rows and columns */
  int nnzTemp=0 ;
  int row, col;
  size_t pos;
  ASSERTA(context);
  contextTestFEMBEM* myCtx = (contextTestFEMBEM*)context;

  ASSERTA(*___Inf == 0);
  ASSERTA(*___Sup == 1);

  // Shift the block index into the global index
  int LigInf_g = *LigInf + myCtx->rowOffset;
  int LigSup_g = *LigSup + myCtx->rowOffset;
  int ColInf_g = *ColInf + myCtx->colOffset;
  int ColSup_g = *ColSup + myCtx->colOffset;
  // FEM part
  if (testedModel == _fembem || testedModel == _fem) {

    /* Count the number of non zero values to copy and reallocate the target sparse matrix */
    for (row=LigInf_g-1 ; row<=LigSup_g-1 ; row++)
      // Line 'row' begins at position ptr_ordered_FEM[row] and ends before ptr_ordered_FEM[row+1]
      for (pos = ptr_ordered_FEM[row]; pos < ptr_ordered_FEM[row+1]; ++pos)
        if (z_matComp_FEM[pos].j >= ColInf_g-1 && z_matComp_FEM[pos].j <= ColSup_g-1)
          nnzTemp++;

    if (nnzTemp) {
      matComp[0]=MpfRealloc(matComp[0], Mpf_nonzerostype_size[stype]*(nnz[0]+nnzTemp)); CHKPTRA(matComp[0]);
      // pointer on the newly allocated area
      SnonZero* s_matComp=(SnonZero*)(matComp[0])+nnz[0];
      DnonZero* d_matComp=(DnonZero*)(matComp[0])+nnz[0];
      CnonZero* c_matComp=(CnonZero*)(matComp[0])+nnz[0];
      ZnonZero* z_matComp=(ZnonZero*)(matComp[0])+nnz[0];
      // update the total number of non-zero
      nnz[0] += nnzTemp;

      /* Copy the new values and convert the coordinates to local indices */
      for (row=LigInf_g-1 ; row<=LigSup_g-1 ; row++)
        // Line 'row' begins at position ptr_ordered_FEM[row] and ends before ptr_ordered_FEM[row+1]
        for (pos = ptr_ordered_FEM[row]; pos < ptr_ordered_FEM[row+1]; ++pos) {
          ASSERTA(z_matComp_FEM[pos].i == row);
          // column index
          col = z_matComp_FEM[pos].j ;
          if (col >= ColInf_g-1 && col <= ColSup_g-1) {
            // we have a non zero element in this block at position i,j
            int i = row - (LigInf_g-1) + (*iloc - 1);
            int j = col - (ColInf_g-1) + (*jloc - 1);
            switch (stype) {
              case SIMPLE_PRECISION:
                s_matComp->i=i;
                s_matComp->j=j;
                s_matComp->v=(S_type)(z_matComp_FEM[pos].v.r);
                s_matComp++;
                break;
              case DOUBLE_PRECISION:
                d_matComp->i=i;
                d_matComp->j=j;
                d_matComp->v=(D_type)(z_matComp_FEM[pos].v.r);
                d_matComp++;
                break;
              case SIMPLE_COMPLEX:
                c_matComp->i=i;
                c_matComp->j=j;
                c_matComp->v.r=(S_type)(z_matComp_FEM[pos].v.r);
                c_matComp->v.i=(S_type)(z_matComp_FEM[pos].v.i);
                c_matComp++;
                break;
              case DOUBLE_COMPLEX:
                z_matComp->i=i;
                z_matComp->j=j;
                z_matComp->v=z_matComp_FEM[pos].v;
                z_matComp++;
                break;
              default:
                break;
            } /* switch (stype) ... */
          } /* if (col >= ColInf_g-1 ... */
        } /* for (pos = ptr_ordered_FEM[row]; ... */
    }
  }

  // BEM part
  if (LigInf_g-1 < nbPtsBEM && ColInf_g-1 < nbPtsBEM) {

    /* Count the number of non zero values in the BEM part reallocate the target sparse matrix */
    nnzTemp=0;
    for (row=LigInf_g-1 ; row<=LigSup_g-1 && row<nbPtsBEM; row++)
      for (col=ColInf_g-1 ; col<=ColSup_g-1 && col<nbPtsBEM; col++)
        nnzTemp++;

    if (nnzTemp>0) {
      matComp[0]=MpfRealloc(matComp[0], Mpf_nonzerostype_size[stype]*(nnz[0]+nnzTemp)); CHKPTRA(matComp[0]);
      // pointer on the newly allocated area
      SnonZero* s_matComp=(SnonZero*)(matComp[0])+nnz[0];
      DnonZero* d_matComp=(DnonZero*)(matComp[0])+nnz[0];
      CnonZero* c_matComp=(CnonZero*)(matComp[0])+nnz[0];
      ZnonZero* z_matComp=(ZnonZero*)(matComp[0])+nnz[0];
      // update the total number of non-zero
      nnz[0] += nnzTemp;

      double coord_i[3], coord_j[3];
      int ierr;
      for (row=LigInf_g-1 ; row<=LigSup_g-1 && row<nbPtsBEM; row++) {
        ierr=computeCoordCylinder(row, &(coord_i[0])); CHKERRA(ierr);
        for (col=ColInf_g-1 ; col<=ColSup_g-1 && col<nbPtsBEM; col++) {
          double _Complex Aij = 0;
          ierr=computeCoordCylinder(col, &(coord_j[0])); CHKERRA(ierr);
          ierr=computeKernelBEM(&(coord_i[0]), &(coord_j[0]), row==col, &Aij); CHKERRA(ierr);

          // we have a non zero element in this block at position i,j
          int i = row - (LigInf_g-1) + (*iloc - 1);
          int j = col - (ColInf_g-1) + (*jloc - 1);
          switch (stype) {
            case SIMPLE_PRECISION:
              s_matComp->i=i;
              s_matComp->j=j;
              s_matComp->v = Aij;
              s_matComp++;
              break;
            case DOUBLE_PRECISION:
              d_matComp->i=i;
              d_matComp->j=j;
              d_matComp->v = Aij;
              d_matComp++;
              break;
            case SIMPLE_COMPLEX:
              c_matComp->i=i;
              c_matComp->j=j;
              c_matComp->cv = Aij;
              c_matComp++;
              break;
            case DOUBLE_COMPLEX:
              z_matComp->i=i;
              z_matComp->j=j;
              z_matComp->cv = Aij;
              z_matComp++;
              break;
            default:
              break;
          } /* switch (stype) ... */
        }
      }
    }
  }

  /* Sort the sparse matrix and sums the values at the same position */
  int ierr;
  switch (stype) {
    case SIMPLE_PRECISION:
      ierr=S_sortAndCompact((SnonZero*)(matComp[0]), nnz) ; CHKERRA(ierr) ;
      break;
    case DOUBLE_PRECISION:
      ierr=sortAndCompact((DnonZero*)(matComp[0]), nnz) ; CHKERRA(ierr) ;
      break;
    case SIMPLE_COMPLEX:
      ierr=C_sortAndCompact((CnonZero*)(matComp[0]), nnz) ; CHKERRA(ierr) ;
      break;
    case DOUBLE_COMPLEX:
      ierr=Z_sortAndCompact((ZnonZero*)(matComp[0]), nnz) ; CHKERRA(ierr) ;
      break;
    default:
      break;
  } /* switch (stype) ... */

  ASSERTA(*nnz <= (LigSup_g-LigInf_g+1)*(ColSup_g-ColInf_g+1));

  return;
}
