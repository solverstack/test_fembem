# Set label_VERSION to the git version

function(git_version label default_version)
    option(${label}_GIT_VERSION "Get the version string from git describe" ON)
    if(${label}_GIT_VERSION)
        find_package(Git)
        if(GIT_FOUND)
            execute_process(COMMAND "${GIT_EXECUTABLE}"
              describe --dirty=-dirty --always --tags
	      WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
              OUTPUT_VARIABLE _GIT_DESCRIBE ERROR_QUIET)
            if(_GIT_DESCRIBE)
                string(STRIP ${_GIT_DESCRIBE} ${label}_VERSION)
                # Remove nonnumeric prefix, if any
                STRING(REGEX REPLACE "^[^0-9]+" "" ${label}_STRIPPED_VERSION ${${label}_VERSION})
                set(${label}_VERSION ${${label}_STRIPPED_VERSION} PARENT_SCOPE)
            endif()
        endif()
    endif()
    if(NOT ${label}_VERSION)
        set(${label}_VERSION ${default_version} PARENT_SCOPE)
    endif()
    message(STATUS "Version string is ${${label}_VERSION}")
endfunction()
