#pragma once
#include "config.h"

/* ================================================================================= */
/*! \brief  Flag allowing unique global data declaration

  \sa  ___TEST_FEMBEM___ */
#ifdef ___TEST_FEMBEM___
#define _EXTERN_TEST_FEMBEM_
#else
#define _EXTERN_TEST_FEMBEM_ extern
#endif
/* ================================================================================= */
#include <math.h>
#include <complex.h>
#include "mpi.h"
#ifdef HAVE_HMAT
#include "hmat/hmat.h"
#endif
#ifdef HAVE_CHAMELEON
#include "chameleon.h"
#endif
#ifdef HMAT_PARALLEL
#include "hmat/hmat_parallel.h" // from hmat (not oss)
#endif
#include "util.h"

#if defined __cplusplus
extern "C" {
#endif

/* ================================================================================= */
/*! \brief  List of numerical model (and matrix content) that we can test.

  _bem yields dense matrices, _fem sparse matrices, _fembem couples dense and sparse matrices.
*/
enum numModel {
  _bem  = 0,
  _fem = 1,
  _fembem = 2
} ;

/*! \brief  Numerical model (and matrix content) we want to test.

  Default is _bem. Can be set with --bem, --fem, --fembem.
*/
extern enum numModel testedModel;

/*! \brief Number of right hand sides for the test */
extern int nbRHS;

/*! \brief Sparsity of the right hand sides (1 non-zero value every 'n' values). Default is nbpts/80.log10(nbpts).

The objective of using 'sparseRHS' is to speed-up the computation in produitClassique() in classic.c.
*/
extern int sparseRHS;

/*! \brief Use new way to compute RHS. Default is True. */
extern int newRHS;

/*! \brief Number of points in the points cloud (BEM+FEM) */
extern int nbPts;

/*! \brief Tile size for the CHAMELEON solver (change it with '--chameleon-tile') */
extern int tileSize;

/*! \brief Number of points in the points cloud (BEM main cylinder only) */
extern int nbPtsMain ;

/*! \brief Number of points in the points cloud (BEM part only) */
extern int nbPtsBEM ;

/*! \brief RHS for my tests */
extern void *rhs;

/*! \brief Using partial interactions between BEM and FEM unknowns

 It is set with --partial-fembem
 If true, then only one third of the BEM unknowns will interact with FEM unknowns.
 It is used to mimic the case where a part of the BEM mesh is NOT the outer surface of the FEM mesh.
 It changes the content of the matrix and hence the results, timings, accuracy, etc.
*/
extern int partial_fembem;

/*! \brief Verbosity flag */
extern int verboseTest;

/*! \brief NTiles recursive clustering enabled */
extern int use_ntiles_rec;

/*! \brief flag to activate result check */
extern int check_result;

/*! \brief flag to activate traces generation */
extern int generate_traces;

/*! \brief flag to active an overmeshed detail */
extern int useDetail;

/*! \brief Flag to compute and display matrix norms during Hmat tests */
extern int computeNorm;

/*! \brief Symmetry flag for the content of the matrices (0=nosym, 1=sym, 2=sym pos def, 1 by default) */
extern int symMatContent;

/*! \brief Symmetry flag for the solver (true by default) */
extern int symMatSolver;

/*! \brief Simple precision accuracy flag */
extern int simplePrec;

/*! \brief Complex scalar type flag */
extern int complexALGO;

/*! \brief Scalar type used by the tests */
extern ScalarType stype;

/*! \brief  Wavelength (for oscillatory kernels). */
extern double lambda;

/*! \brief  Number of points per wavelength (for oscillatory kernels) on the main cylinder. */
extern double ptsPerLambda;

/*! \brief  Number of points per wavelength (for oscillatory kernels) on the detail. */
extern double ptsPerLambdaDetail;

/*! \brief  Radius of the leaves in the trees. */
extern double radiusLeaf;

/*! \brief  Write a VTK file of the mesh. Enabled with option --write-mesh. */
extern int writeMesh;

/*! \brief  Write a UNV file of the mesh. Enabled with option --write-mesh-unv. */
extern int writeMeshUNV;

/*! \brief  Create a conformal volume mesh by using alternatively 2 ways of splitting hexa into 5 tetra.

  Disabled with --no-conformal-mesh, enabled with --conformal-mesh. Default is enabled. */
extern int conformalMesh;

/*! \brief  List of  algorithms that we can test. */
enum algo {
  _undefined,
  _gemvHMAT,
  _solveHMAT,
  _inverseHMAT,
  _gemvCHAMELEON,
  _solveCHAMELEON,
  _gemvHLIBPRO,
  _solveHLIBPRO,
  _gemvHCHAMELEON,
  _gemmHCHAMELEON,
  _solveHCHAMELEON,
  _gemvCOMPOSYX,
  _solveCOMPOSYX
} ;

/*! \brief  Algorithms that we want to test. */
extern enum algo testedAlgo;

/*! \brief  Flag to use simple assembly routine for HMAT (default is 0).

  It can be changed with --use_simple_assembly (read in readFlagsTestHMAT() in hmat.c)
*/
extern int use_simple_assembly;

/*! \brief Value of the Hmat clustering divider (number of children per box, default is 2)

  It can be changed with "--hmat_divider" (read in readFlagsTestHMAT() in hmat.c)
*/
extern int divider;

/*! \brief  Flag to compute the residual norm after the Hmat solve (default is 0).

  It can be changed with --hmat_residual (read in readFlagsTestHMAT() in hmat.c)
*/
extern int hmat_residual;

/*! \brief  Flag to use Hmat iterative refinement (default is 0).

  It can be changed with --hmat_refine (read in readFlagsTestHMAT() in hmat.c)
*/
extern int hmat_refine;
/*! \brief  Flag to use Hmat shuffle clustering (default is 0).

  It can be changed with --use_hmat_shuffle (read in readFlagsTestHMAT() in hmat.c)
*/
extern int use_hmat_shuffle;

/*! \brief Value of the Hmat shuffle clustering minimum divider (minimum number of children per box, default is 2)

  It can be changed with "--hmat_divider_min" (read in readFlagsTestHMAT() in hmat.c)
*/
extern int divider_min;

/*! \brief Value of the Hmat shuffle clustering maximum divider (maximum number of children per box, default is 4)

  It can be changed with "--hmat_divider_max" (read in readFlagsTestHMAT() in hmat.c)
*/
extern int divider_max;

/*! \brief Flag to use HODLR storage and algorithms (default is 0).

  It can be changed with "--hodlr" (read in readFlagsTestHMAT() in hmat.c)
*/
extern int use_hodlr;

/*! \brief number of elements in z_matComp_FEM[]
   */
extern size_t nnz_FEM;

/*! \brief  Array of non-zero values of the FEM matrix
*/
extern ZnonZero*z_matComp_FEM;

/*! \brief  beginning of each line in z_matComp_FEM[]
   */
extern size_t *ptr_ordered_FEM;

/*! \brief User context for building MPF matrices and vectors in testMPF */
typedef struct {
  /*! \brief Pointer on the data used to assembly Mat/vec with computeDenseBlockRHS() */
  void *dataVec;
  /*! \brief Offset on row and column when building sub-part of the FEM-BEM matrix */
  int rowOffset, colOffset;
  /*! \brief Dimensions in columns of the matrix

      Used in prepare_hmat() - and indirectly in computeDenseBlockFEMBEM() and CHAMELEON_build_callback_FEMBEM()
  */
  int colDim;
} contextTestFEMBEM;

#ifndef HAVE_HMAT
// If we compile without hmat, we need these phony declarations to use prepare_block()/advanced_compute_hmat() in computeDenseBlockFEMBEM()
typedef struct hmat_block_info_struct {
  void * user_data;
  void (*release_user_data)(void*);
  char (*is_guaranteed_null_row)(const struct hmat_block_info_struct * block_info, int block_row_offset, int stratum);
  char (*is_guaranteed_null_col)(const struct hmat_block_info_struct * block_info, int block_col_offset, int stratum);
} hmat_block_info_t;
struct hmat_block_compute_context_t {
  void* user_data;
  int row_start, row_count, col_start, col_count;
  int stratum;
  void* block;
};
typedef enum {
  hmat_factorization_none = -1,
  hmat_factorization_lu,
  hmat_factorization_ldlt,
  hmat_factorization_llt
} hmat_factorization_t;
#endif

int computeKernelBEM(double *coord1, double *coord2, int self, double _Complex *kernel) ;
int produitClassiqueBEM(int ffar, void *sol, MPI_Comm myComm) ;
int produitClassique(void **sol, MPI_Comm myComm) ;
int computeRelativeError(void *sol, void *ref, double *eps) ;
int computeVecNorm(void *ref, double *norm);
int sommePara(double *buf) ;
int computeCoordCylinder(int i, double *coord) ;
int initCylinder(int *argc, char ***argv) ;
int initCylinderDetail(int *argc, char ***argv) ;
int getMeshStep(double *m) ;
int getMeshStepDetail(double *m) ;
int initPipe(int *argc, char ***argv) ;
int computeCoordPipe(int i, double *coord) ;
int createFemMatrix() ;
int freeFemMatrix() ;
int produitClassiqueFEM(void *sol, MPI_Comm myComm) ;
double* createPipe(void) ;
int computeKernelFEM(int i, int j, Z_type *kernel) ;
void computeSparseBlockFEMBEM(int *LigInf, int *LigSup, int *ColInf, int *ColSup, int *___Inf, int *___Sup, int *iloc, int *jloc, int *nnz, void **matComp, void *context);
int main(int argc, char **argv) ;
int computeRhs(void) ;
void computeDenseBlockData (int *LigInf, int *LigSup, int *ColInf, int *ColSup, int *___Inf, int *___Sup, int *iloc, int *jloc, int *tailleS, int *tailleL, void *bloc, void *context);
double getTime() ;
int displayArray(char *s, void *f, int m, int n) ;
int testHMAT(double * relative_error) ;
int prepareTEST(void);
double* createCylinder(void) ;
void interactionKernel(void* data, int i, int j, void* result) ;
int printHelp() ;
void computeDenseBlockFEMBEM(int *LigInf, int *LigSup, int *ColInf, int *ColSup, int *___Inf, int *___Sup, int *iloc, int *jloc, int *tailleS, int *tailleL, void *bloc, void *context);
void computeDenseBlockFEMBEM_Cprec(int *LigInf, int *LigSup, int *ColInf, int *ColSup, int *___Inf, int *___Sup, int *iloc, int *jloc, int *tailleS, int *tailleL, void *bloc, void *context);
void prepare_hmat(int, int, int, int, int*, int*, int*, int*, void*, hmat_block_info_t *);
void advanced_compute_hmat(struct hmat_block_compute_context_t*);
int init_hmat_interface() ;
int readFlagsTestHMAT(int *argc, char ***argv) ;
int testCHAMELEON(double * relative_error);
int testHLIBPRO(double * relative_error);
int initHLIBPRO(int* argc, char*** argv) ;
int testHCHAMELEON(double * relative_error);
int testCOMPOSYX(double * relative_error);

#ifdef HAVE_CHAMELEON
extern cham_flttype_t chameleonType;
extern int (*CHAMELEON_sytrf_Tile)( cham_uplo_t, CHAM_desc_t * );
extern int (*CHAMELEON_sytrs_Tile)( cham_uplo_t, CHAM_desc_t *, CHAM_desc_t * );
extern int (*CHAMELEON_potrf_Tile)( cham_uplo_t, CHAM_desc_t * );
extern int (*CHAMELEON_potrs_Tile)( cham_uplo_t, CHAM_desc_t * );
extern int (*CHAMELEON_getrf_nopiv_Tile)( CHAM_desc_t * );
extern int (*CHAMELEON_getrs_nopiv_Tile)( CHAM_desc_t *, CHAM_desc_t * );
extern int (*CHAMELEON_build_Tile)( cham_uplo_t, CHAM_desc_t *, void *, void* );

int testFEMBEM_initChameleon( ScalarType stype );
int CHAMELEON_gemm_Tile( CHAM_desc_t *descA, CHAM_desc_t *descX, CHAM_desc_t *descY );

int CHAMELEON_generate_matrix( cham_flttype_t flttype, int NB, int PQ[2],
CHAM_desc_t     **descA );
int CHAMELEON_destroy_matrix( CHAM_desc_t *descA );
#endif

#ifdef CHAMELEON_USE_HMAT
struct HCHAM_desc_s;
typedef struct HCHAM_desc_s HCHAM_desc_t;

struct HCHAM_desc_s {
  CHAM_desc_t                 *super;
  hmat_cluster_tree_t        **clusters;
  hmat_admissibility_t        *admissibilityCondition;
  hmat_interface_t            *hi;
  hmat_matrix_t               *hmatrix;
  int                         *perm;
};

int         HCHAMELEON_generate_matrix( cham_flttype_t flttype, int NB, int PQ[2],
HCHAM_desc_t     *hdescA,
hmat_interface_t *hi );
int          HCHAMELEON_destroy_matrix( HCHAM_desc_t *hdescA );
hmat_info_t  HCHAMELEON_getinfo( HCHAM_desc_t *hdesc );
CHAM_desc_t *HCHAMELEON_uncompress_matrix( HCHAM_desc_t *hdesc );
void         HCHAMELEON_lapmr( char forward, char trans, int nrhs,
                               HCHAM_desc_t *hdesc,
                               char *Bptr );
#endif

#ifdef HAVE_HMAT
struct HMAT_desc_s;
typedef struct HMAT_desc_s HMAT_desc_t;

struct HMAT_desc_s {
  hmat_matrix_t               *hmatrix;
  hmat_clustering_algorithm_t *clustering;
  hmat_cluster_tree_t         *cluster_tree;
  hmat_admissibility_t        *admissibilityCondition;
  contextTestFEMBEM           *myCtx;
};

HMAT_desc_t *HMAT_generate_matrix( hmat_interface_t *hi );
void         HMAT_destroy_matrix ( hmat_interface_t *hi,
                                   HMAT_desc_t      *hdesc );
#endif

#if defined __cplusplus
} // extern "C"
#endif

