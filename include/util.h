#pragma once
#include "config.h"
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HAVE_TIME_H
#include <time.h>
#endif
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_SYS_RESOURCE_H
#include <sys/resource.h>
#endif
#ifdef _WIN32
#include <windows.h>
#endif
#ifdef HAVE_MACH_MACH_TIME_H
#include <mach/mach_time.h>
#endif
#include <ctype.h>
#include <limits.h>
#include <float.h>
#ifdef _OPENMP
#include "omp.h"
#endif
#include <stdbool.h>


#if defined __cplusplus
extern "C" {
#endif

typedef enum { MPF_FALSE, MPF_TRUE } Logical;

/**************************************************************************************/
/***   Scalar Types Definitions   *****************************************************/
/**************************************************************************************/
/*! \brief Simple real type.

  This scalar type corresponds to the SIMPLE_PRECISION value in the ScalarType enumeration.
*/
typedef float S_type ;
/**************************************************************************************/
/*! \brief Double real type.

  This scalar type corresponds to the DOUBLE_PRECISION value in the ScalarType enumeration.
*/
typedef double D_type ;
/**************************************************************************************/
/*! \brief Simple complex type.

  This scalar type corresponds to the SIMPLE_COMPLEX value in the ScalarType enumeration.
*/
typedef struct {
  /*! \brief real part */
  float r ;
  /*! \brief imaginary part */
  float i ;
} C_type;
/**************************************************************************************/
/*! \brief Double complex type.

  This scalar type corresponds to the DOUBLE_COMPLEX value in the ScalarType enumeration.
*/
typedef struct {
  /*! \brief real part */
  double r ;
  /*! \brief imaginary part */
  double i ;
} Z_type;
/**************************************************************************************/
/*! \brief All the scalar types

  This enumeration defines the different scalar types available in MPF.
  They are transmitted to the vectors and matrices creation routine such as MatCreate().
*/
typedef enum {
  /*! \brief Simple real type (float in C, REAL in fortran, S_type in MPF) */
  SIMPLE_PRECISION=0,
  /*! \brief Double real type (double in C, REAL*8 in fortran, D_type in MPF) */
  DOUBLE_PRECISION=1,
  /*! \brief Simple complex type (doesn't exist in C, COMPLEX in fortran, C_type in MPF) */
  SIMPLE_COMPLEX=2,
  /*! \brief Double complex type (doesn't exist in C, DOUBLE COMPLEX in fortran, Z_type in MPF) */
  DOUBLE_COMPLEX=3,
  /*! \brief Number of scalar types available. */
  nbScalarType=4
} ScalarType;

/**************************************************************************************/
/***   Sparse Scalar Types Definitions   **********************************************/
/**************************************************************************************/
/*! \brief Simple real sparse type.

  This sparse scalar type corresponds to the position (row, column) of a non-zero element
  followed by its value (S_type).
*/
typedef struct {
  /*! \brief Row position */
  int i ;
  /*! \brief Column position */
  int j ;
  /*! \brief Value */
  S_type v;
} SnonZero;
/**************************************************************************************/
/*! \brief Double real sparse type.

  This sparse scalar type corresponds to the position (row, column) of a non-zero element
  followed by its value (D_type).
*/
typedef struct {
  /*! \brief Row position */
  int i ;
  /*! \brief Column position */
  int j ;
  /*! \brief Value */
  D_type v;
} DnonZero;
/**************************************************************************************/
/*! \brief Simple complex sparse type.

  This sparse scalar type corresponds to the position (row, column) of a non-zero element
  followed by its value (C_type).
*/
typedef struct {
  /*! \brief Row position */
  int i ;
  /*! \brief Column position */
  int j ;
  union {
    float _Complex cv;
    /*! \brief Value */
    C_type v;
  };
} CnonZero;
/**************************************************************************************/
/*! \brief Double complex sparse type.

  This sparse scalar type corresponds to the position (row, column) of a non-zero element
  followed by its value (Z_type).
*/
typedef struct {
  /*! \brief Row position */
  int i ;
  /*! \brief Column position */
  int j ;
  union {
    double _Complex cv;
    /*! \brief Value */
    Z_type v;
  };
} ZnonZero;
/**************************************************************************************/

/*! \brief Name of the scalar types */
_EXTERN_TEST_FEMBEM_ char *MPF_scalName[4]
#ifdef   ___TEST_FEMBEM___
={"SIMPLE REAL", "DOUBLE REAL", "SIMPLE COMPLEX", "DOUBLE COMPLEX"}
 #endif
 ;

/* ================================================================================== */
/*! \brief  Multi-arithmetics constant.

This structure is used to store any floating point constants in all of the four possible
arithmetics. This is an array of 4 void* pointers, pointing on the constant in S, D, C and Z precision
(in that order). For instance, if X is an MpfConst, then X[SIMPLE_COMPLEX] is a pointer on a C_type.
*/
typedef void* MpfConst [nbScalarType] ;
/* ================================================================================== */
/* +1 in Floating point */
/*! \brief +1 in simple real precision. */
_EXTERN_TEST_FEMBEM_ S_type Mpf_s_pone
#ifdef   ___TEST_FEMBEM___
=1.
    #endif
    ;
/*! \brief +1 in double real precision. */
_EXTERN_TEST_FEMBEM_ D_type Mpf_d_pone
#ifdef   ___TEST_FEMBEM___
=1.
    #endif
    ;
/*! \brief +1 in simple complex precision. */
_EXTERN_TEST_FEMBEM_ C_type Mpf_c_pone
#ifdef   ___TEST_FEMBEM___
={1., 0.}
 #endif
 ;
/*! \brief +1 in double complex precision. */
_EXTERN_TEST_FEMBEM_ Z_type Mpf_z_pone
#ifdef   ___TEST_FEMBEM___
={1., 0.}
 #endif
 ;
/*! \brief +1 in all precisions. */
_EXTERN_TEST_FEMBEM_ MpfConst Mpf_pone
#ifdef   ___TEST_FEMBEM___
={&Mpf_s_pone, &Mpf_d_pone, &Mpf_c_pone, &Mpf_z_pone}
 #endif
 ;
/* ================================================================================== */
/* 0 in Floating point */
/*! \brief Zero in simple real precision. */
_EXTERN_TEST_FEMBEM_ S_type Mpf_s_zero
#ifdef   ___TEST_FEMBEM___
=0.
    #endif
    ;
/*! \brief Zero in double real precision. */
_EXTERN_TEST_FEMBEM_ D_type Mpf_d_zero
#ifdef   ___TEST_FEMBEM___
=0.
    #endif
    ;
/*! \brief Zero in simple complex precision. */
_EXTERN_TEST_FEMBEM_ C_type Mpf_c_zero
#ifdef   ___TEST_FEMBEM___
={0., 0.}
 #endif
 ;
/*! \brief Zero in double complex precision. */
_EXTERN_TEST_FEMBEM_ Z_type Mpf_z_zero
#ifdef   ___TEST_FEMBEM___
={0., 0.}
 #endif
 ;
/*! \brief Zero in all precisions. */
_EXTERN_TEST_FEMBEM_ MpfConst Mpf_zero
#ifdef   ___TEST_FEMBEM___
={&Mpf_s_zero, &Mpf_d_zero, &Mpf_c_zero, &Mpf_z_zero}
 #endif
 ;
/* ================================================================================== */
/* -1 in Floating point */
/*! \brief -1 in simple real precision. */
_EXTERN_TEST_FEMBEM_ S_type Mpf_s_mone
#ifdef   ___TEST_FEMBEM___
=-1.
#endif
;
/*! \brief -1 in double real precision. */
_EXTERN_TEST_FEMBEM_ D_type Mpf_d_mone
#ifdef   ___TEST_FEMBEM___
=-1.
#endif
;
/*! \brief -1 in simple complex precision. */
_EXTERN_TEST_FEMBEM_ C_type Mpf_c_mone
#ifdef   ___TEST_FEMBEM___
={-1., 0.}
#endif
;
/*! \brief -1 in double complex precision. */
_EXTERN_TEST_FEMBEM_ Z_type Mpf_z_mone
#ifdef   ___TEST_FEMBEM___
={-1., 0.}
#endif
;
/*! \brief -1 in all precisions. */
_EXTERN_TEST_FEMBEM_ MpfConst Mpf_mone
#ifdef   ___TEST_FEMBEM___
={&Mpf_s_mone, &Mpf_d_mone, &Mpf_c_mone, &Mpf_z_mone}
#endif
;

/* ================================================================================== */
/*! \brief 0 in integer type. */
_EXTERN_TEST_FEMBEM_ int Mpf_i_zero
#ifdef   ___TEST_FEMBEM___
=0
#endif
;
/*! \brief 1 in integer type. */
_EXTERN_TEST_FEMBEM_ int Mpf_i_one
#ifdef   ___TEST_FEMBEM___
=1
#endif
;

/* ================================================================================== */
/*! \brief Size (in bytes) of the different scalar types. */
_EXTERN_TEST_FEMBEM_ size_t Mpf_stype_size[nbScalarType]
#ifdef   ___TEST_FEMBEM___
={sizeof(S_type), sizeof(D_type), sizeof(C_type), sizeof(Z_type)}
#endif
;
/*! \brief Size (in bytes) of the different non-zero scalar types. */
_EXTERN_TEST_FEMBEM_ size_t Mpf_nonzerostype_size[nbScalarType]
#ifdef   ___TEST_FEMBEM___
={sizeof(SnonZero), sizeof(DnonZero), sizeof(CnonZero), sizeof(ZnonZero)}
 #endif
 ;
/*! \brief MPI type associated to each scalar type. */
_EXTERN_TEST_FEMBEM_ MPI_Datatype Mpf_mpitype[nbScalarType]
#ifdef   ___TEST_FEMBEM___
={MPI_FLOAT, MPI_DOUBLE, MPI_COMPLEX, MPI_DOUBLE_COMPLEX}
#endif
;

#ifdef HAVE_HMAT
enum mpf_hmat_engine { mpf_hmat_seq, mpf_hmat_starpu, mpf_hmat_toyrt };
struct mpf_hmat_settings_t {
  /** hmat interfaces for all scalar types */
  hmat_interface_t *interfaces[4];
  bool interface_initialized;
  enum mpf_hmat_engine engine;
  hmat_factorization_t factorization_type;
  hmat_progress_t progress;
  int l0size;
  hmat_compress_t compressionMethod;
  double epsilon;
  double acaEpsilon;
  int max_leaf_size;
};
_EXTERN_TEST_FEMBEM_ struct mpf_hmat_settings_t mpf_hmat_settings;
struct mpf_hmat_create_compression_args_t {
  int method;
  double threshold;
  const void * output;
};
int hmat_get_sync_exec(void) ;
void mpf_hmat_compression(struct mpf_hmat_create_compression_args_t *);
#endif

#if !(defined (_WIN32)) /* It's a UNIX system, I know this ! */
/* Try to detect a debugger. This relies on the fact that GDB and IDB
   (on Linux at least) catches the signal SIGTRAP and does not deliver
   it to the application.

   When no debugger is attached, the SIGTRAP raised by raise(SIGTRAP)
   will go to my_sigtrap_handler() which will swallow it, and continue
   the program execution as if nothing happened.  Otherwise, SIGTRAP
   will stop the program inside the debugger, as if a breakpoint had
   been reached.

   Thus, debug_break() will break into the debugger if present, and do
   nothing otherwise.
*/
#include <signal.h>

static int is_debugger_present = -1;
static void my_sigtrap_handler(int signum) {
  (void)signum; // silent unused parameter warnings
  is_debugger_present = 0;
  signal(SIGTRAP, SIG_DFL);
}

inline static void debug_break() {
  if (is_debugger_present == -1) {
    is_debugger_present = 1;
    signal(SIGTRAP, my_sigtrap_handler);
    raise(SIGTRAP);
  }
}
#else
inline static void debug_break() {};
#endif

#if !(defined (_WIN32)) /* It's a UNIX system, I know this ! */
#include <execinfo.h>
#include <unistd.h>
inline static void mpf_backtrace(){
  void * stack[32];
  int n = backtrace(stack, 32);
  backtrace_symbols_fd(stack, n, STDERR_FILENO);
}
#else
inline static void mpf_backtrace(){}
#endif

/*! \brief Displays an error message and that's all. */
#define SETWARN(n,s, ...)   {MpfWarning(__LINE__,PACKAGE_NAME,__FILE__,__func__,n,s, ## __VA_ARGS__);}
/*! \brief Displays an error message and quits the current routine. */
#define SETERRQ(n,s, ...)   {int _ierr = MpfError(__LINE__,PACKAGE_NAME,__FILE__,__func__,n,s, ## __VA_ARGS__); debug_break(); return _ierr;}
/*! \brief Displays an error message and aborts the code. */
#define SETERRA(n,s, ...)   {int _ierr = MpfError(__LINE__,PACKAGE_NAME,__FILE__,__func__,n,s, ## __VA_ARGS__);mpf_backtrace();debug_break();MPI_Abort(MPI_COMM_WORLD,_ierr);}
/*! \brief Checks the integer n, if not zero then it displays an error message and quits the current routine. */
#define CHKERRQ(n)     {if (n) SETERRQ(n,(char *)0);}
/*! \brief Checks the integer n, if not zero then it displays an error message and aborts the code. */
#define CHKERRA(n)     {if (n) SETERRA(n,(char *)0);}
/*! \brief Checks the pointer p, if it is NULL then it displays an error message and quits the current routine. */
#define CHKPTRQ(p)     {if (!(p)) SETERRQ(1,"Unable to allocate requested memory");}
/*! \brief Checks the pointer p, if it is NULL then it displays an error message and aborts the code. */
#define CHKPTRA(p)     {if (!(p)) SETERRA(1,"Unable to allocate requested memory");}
/*! \brief Evaluates the expression x, if it is false then it displays an error message and quits the current routine. */
#define ASSERTQ(x) { if (!(x)) { SETERRQ(1, "Assert failure %s", #x); }}
/*! \brief Evaluates the expression x, if it is false then it displays an error message and aborts the code. */
#define ASSERTA(x) { if (!(x)) { SETERRA(1, "Assert failure %s", #x); }}

#if !defined (MIN)
  /*! \brief Minimum value of two elements. */
#define MIN(a,b)           ( ((a)<(b)) ? (a) : (b) )
#endif

#define MpfCalloc(a,b) calloc(a,b)
#define MpfMalloc(a,b) malloc(a)
#define MpfRealloc(a,b) realloc(a,b)
#define MpfFree(a) free(a)
#define enter_context(a)
#define leave_context()

#if defined(_WIN32) && \
  !(defined (__MINGW64__) || defined(__MINGW32__))
typedef struct my_timespec {
  int64_t tv_sec;
  int64_t tv_nsec;
} Time;
#else
typedef struct timespec Time;
#endif

int MpfError(int line,char *package,char *file,const char *function, int number, ...) ;
int MpfWarning(int line,char *package,char *file,const char *function,int number, ...) ;
int Mpf_progressBar(MPI_Comm comm, int currentValue, int maximumValue) ;
int Mpf_printf(MPI_Comm comm, const char *fmt, ...);
int MpfArgGetDouble( int *Argc, char **argv, int rflag, const char *name, double *val ) ;
int MpfArgHasName( int *Argc, char **argv, int rflag, const char *name );
int MpfArgGetInt( int  *Argc, char **argv, int rflag, const char *name, int  *val );
int MPI_AllreduceL(const void *sendbuf, void *recvbuf, ssize_t count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm) ;
int S_sortAndCompact(SnonZero *mat, int *size);
int sortAndCompact(DnonZero *mat, int *size);
int C_sortAndCompact(CnonZero *mat, int *size);
int Z_sortAndCompact(ZnonZero *mat, int *size);
int Z_sortAndCompactL(ZnonZero *mat, size_t *size);
int compare_SnonZero_Line(const void *pt1, const void *pt2);
int compare_DnonZero_Line(const void *pt1, const void *pt2);
int compare_CnonZero_Line(const void *pt1, const void *pt2);
int compare_ZnonZero_Line(const void *pt1, const void *pt2);
Time get_time(void);
Time time_interval(Time t1, Time t2);
Time add_times(Time t1, Time t2);
double time_in_s(Time t);
double time_interval_in_seconds(Time t1, Time t2);
void simpleToDouble(void *data, size_t n) ;
void doubleToSimple(void *data, size_t n) ;
int SCAB_Init(int* argc, char*** argv) ;
int SCAB_Exit(int* argc, char** argv) ;

#if defined __cplusplus
} // extern "C"
#endif
