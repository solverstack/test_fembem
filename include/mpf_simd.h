// Replacement for MPF macros & routines for SIMD optimisations
#define MPF_SIMD_DISP(x) x
#define mpf_simd_cexp cexp
#define mpf_simd_sincos(theta,s,c) {*s=sin(theta);*(c)=cos(theta);}
